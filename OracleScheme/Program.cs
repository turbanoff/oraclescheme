﻿using System;
using System.Data.Common;
using System.Data.OracleClient;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using NLog;
using OracleScheme.DBObjects;

namespace OracleScheme
{
    public static class Program
    {
        static readonly Logger Log = LogManager.GetCurrentClassLogger();
        static readonly object sync = new object();

        public static int Main(string[] args)
        {
            var options = new Options();
            lock (sync)
            {
                if (!CommandLine.Parser.Default.ParseArguments(args, options, (a,b) => {}))
                {
                    return 1;
                }
            }
            try
            {
                Main(options);
                return 0;
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.Message);
                return 1;
            }
        }

        #pragma warning disable 28
        public static void Main(Options options)
        #pragma warning restore 28
        {
            CommonSubOptions commonOptions = (CommonSubOptions) options.FileInputOptions ?? options.OracleInputOptions;
            if (commonOptions == null)
            {
                throw new ArgumentException(options.GetUsage());
            }
            bool readFromDB = (commonOptions == options.OracleInputOptions);
            //Read input
            OracleSchema oracleSchema;
            if (readFromDB)
            {
                var db = options.OracleInputOptions;
                if (db.ConnectionString == null && (db.DataSource == null || db.Login == null || db.Password == null))
                {
                    throw new ArgumentException(options.GetUsage("dump"));
                }
                #pragma warning disable 0618
                DbProviderFactory provider = OracleClientFactory.Instance;
                #pragma warning restore 0618
                DbConnectionStringBuilder builder = provider.CreateConnectionStringBuilder();
                if (db.ConnectionString != null)
                {
                    builder.ConnectionString = db.ConnectionString;
                }
                else
                {
                    builder.Add("User ID", db.Login);
                    builder.Add("Password", db.Password);
                    builder.Add("Data Source", db.DataSource);
                    builder.Add("Pooling", false);
                }
                oracleSchema = new OracleSchema(db.SchemaName);
                using (DbConnection dbConnection = provider.CreateConnection())
                {
                    dbConnection.ConnectionString = builder.ConnectionString;
                    dbConnection.Open();
                    oracleSchema.FillFromDatabase(dbConnection);
                }
            }
            else
            {
                Log.Info("Start deserialization");
                var formatter = new BinaryFormatter();
                using (var fileStream = new FileStream(options.FileInputOptions.InputFile, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    oracleSchema = (OracleSchema)formatter.Deserialize(fileStream);
                }
                Log.Info("End deserialization");
            }

            //Prepare to output
            oracleSchema.Make();

            //Write output
            if (commonOptions.OutputBinaryFile != null)
            {
                var formatter = new BinaryFormatter();
                using (var stream = new FileStream(commonOptions.OutputBinaryFile, FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    formatter.Serialize(stream, oracleSchema);
                }
            }
            else
            {
                Log.Info("Generate Text");
                var generator = new TextGenerator(commonOptions.OutputSchemaName);
                generator.Visit(oracleSchema);

                if (commonOptions.OutputSqlFile != null)
                {
                    File.WriteAllText(commonOptions.OutputSqlFile, generator.Script, Encoding.Default);                    
                }
                else
                {
                    Console.WriteLine(generator.Script);
                }
            }
        }
    }
}
