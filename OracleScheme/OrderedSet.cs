﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace OracleScheme
{
    [Serializable]
    public class OrderedSet<T> : ICollection<T>
    {
        private readonly Dictionary<T, int> dictionary = new Dictionary<T, int>();
        private readonly List<T> list = new List<T>();

        public int Count
        {
            get { return list.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        void ICollection<T>.Add(T item)
        {
            Add(item);
        }

        public void Clear()
        {
            list.Clear();
            dictionary.Clear();
        }

        public bool Remove(T item)
        {
            int index;
            if (!dictionary.TryGetValue(item, out index))
                return false;
            dictionary.Remove(item);
            list.RemoveAt(index);
            return true;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return list.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public bool Contains(T item)
        {
            return dictionary.ContainsKey(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            list.CopyTo(array, arrayIndex);
        }

        public bool Add(T item)
        {
            if (dictionary.ContainsKey(item)) return false;
            list.Add(item);
            dictionary.Add(item, list.Count - 1);
            return true;
        }
    }
}
