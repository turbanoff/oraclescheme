using System;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using OracleScheme.DBObjects;

namespace OracleScheme
{
    static internal class OracleTypeUtils
    {
        public static ColumnType GetColumnType(OracleColumnType genericType, int? dataLength, int? dataPrecision, int? dataScale, char charUsed)
        {
            return new ColumnType(genericType, dataLength, dataPrecision, dataScale, charUsed);
        }

        public static OracleColumnType OracleGenericType(string dataType)
        {
            return (from fieldInfo in typeof (OracleColumnType).GetFields()
                    let attribute = (OracleColumnAttribute)Attribute.GetCustomAttribute(fieldInfo, typeof(OracleColumnAttribute))
                    where attribute != null
                    where dataType == attribute.Name || Regex.Match(dataType, '^' + attribute.RegexPattern + '$').Success
                    select (OracleColumnType) fieldInfo.GetValue(null)).FirstOrDefault();
        }

        public static string ToString(OracleColumnType columnType)
        {
            FieldInfo fieldInfo = typeof (OracleColumnType).GetField(columnType.ToString());
            OracleColumnAttribute attribute = (OracleColumnAttribute) Attribute.GetCustomAttribute(fieldInfo, typeof (OracleColumnAttribute));
            return attribute.Name.ToLower();
        }
    }
}