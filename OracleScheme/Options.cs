﻿using System;
using CommandLine;
using CommandLine.Text;

namespace OracleScheme
{
    public class CommonSubOptions
    {
        [Option('o', "output", HelpText = "SQL schema output file")]
        public string OutputSqlFile { get; set; }

        [Option('b', "binary-output", HelpText = "Binary output file")]
        public string OutputBinaryFile { get; set; }

        [Option("target-schema", HelpText = "Name of target schema. Useful only with SQL output type")]
        public string OutputSchemaName { get; set; }
    }

    public class OracleSourceOptions : CommonSubOptions
    {
        [Option('l', "login", HelpText = "Oracle connection login", MutuallyExclusiveSet = "splitted")]
        public string Login { get; set; }

        [Option('p', "password", HelpText = "Oracle connection password password", MutuallyExclusiveSet = "splitted")]
        public string Password { get; set; }

        [Option('d', "data-source", HelpText = "Oracle data source", MutuallyExclusiveSet = "splitted")]
        public string DataSource { get; set; }

        [Option('c', "connection-string", HelpText = "Oracle connection string", MutuallyExclusiveSet = "union")]
        public string ConnectionString { get; set; }

        [Option('s', "schema-name", HelpText = "Schema name")]
        public string SchemaName { get; set; }
    }

    public class FileSourceOptions : CommonSubOptions
    {
        [Option('i', "input", HelpText = "File with binary schema content", Required = true)]
        public string InputFile { get; set; }
    }

    /// <summary>Program command line options</summary>
    public class Options
    {
        [VerbOption("dump", HelpText = "Extract information from Oracle database")]
        public OracleSourceOptions OracleInputOptions { get; set; }

        [VerbOption("import", HelpText = "Extract informatin from existing dump file")]
        public FileSourceOptions FileInputOptions { get; set; }

        [HelpOption]
        public string GetUsage()
        {
            return HelpText.AutoBuild(this, current => HelpText.DefaultParsingErrorsHandler(this, current));
        }

        [HelpVerbOption]
        public string GetUsage(string verb)
        {
            return HelpText.AutoBuild(this, verb);
        }
    }
}
