using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using OracleScheme.DBObjects;

namespace OracleScheme
{
    public static class Mixin
    {
        public static StringBuilder AppendIdentifier(this StringBuilder sb, string identifier)
        {
            return sb.Append(identifier.Quote());
        }

        public static string Quote(this string identifier)
        {
            return IsNormalIdentifier(identifier) || (identifier[0] == '\"' && identifier[identifier.Length - 1] == '\"')
                       ? identifier
                       : '\"' + identifier + '\"';
        }

        public static string Upper(this string identifier)
        {
            string quotedName = identifier.Quote();
            if (!quotedName.StartsWith("\""))
                return quotedName.ToUpper();
            return quotedName;
        }

        public static StringBuilder AppendSchema(this StringBuilder sb, string schemaName)
        {
            if (schemaName != null)
                sb.Append(schemaName).Append('.');
            return sb;
        }

        private static bool IsNormalIdentifier(string identifier)
        {
            return Regex.IsMatch(identifier, @"^[A-Z][0-9A-Z_\$#]*$") && !IsKeyword(identifier);
        }


        private static readonly HashSet<string> Keywords = new HashSet<string>
            {
                "ALL", "ALTER", "AND", "ANY", "AS", "ASC", "BETWEEN", "BY", "CHAR", "CHECK", "CLUSTER", "COMPRESS", "CONNECT", "CREATE",
                "DATE", "DECIMAL", "DEFAULT", "DELETE", "DESC", "DISTINCT", "DROP", "ELSE", "EXCLUSIVE", "EXISTS", "FLOAT", "FOR", "FROM",
                "GRANT", "GROUP", "HAVING", "IDENTIFIED", "IN", "INDEX", "INSERT", "INTEGER", "INTERSECT", "INTO", "IS", "LIKE", "LOCK",
                "LONG", "MINUS", "MODE", "NOCOMPRESS", "NOT", "NOWAIT", "NULL", "NUMBER", "OF", "ON", "OPTION", "OR", "ORDER", "PCTFREE",
                "PRIOR", "PUBLIC", "RAW", "RENAME", "RESOURCE", "REVOKE", "SELECT", "SET", "SHARE", "SIZE", "SMALLINT", "START", "SYNONYM",
                "TABLE", "THEN", "TO", "TRIGGER", "UNION", "UNIQUE", "UPDATE", "VALUES", "VARCHAR", "VARCHAR2", "VIEW", "WHERE", "WITH"
            };

        private static bool IsKeyword(string identifier)
        {
            return Keywords.Contains(identifier);
        }

        public static void FindColumns(this IWithColumns table, List<TableColumn> oracleColumns)
        {
            table.Columns = oracleColumns.Where(column => column.TableName == table.Name).OrderBy(column => column.ColumnID).ToList();
            foreach (var tableColumn in table.Columns)
            {
                tableColumn.ParentTable = table;
            }
        }

        public static void FindDependecies(this WithDependencies main, List<WithDependencies> listOfAll, List<ObjectDependency> dependencies )
        {

            var result = (from d in dependencies
                          where d.Type == main.ObjectType && d.Name == main.Name
                          join one in listOfAll on new {Name = d.ReferencedName, Type = d.ReferencedType} equals
                              new {one.Name, Type = one.ObjectType}
                          select new {RefObject = one, Dependecy = d}).ToList();

            main.AddDependencies(result.Select(arg => arg.RefObject));
            dependencies.RemoveAll(dependecy => result.Any(arg => arg.Dependecy == dependecy));
        }
    }
}