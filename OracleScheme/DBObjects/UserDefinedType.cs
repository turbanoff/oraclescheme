﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace OracleScheme.DBObjects
{
    [Serializable]
    public class UserDefinedType : WithDependencies
    {
        public readonly UserTypeCode TypeCode;
        public readonly int AttrubutesCount;
        public readonly int MethodsCount;
        public readonly List<ObjectTypeAttribute> Attributes;
        public readonly List<ObjectTypeMethod> Methods;
        public CollectionElementType ElementType;
        public readonly string Owner;

        public override string ObjectType { get { return "TYPE"; } }

        public override int SortOrder { get { return 40; } }

        public UserDefinedType(string name, string typeCode, int attrubutesCount, int methodsCount)
        {
            this.Name = name;
            this.Owner = null;
            if (typeCode == "OBJECT")
            {
                this.TypeCode = UserTypeCode.ObjectType;
                Attributes = new List<ObjectTypeAttribute>();
                Methods = new List<ObjectTypeMethod>();
            }
            else if (typeCode == "COLLECTION")
            {
                this.TypeCode = UserTypeCode.CollectionType;
            }
            else
            {
                this.TypeCode = UserTypeCode.Undefined;
            }
            this.AttrubutesCount = attrubutesCount;
            this.MethodsCount = methodsCount;
        }
        
        /// <summary>Used only for table/view columns</summary>
        public UserDefinedType(string name, string owner)
        {
            this.Name = name;
            this.Owner = owner;

            this.TypeCode = UserTypeCode.Undefined;
            this.AttrubutesCount = 0;
            this.MethodsCount = 0;
            this.Name = name;
        }

        public void FindAttributes(List<ObjectTypeAttribute> allAttributes, List<UserDefinedType> userTypes)
        {
            Attributes.AddRange(allAttributes.Where(attribute => attribute.TypeName == Name).OrderBy(attribute => attribute.Position));
            foreach (ObjectTypeAttribute attribute in Attributes)
            {
                attribute.FindType(userTypes);
            }
        }

        public void FindElementType(List<CollectionElementType> collectionElementTypes, List<UserDefinedType> userTypes)
        {
            this.ElementType = collectionElementTypes.First(type => type.TypeName == Name);
            this.ElementType.FindType(userTypes);
        }

        public override void Visit(IOracleObjectVisitor visitor)
        {
            visitor.Visit(this);
        }

        public UserDefinedTypeBody FindBodyCode(Dictionary<SourceObjectKey, string> sourceLines)
        {
            SourceObjectKey key = new SourceObjectKey("TYPE BODY", Name);
            string bodyCode;
            if (sourceLines.TryGetValue(key, out bodyCode))
            {
                UserDefinedTypeBody typeBody = new UserDefinedTypeBody(Name, bodyCode);
                typeBody.AddDependency(this);
                sourceLines.Remove(key);
                return typeBody;
            }
            return null;
        }

        public void FindMethods(List<ObjectTypeMethod> objectTypeMethods)
        {
            Methods.AddRange(objectTypeMethods.Where(method => method.TypeName == Name).OrderBy(method => method.MethodNo));
            foreach (ObjectTypeMethod method in Methods)
            {
                objectTypeMethods.Remove(method);
            }
        }
    }

    public enum UserTypeCode
    {
        Undefined = 0,
        ObjectType,
        CollectionType
    }
}