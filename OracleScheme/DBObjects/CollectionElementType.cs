﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace OracleScheme.DBObjects
{
    [Serializable]
    public class CollectionElementType
    {
        public readonly ColumnType ElementType;
        public readonly string TypeName;
        public readonly CollectionType CollType;
        private string elementTypeName;
        public UserDefinedType UserDefinedElementType;
        public readonly int UpperBound;

        public CollectionElementType(string typeName, string collType, int? upperBound, string elemTypeName, int? length, int? precision, int? scale, char charUsed)
        {
            this.TypeName = typeName;
            switch (collType)
            {
                case "TABLE":
                    this.CollType = CollectionType.Table;
                    break;
                case "VARYING ARRAY":
                    this.CollType = CollectionType.Varray;
                    this.UpperBound = upperBound.Value;
                    break;
                default:
                    this.CollType = CollectionType.Undefined;
                    break;
            }
            OracleColumnType oracleGenericType = OracleTypeUtils.OracleGenericType(elemTypeName);
            if (oracleGenericType == OracleColumnType.Undefined)
            {
                this.elementTypeName = elemTypeName;
            }
            else
            {
                this.ElementType = OracleTypeUtils.GetColumnType(oracleGenericType, length, precision, scale, charUsed);
            }
            
        }

        public void FindType(List<UserDefinedType> userTypes)
        {
            if (elementTypeName != null)
            {
                UserDefinedElementType = userTypes.First(userType => userType.Name == elementTypeName);
                elementTypeName = null;
            }
        }

        public override string ToString()
        {
            if (UserDefinedElementType == null)
            {
                return ElementType.ToString();
            }
            else
            {
                return UserDefinedElementType.Name.Quote();
            }
        }
        public bool IsUserType()
        {
            return UserDefinedElementType != null;
        }
    }

    public enum CollectionType
    {
        Undefined = 0,
        Table,
        Varray
    }
}