﻿using System;

namespace OracleScheme.DBObjects
{
    [Serializable]
    public class IndexExpression : IIndexPart
    {
        public readonly int Position;
        public readonly string IndexName;
        public readonly string TableName;
        public readonly string ColumnExpression;

        public IndexExpression(string indexName, string tableName, string columnExpression, int position)
        {
            this.IndexName = indexName;
            this.TableName = tableName;
            this.ColumnExpression = columnExpression;
            this.Position = position;
        }

        public string Part
        {
            get { return ColumnExpression; }
        }
    }
}