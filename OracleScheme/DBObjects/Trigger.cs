﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace OracleScheme.DBObjects
{
    [Serializable]
    public class Trigger : WithDependencies
    {
        public readonly TriggerType Type;
        public readonly TriggeringEventAction TriggeringEvent;
        private readonly string TableName;
        public readonly string ReferencingNames;
        public readonly string WhenClause;
        public readonly bool Enabled;
        public readonly string Body;
        public readonly string BaseObjectType;
        public List<string> Columns { get; private set; }
        public WithDependencies TriggerTable { get; private set; }

        public Trigger(string name, string type, string triggeringEvent, string tableName, string baseObjectType, string referencingNames, string whenClause, bool enabled, string body)
        {
            switch (type)
            {
                case "BEFORE EACH ROW": Type = TriggerType.BeforeEachRow; break;
                case "AFTER EACH ROW": Type = TriggerType.AfterEachRow; break;
                case "BEFORE STATEMENT": Type = TriggerType.BeforeStatement; break;
                case "AFTER STATEMENT": Type = TriggerType.AfterStatement; break;
                case "INSTEAD OF": Type = TriggerType.InsteadOf; break;
                case "BEFORE EVENT": Type = TriggerType.BeforeEvent; break;
                case "AFTER EVENT": Type = TriggerType.AfterEvent; break;
                default: throw new ArgumentException("Unknown TRIGGER_TYPE: " + type + " for trigger " + name);
            }
            if (triggeringEvent.Contains("INSERT"))
                TriggeringEvent = TriggeringEventAction.Insert;
            if (triggeringEvent.Contains("UPDATE"))
                TriggeringEvent |= TriggeringEventAction.Update;
            if (triggeringEvent.Contains("DELETE"))
                TriggeringEvent |= TriggeringEventAction.Delete;

            if (triggeringEvent.Contains("CREATE"))
                TriggeringEvent |= TriggeringEventAction.Create;
            if (triggeringEvent.Contains("DDL"))
                TriggeringEvent |= TriggeringEventAction.DDL;
            if (triggeringEvent.Contains("ALTER"))
                TriggeringEvent |= TriggeringEventAction.Alter;
            if (triggeringEvent.Contains("TRUNCATE"))
                TriggeringEvent |= TriggeringEventAction.Truncate;
            if (triggeringEvent.Contains("RENAME"))
                TriggeringEvent |= TriggeringEventAction.Rename;
            if (triggeringEvent.Contains("DROP"))
                TriggeringEvent |= TriggeringEventAction.Drop;
            if (triggeringEvent.Contains("LOGON"))
                TriggeringEvent |= TriggeringEventAction.Logon;
            if (triggeringEvent.Contains("STARTUP"))
                TriggeringEvent |= TriggeringEventAction.Startup;

            Name = name;
            TableName = tableName;
            ReferencingNames = referencingNames;
            WhenClause = whenClause;
            Enabled = enabled;
            Body = body;
            BaseObjectType = baseObjectType;
        }

        public override string ObjectType { get { return "TRIGGER"; } }

        public override int SortOrder { get { return 90; } }

        public override void Visit(IOracleObjectVisitor visitor)
        {
            visitor.Visit(this);
        }

        public void FindColumns(List<TriggerColumn> triggerColumns)
        {
            List<TriggerColumn> cols = triggerColumns.Where(tr => tr.TriggerName == Name).ToList();
            this.Columns = new List<string>();
            foreach (TriggerColumn c in cols)
            {
                this.Columns.Add(c.ColumnName);
                triggerColumns.Remove(c);
            }
        }

        public void FindTable(List<Table> tables, List<View> views)
        {
            if (TableName != null)
            {
                TriggerTable = tables.Cast<WithDependencies>().Concat(views).FirstOrDefault(t => t.Name == TableName);
                if (TriggerTable != null)
                {
                    AddDependency(TriggerTable);
                }
            }
        }
    }

    public enum TriggerType
    {
        BeforeEachRow,
        AfterEachRow,
        BeforeStatement,
        AfterStatement,
        InsteadOf,
        BeforeEvent,
        AfterEvent
    }

    [Flags]
    public enum TriggeringEventAction
    {
        Insert = 1,
        Update = 2,
        Delete = 4,
        Create = 8,
        DDL = 16,
        Alter = 32,
        Truncate = 128,
        Rename = 64,
        Drop = 256,
        Logon = 512,
        Startup = 1024
    }
}