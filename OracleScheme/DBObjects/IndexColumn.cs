using System;

namespace OracleScheme.DBObjects
{
    [Serializable]
    public class IndexColumn
    {
        public readonly string IndexName;
        public readonly string Name;
        public readonly int Position;

        public IndexColumn(string indexName, string name, int position)
        {
            IndexName = indexName;
            Name = name;
            Position = position;
        }
    }
}