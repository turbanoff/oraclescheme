﻿using System;
using System.Collections.Generic;

namespace OracleScheme.DBObjects
{
    [Serializable]
    public class Package : WithDependencies
    {
        public string Code { get; private set; }

        public Package(string objectName)
        {
            this.Name = objectName;
        }

        /// <summary>Ищет свой код, и дополнительно создает объект с телом, если оно есть</summary>
        /// <param name="sourceLines">Общий набор исходных текстов</param>
        /// <returns>Body for corresponding package</returns>
        public PackageBody FindCode(Dictionary<SourceObjectKey, string> sourceLines)
        {
            SourceObjectKey packageKey = new SourceObjectKey("PACKAGE", Name);
            this.Code = sourceLines[packageKey];
            sourceLines.Remove(packageKey);

            SourceObjectKey bodyKey = new SourceObjectKey("PACKAGE BODY", Name);

            string bodyCode;
            if (sourceLines.TryGetValue(bodyKey, out bodyCode))
            {
                PackageBody packageBody = new PackageBody(Name, bodyCode);
                packageBody.AddDependency(this);
                sourceLines.Remove(bodyKey);
                return packageBody;
            }
            return null;
        }

        public override string ObjectType { get { return "PACKAGE"; } }

        public override int SortOrder { get { return 80; } }

        public override void Visit(IOracleObjectVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}