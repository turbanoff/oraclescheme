﻿namespace OracleScheme.DBObjects
{
    public interface IIndexPart
    {
        string Part { get; }
    }
}