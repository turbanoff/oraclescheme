﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace OracleScheme.DBObjects
{
    [Serializable]
    public class TablePartition
    {
        public readonly string TableName;
        public readonly string Name;
        public readonly int SubpartitionCount;
        public readonly string HighValue;
        public readonly int Position;
        public List<TableSubpartition> Subpartitions { get; private set; }

        public TablePartition(string tableName, string name, int subpartitionCount, string highValue, int position)
        {
            TableName = tableName;
            Name = name;
            SubpartitionCount = subpartitionCount;
            HighValue = highValue;
            Position = position;
        }

        public bool IsWithAutoName
        {
            get { return Regex.IsMatch(Name, @"^SYS_P\d{2,}$"); }
        }

        public void FindSubpartitions(List<TableSubpartition> subpartitions)
        {
            this.Subpartitions = subpartitions.Where(s => s.TableName == TableName && s.PartitionName == Name).OrderBy(s => s.Position).ToList();
        }
    }
}