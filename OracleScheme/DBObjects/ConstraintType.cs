namespace OracleScheme.DBObjects
{
    public enum ConstraintType
    {
        Unknown = 0,

        PrimaryKey,
        ForeignKey,
        Unique,
        Check
    }
}