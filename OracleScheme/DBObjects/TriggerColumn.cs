﻿using System;

namespace OracleScheme.DBObjects
{
    [Serializable]
    public class TriggerColumn
    {
        public readonly string TriggerName;
        public readonly string ColumnName;

        public TriggerColumn(string triggerName, string columnName)
        {
            TriggerName = triggerName;
            ColumnName = columnName;
        }
    }
}