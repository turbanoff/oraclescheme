﻿using System;
using System.Text.RegularExpressions;

namespace OracleScheme.DBObjects
{
    [Serializable]
    public class TableSubpartition
    {
        public readonly string TableName;
        public readonly string PartitionName;
        public readonly string Name;
        public readonly string HighValue;
        public readonly int Position;

        public TableSubpartition(string tableName, string partitionName, string name, string highValue, int position)
        {
            TableName = tableName;
            PartitionName = partitionName;
            Name = name;
            HighValue = highValue;
            Position = position;
        }

        public bool IsWithAutoName
        {
            get { return Regex.IsMatch(Name, @"^SYS_SUBP\d{3,}$"); }
        }
    }
}