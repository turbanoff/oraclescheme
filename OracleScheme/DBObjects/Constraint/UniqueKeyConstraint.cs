using System;

namespace OracleScheme.DBObjects.Constraint
{
    [Serializable]
    public sealed class UniqueKeyConstraint : ReferenceableConstraint
    {
        internal UniqueKeyConstraint(string name, string tableName)
            : base(name, tableName, ConstraintType.Unique)
        {
        }

        protected override string GetExactType
        {
            get { return "unique"; }
        }
    }
}