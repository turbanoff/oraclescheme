using System;
using System.Collections.Generic;
using System.Linq;

namespace OracleScheme.DBObjects.Constraint
{
    [Serializable]
    public sealed class ReferentialIntegrityConstraint : TableConstraint
    {
        private ReferenceableConstraint refTableConstraint;
        private readonly string refConstraintName;
        public bool IsCycleReferense { get; internal set; }
        private readonly ForeignKeyDeleteRule deleteRule;

        internal ReferentialIntegrityConstraint(string name, string tableName, string refConstraintName, string deleteRule)
            : base(name, tableName, ConstraintType.ForeignKey)
        {
            this.refConstraintName = refConstraintName;
            switch (deleteRule)
            {
                case "NO ACTION":
                    this.deleteRule = ForeignKeyDeleteRule.NoAction;
                    break;
                case "CASCADE":
                    this.deleteRule = ForeignKeyDeleteRule.Cascade;
                    break;
                case "SET NULL":
                    this.deleteRule = ForeignKeyDeleteRule.SetNull;
                    break;
                default:
                    throw new ArgumentException("Unknown DELETE_RULE: " + deleteRule + " for constraint " + refConstraintName);
            }
            
        }

        public override string GetDDL()
        {
            if (refTableConstraint == null)
                return "TODO: omit non-finded constraint";

            return "foreign key (" + string.Join(",", QuotedColumns) + ") " + GetColumnDDL(); 
        }

        public override string GetColumnDDL()
        {
            string s = "references " + refTableConstraint.TableName.Quote() + "(" + string.Join(",", refTableConstraint.QuotedColumns) + ")";
            switch (deleteRule)
            {
                case ForeignKeyDeleteRule.Cascade:
                    return s + " on delete cascade";
                case ForeignKeyDeleteRule.SetNull:
                    return s + " on delete set null";
                default:
                    return s;
            }
        }

        public void FindRefConstraint(List<TableConstraint> allSchemaConstraints)
        {
            refTableConstraint = (ReferenceableConstraint) allSchemaConstraints.FirstOrDefault(
                constraint =>
                constraint.Name == refConstraintName &&
                (constraint.ConstraintType == ConstraintType.PrimaryKey || constraint.ConstraintType == ConstraintType.Unique)
                                                            );
        }

        public ReferenceableConstraint RefTableConstraint
        {
            get { return refTableConstraint; }
        }
    }

    public enum ForeignKeyDeleteRule
    {
        NoAction,
        Cascade,
        SetNull
    }
}