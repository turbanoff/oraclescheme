using System;

namespace OracleScheme.DBObjects.Constraint
{
    [Serializable]
    public sealed class PrimaryKeyConstraint : ReferenceableConstraint
    {
        public PrimaryKeyConstraint(string name, string tableName)
            : base(name, tableName, ConstraintType.PrimaryKey)
        {
        }

        protected override string GetExactType
        {
            get { return "primary key"; }
        }
    }
}