﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;

namespace OracleScheme.DBObjects.Constraint
{
    [Serializable]
    [DebuggerDisplay("Table {TableName} Name {Name} Type {ConstraintType}")]
    public abstract class TableConstraint
    {
        public readonly string Name;
        public readonly string TableName;
        public readonly ConstraintType ConstraintType;
        private string IndexName;
        public bool IsEnabled { get; private set; }
        public TableIndex Index { get; private set; }
        public Table ParentTable { get; set; }
        protected internal List<TableColumn> Columns { get; private set; }
        protected internal List<ConstraintColumn> NonUsedColumns { get; private set; }
        protected internal IEnumerable<string> QuotedColumns
        {
            get { return Columns.Select(column => column.ToString().Quote()); } 
        }

        protected TableConstraint(string name, string tableName, ConstraintType constraintType)
        {
            this.Name = name;
            this.TableName = tableName;
            this.ConstraintType = constraintType;
        }

        public bool IsCanBeInlined
        {
            get { return Columns.Count == 1 && IsWithAutoName && (Index == null || Index.IsWithAutoName);  }
        }

        protected bool IsPKOrganizaionIndex
        {
            get { return Regex.IsMatch(Name, @"^SYS_IOT_TOP_\d{5,}$"); }
        }

        public bool IsWithAutoName
        {
            get { return Regex.IsMatch(Name, @"^SYS_C\d{6,}$") || IsPKOrganizaionIndex; }
        }

        public abstract string GetDDL();

        public void FindColumns(List<ConstraintColumn> tableConstraints, List<TableColumn> columns)
        {
            this.Columns = new List<TableColumn>(columns.Count);
            this.NonUsedColumns = new List<ConstraintColumn>(0);

            var constraintColumns = tableConstraints.Where(c => c.ConstraintName == this.Name).OrderBy(c => c.Positon);
            foreach (ConstraintColumn constraintColumn in constraintColumns)
            {
                TableColumn firstOrDefault = columns.FirstOrDefault(column => column.Name == constraintColumn.Name);
                if (firstOrDefault != null)
                {
                    this.Columns.Add(firstOrDefault);
                }
                else
                {
                    this.NonUsedColumns.Add(constraintColumn);
                }
            }
        }

        public static TableConstraint Create(IDataRecord record)
        {
            char constraintType = record.GetChar("CONSTRAINT_TYPE");
            string name = record.GetString("CONSTRAINT_NAME");
            string tableName = record.GetString("TABLE_NAME");

            TableConstraint result;
            switch (constraintType)
            {
                case 'R':
                    result = new ReferentialIntegrityConstraint(name, tableName, record.GetString("R_CONSTRAINT_NAME"), record.GetString("DELETE_RULE"));
                    break;
                case 'U':
                    result = new UniqueKeyConstraint(name, tableName);
                    break;
                case 'P':
                    result = new PrimaryKeyConstraint(name, tableName);
                    break;
                case 'C':
                    result = new CheckConstraint(name, tableName, record.GetString("SEARCH_CONDITION"));
                    break;
                //TODO: 'O' and 'F' constraints
                default:
                    result = new UnknownConstraint(name, tableName);
                    break;
            }
            result.IndexName = record.GetString("INDEX_NAME");
            result.IsEnabled = record.GetString("STATUS") == "ENABLED";
            return result;
        }

        public void FindIndex(List<TableIndex> indexes)
        {
            if (IndexName != null)
                Index = indexes.FirstOrDefault(index => index.Name == IndexName);
        }

        public abstract string GetColumnDDL();
    }
}