using System;
using System.Text.RegularExpressions;

namespace OracleScheme.DBObjects.Constraint
{
    [Serializable]
    public sealed class CheckConstraint : TableConstraint
    {
        public readonly string CheckText;
        private bool? notNullConstraint = null;

        public bool IsNotNull
        {
            get
            {
                if (notNullConstraint == null)
                {
                    notNullConstraint = Columns.Count == 1 && Regex.IsMatch(CheckText, @"^\s*""?" + Columns[0].Name + @"""?\s+is\s+not\s+null\s*$", RegexOptions.IgnoreCase);
                }
                return notNullConstraint.Value;
            }
        }
        

        public CheckConstraint(string name, string tableName, string checkText)
            : base(name, tableName, ConstraintType.Check)
        {
            this.CheckText = checkText;
        }

        public override string GetDDL()
        {
            string ddl = IsNotNull ? "check (" + Columns[0].Name.Quote() + " is not null)" : GenerateDefaultDDL();
            if (IsEnabled)
            {
                return ddl;
            }
            else
            {
                return ddl + " disable";
            }
        }

        private string GenerateDefaultDDL()
        {
            return "check (" + CheckText.Trim() + ")";
        }

        public override string GetColumnDDL()
        {
            string ddl = IsNotNull ? "not null" : GenerateDefaultDDL();
            if (IsEnabled)
            {
                return ddl;
            }
            else
            {
                return ddl + " disable";
            }
        }
    }
}