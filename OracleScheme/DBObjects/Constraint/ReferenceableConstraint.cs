﻿using System;

namespace OracleScheme.DBObjects.Constraint
{
    /// <summary>Unique or Primary key</summary>
    [Serializable]
    public abstract class ReferenceableConstraint : TableConstraint
    {
        protected ReferenceableConstraint(string name, string tableName, ConstraintType constraintType)
            : base(name, tableName, constraintType)
        {
        }

        public sealed override string GetDDL()
        {
            return GetExactType + " (" + string.Join(",", QuotedColumns) + ")";
        }

        public sealed override string GetColumnDDL()
        {
            return GetExactType;
        }

        protected abstract string GetExactType { get; }
    }
}