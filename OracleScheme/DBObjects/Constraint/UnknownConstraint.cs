using System;

namespace OracleScheme.DBObjects.Constraint
{
    [Serializable]
    public sealed class UnknownConstraint : TableConstraint
    {
        public UnknownConstraint(string name, string tableName)
            : base(name, tableName, ConstraintType.Unknown)
        {
        }

        public override string GetDDL()
        {
            return "unknown (" + string.Join(",", QuotedColumns) + ')';
        }

        public override string GetColumnDDL()
        {
            return "unknown";
        }
    }
}