using System.Collections.Generic;

namespace OracleScheme.DBObjects
{
    public interface IWithColumns
    {
        string Name { get; }
        List<TableColumn> Columns { get; set; }
    }
}