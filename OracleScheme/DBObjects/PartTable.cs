﻿using System;

namespace OracleScheme.DBObjects
{
    [Serializable]
    public class PartTable
    {
        public readonly string TableName;
        public readonly string PartitioningType;
        public readonly string SubpartitioningType;
        public readonly string RefConstraintName;
        public readonly string Interval;

        public PartTable(string tableName, string partitioningType, string subpartitioningType, string refConstraintName, string interval)
        {
            TableName = tableName;
            PartitioningType = partitioningType;
            SubpartitioningType = subpartitioningType == "NONE" ? null : subpartitioningType;
            RefConstraintName = refConstraintName;
            Interval = interval;
        }
    }
}