﻿using System;

namespace OracleScheme.DBObjects
{
    [Serializable]
    public class DatabaseLink : WithDependencies
    {
        public readonly string Username;
        public readonly string Host;

        public DatabaseLink(string name, string username, string host)
        {
            Name = name;
            Username = username;
            Host = host;
        }

        public override string ObjectType { get { return "DATABASE LINK"; } }

        public override int SortOrder { get { return 10; } }

        public override void Visit(IOracleObjectVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}