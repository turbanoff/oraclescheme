﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;

namespace OracleScheme.DBObjects
{
    [Serializable]
    [DebuggerDisplay("{Name}, Table = {TableName}, Unique {IsUnique}, Type {Type}")]
    public sealed class TableIndex
    {
        public readonly string Name;
        public readonly IndexType Type;
        public readonly string TableName;
        public readonly bool IsUnique;
        public List<IIndexPart> Columns { get; internal set; }

        public Table ParentTable { get; internal set; }

        public TableIndex(string name, string tableName, string indexType, bool isUnique)
        {
            this.Name = name;
            this.TableName = tableName;
            this.Type = GetIndexType(indexType);
            this.IsUnique = isUnique;
        }

        public bool IsWithAutoName
        {
            get { return Regex.Match(Name, @"SYS_C\d{6,}").Success && Columns.Count == 1; }
        }

        public bool IsFunctionalBased
        {
            get { return this.Type == IndexType.FunctionBasedNormal; }
        }

        public bool IsBitmap
        {
            get { return this.Type == IndexType.Bitmap; }
        }

        private static IndexType GetIndexType(string indexType)
        {
            switch (indexType)
            {
                case "NORMAL":
                    return IndexType.Normal;
                case "BITMAP":
                    return IndexType.Bitmap;
                case "FUNCTION-BASED NORMAL":
                    return IndexType.FunctionBasedNormal;
                default:
                    return IndexType.Unknown;
            }
        }

        private class IndexPartWithPosition
        {
            public readonly int Position;
            public readonly IIndexPart IndexPart;

            internal IndexPartWithPosition(int position, IIndexPart indexPart)
            {
                this.Position = position;
                this.IndexPart = indexPart;
            }
        }

        public void FindColumns(List<IndexColumn> indexColumns, List<TableColumn> columns, List<IndexExpression> indexExpressions)
        {
            //TODO: проверить, если IndexExpression состоит из одного столбца
            var tableColumns = from col in columns
                               where col.ParentTable == this.ParentTable
                               join indCol in indexColumns on col.Name equals indCol.Name
                               where indCol.IndexName == this.Name
                               orderby indCol.Position
                               select new IndexPartWithPosition(indCol.Position, col);

            var expressions = indexExpressions
                                .Where(expression => expression.IndexName == this.Name && expression.TableName == this.TableName)
                                .Select(expression => new IndexPartWithPosition(expression.Position, expression));

            this.Columns = tableColumns.Concat(expressions).OrderBy(indexPart => indexPart.Position).Select(indexPart => indexPart.IndexPart).ToList();
        }
    }

    public enum IndexType
    {
        Unknown = 0,
        Normal, 
        Bitmap,
        FunctionBasedNormal
    }
}