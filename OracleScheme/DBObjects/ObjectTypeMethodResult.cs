﻿using System;

namespace OracleScheme.DBObjects
{
    [Serializable]
    public class ObjectTypeMethodResult
    {
        public readonly string TypeName;
        public readonly string MethodName;
        public readonly int MethodNo;
        public readonly string ResultTypeName;

        public ObjectTypeMethodResult(string typeName, string methodName, int methodNo, string resultTypeName)
        {
            TypeName = typeName;
            MethodName = methodName;
            MethodNo = methodNo;
            OracleColumnType genericType = OracleTypeUtils.OracleGenericType(resultTypeName);
            ResultTypeName = genericType == OracleColumnType.Undefined ? resultTypeName : OracleTypeUtils.ToString(genericType);
        }
    }
}