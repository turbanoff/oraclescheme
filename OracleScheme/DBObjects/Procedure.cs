﻿using System;
using System.Collections.Generic;

namespace OracleScheme.DBObjects
{
    [Serializable]
    public class Procedure : WithDependencies
    {
        public string Code { get; private set; }

        public Procedure(string name)
        {
            Name = name;
        }

        public void FindCode(Dictionary<SourceObjectKey, string> sourceLines)
        {
            SourceObjectKey key = new SourceObjectKey("PROCEDURE", Name);
            this.Code = sourceLines[key];
            sourceLines.Remove(key);
        }

        public override string ObjectType { get { return "PROCEDURE"; } }

        public override int SortOrder { get { return 60; } }

        public override void Visit(IOracleObjectVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}