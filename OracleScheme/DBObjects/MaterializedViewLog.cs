﻿using System;

namespace OracleScheme.DBObjects
{
    [Serializable]
    public class MaterializedViewLog
    {
        public readonly string MasterTableName;
        public readonly string LogTableName;
        public readonly bool IsWithPrimaryKey;
        public readonly bool IsWithRowId;
        public readonly bool IsIncludeNewValues;
        public readonly bool IsWithSequence;

        public MaterializedViewLog(string masterTableName, string logTableName, string tempLogTableName, bool isWithRowId, bool isWithPrimaryKey, bool isWithSequence, bool isIncludeNewValues)
        {
            MasterTableName = masterTableName;
            LogTableName = logTableName;
            TempLogTableName = tempLogTableName;
            IsWithPrimaryKey = isWithPrimaryKey;
            IsWithRowId = isWithRowId;
            IsIncludeNewValues = isIncludeNewValues;
            IsWithSequence = isWithSequence;
        }

        public string TempLogTableName { get; set; }

        public Table MasterTable { get; set; }
    }
}