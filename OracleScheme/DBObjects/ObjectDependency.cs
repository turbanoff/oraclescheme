﻿using System;

namespace OracleScheme.DBObjects
{
    [Serializable]
    public class ObjectDependency
    {
        public readonly string Name;
        public readonly string Type;
        public readonly string ReferencedName;
        public readonly string ReferencedType;

        public ObjectDependency(string name, string type, string referencedName, string referencedType)
        {
            Name = name;
            Type = type;
            ReferencedName = referencedName;
            ReferencedType = referencedType;
        }

        public override string ToString()
        {
            return string.Format("Name: {0}, Type: {1}, ReferencedName: {2}, ReferencedType: {3}", Name, Type, ReferencedName, ReferencedType);
        }

        public string ToStringWithoutRefType()
        {
            return string.Format("Name: {0}, Type: {1}, ReferencedName: {2}", Name, Type, ReferencedName);
        }
    }
}