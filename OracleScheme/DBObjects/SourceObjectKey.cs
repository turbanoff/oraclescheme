﻿using System;

namespace OracleScheme.DBObjects
{
    public struct SourceObjectKey
    {
        public readonly string ObjectType;
        public readonly string ObjectName;

		public SourceObjectKey(string ObjectType, string ObjectName)
		{
			this.ObjectType = ObjectType;
			this.ObjectName = ObjectName;
		}
		
        public override bool Equals(object obj)
        {
			return (obj is SourceObjectKey) && Equals((SourceObjectKey)obj);
        }

		public bool Equals(SourceObjectKey other)
		{
			return this.ObjectType == other.ObjectType && this.ObjectName == other.ObjectName;
		}

		public override int GetHashCode()
		{
			int hashCode = 0;
			unchecked {
				if (ObjectType != null)
					hashCode += 1000000007 * ObjectType.GetHashCode();
				if (ObjectName != null)
					hashCode += 1000000009 * ObjectName.GetHashCode();
			}
			return hashCode;
		}
    }
}
