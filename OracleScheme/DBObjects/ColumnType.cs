using System;

namespace OracleScheme.DBObjects
{
    [AttributeUsage(AttributeTargets.Field)]
    public class OracleColumnAttribute : Attribute
    {
        public readonly string Name;
        public readonly string RegexPattern;

        public OracleColumnAttribute(string name)
        {
            this.Name = name;
            this.RegexPattern = name;
        }

        public OracleColumnAttribute(string name, string regexPattern)
        {
            this.Name = name;
            this.RegexPattern = regexPattern;
        }
    }
    
    /// <summary>���� �� ���������������� ����� Oracle</summary>
    [Serializable]
    public class ColumnType
    {
        private readonly OracleColumnType columnType;
        private readonly int dataLength;
        private readonly int? dataPrecision;
        private readonly int? dataScale;
        private readonly char charUsed;

        public ColumnType(OracleColumnType oracleGenericType, int? length, int? precision, int? scale, char charUsed)
        {
            this.columnType = oracleGenericType;
            if (length == null)
            {
                this.dataLength = 22;
                this.dataScale = scale ?? 0;
            }
            else
            {
                this.dataLength = length.Value;
                this.dataScale = scale;
            }
            this.dataPrecision = precision;
            this.charUsed = charUsed;
        }

        public override string ToString()
        {
            string genericTypeName = columnType.ToString().ToLower();
            switch (columnType)
            {
                case OracleColumnType.Number:
                    if (dataLength == 22 && dataPrecision == null && dataScale == 0)
                        return "integer";
                    if (dataLength == 22 && dataPrecision == null && dataScale == null)
                        return "number";
                    if (dataPrecision == null || dataScale == null)
                        return "number(undefined)";
                    if (dataScale == 0)
                        return "number(" + dataPrecision + ")";
                    return "number(" + dataPrecision + "," + dataScale + ")";
                case OracleColumnType.Raw:
                    return "raw(" + dataLength + ")";
                case OracleColumnType.Timestamp:
                    if (dataScale == 6)
                        return "timestamp";
                    return "timestamp(" + dataScale + ")";
                case OracleColumnType.TimestampWithTimeZone:
                    if (dataScale == 6)
                        return "timestamp with time zone";
                    return "timestamp(" + dataScale + ") with time zone";
                case OracleColumnType.TimestampWithLocalTimeZone:
                    if (dataScale == 6)
                        return "timestamp with local time zone";
                    return "timestamp(" + dataScale + ") with local time zone";
                case OracleColumnType.Varchar2:
                case OracleColumnType.Char:
                    return genericTypeName + "(" + dataLength + " " + (charUsed == 'B' ? "byte" : "char") + ")";
                case OracleColumnType.NVarchar2:
                case OracleColumnType.NChar:
                    return genericTypeName + "(" + dataLength + ")";
                case OracleColumnType.LongRaw:
                    return "long raw";
                case OracleColumnType.Float:
                    if (dataPrecision == 126)
                        return "float";
                    return "float(" + dataPrecision + ")";
                case OracleColumnType.IntervalDayToSecond:
                    if (dataPrecision == 2 && dataScale == 6)
                        return "interval day to second";
                    if (dataPrecision == 2 && dataScale != 6)
                        return "interval day to second(" +  dataScale+ ")";
                    if (dataPrecision != 2 && dataScale == 6)
                        return "interval day(" + dataPrecision + ") to second";
                    return "interval day(" + dataPrecision + ") to second(" + dataScale + ")";
                case OracleColumnType.IntervalYearToMonth:
                    if (dataPrecision == 2)
                        return "interval year to month";
                    return "interval year(" + dataPrecision + ") to month";
                case OracleColumnType.BinaryDouble:
                    return "binary_double";
                case OracleColumnType.BinaryFloat:
                    return "binary_float";
                default:
                    return genericTypeName;
            }
        }
    }

    public enum OracleColumnType
    {
        Undefined = 0,

        [OracleColumn("CHAR")]
        Char,
        [OracleColumn("VARCHAR2")]
        Varchar2,
        [OracleColumn("NCHAR")]
        NChar,
        [OracleColumn("NVARCHAR2")]
        NVarchar2,

        [OracleColumn("NUMBER", @"(NUMBER|INTEGER)")]
        Number,
        [OracleColumn("BINARY_FLOAT")]
        BinaryFloat,
        [OracleColumn("BINARY_DOUBLE")]
        BinaryDouble,
        [OracleColumn("FLOAT")]
        Float,

        [OracleColumn("DATE")]
        Date,
        [OracleColumn("TIMESTAMP", @"TIMESTAMP(\(\d+\))?")]
        Timestamp,
        [OracleColumn("TIMESTAMP WITH TIME ZONE", @"TIMESTAMP(\(\d+\))? WITH TIME ZONE")]
        TimestampWithTimeZone,
        [OracleColumn("TIMESTAMP WITH LOCAL TIME ZONE", @"TIMESTAMP(\(\d+\))? WITH LOCAL TIME ZONE")]
        TimestampWithLocalTimeZone,

        [OracleColumn("BLOB")]
        Blob,
        [OracleColumn("CLOB")]
        Clob,
        [OracleColumn("NCLOB")]
        NClob,

        [OracleColumn("BFILE")]
        BFile,

        [OracleColumn("RAW")]
        Raw,
        [OracleColumn("LONG RAW")]
        LongRaw,
        [OracleColumn("LONG")]
        Long,

        [OracleColumn("ROWID")]
        RowID,
        [OracleColumn("UROWID")]
        URowID,

        [OracleColumn("INTERVAL DAY TO SECOND", @"INTERVAL DAY(\(\d+\))? TO SECOND(\(\d+\))?")]
        IntervalDayToSecond,
        [OracleColumn("INTERVAL YEAR TO MONTH", @"INTERVAL YEAR(\(\d+\))? TO MONTH(\(\d+\))?")]
        IntervalYearToMonth,
    }
}