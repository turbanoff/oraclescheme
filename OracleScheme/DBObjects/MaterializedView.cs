﻿using System;
using System.Collections.Generic;

namespace OracleScheme.DBObjects
{
    [Serializable]
    public class MaterializedView : WithDependencies
    {
        public readonly string ContainerName;
        public readonly bool IsRewriteEnabled;
        public readonly string Query;
        public readonly string RefreshMode;
        public readonly string RefreshMethod;
        public readonly string BuildMode;
        public Table Container { get; set; }

        public MaterializedView(string name, string containerName, string query, bool isRewriteEnabled, string refreshMode, string refreshMethod, string buildMode)
        {
            this.Name = name;
            this.ContainerName = containerName;
            this.Query = query;
            this.IsRewriteEnabled = isRewriteEnabled;
            this.RefreshMode = refreshMode;
            this.RefreshMethod = refreshMethod;
            this.BuildMode = buildMode;
        }

        public override string ObjectType { get { return "MATERIALIZED VIEW"; } }

        public override int SortOrder { get { return 120; } }

        public override void Visit(IOracleObjectVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}