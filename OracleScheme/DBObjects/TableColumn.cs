using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace OracleScheme.DBObjects
{
    [Serializable]
    [DebuggerDisplay("Table {TableName}. Name {Name}. Type {Type}")]
    public sealed class TableColumn : IIndexPart
    {
        public readonly string TableName;
        public readonly string Name;
        public readonly int ColumnID;
        public readonly ColumnType Type;
        public readonly string DefaultValue;
        public readonly bool IsVirtual;
        public IWithColumns ParentTable { get; set; }
        private string dataType;
        private string typeOwner;
        public UserDefinedType UserType;
        public UserNestedTable StoreTable;

        public TableColumn(string name, string dataType, string typeOwner, string tableName, int columnID, int dataLength, int? dataPrecision, int? dataScale, char charUsed, string defaultValue, bool virtualColumn)
        {
            this.Name = name;
            this.TableName = tableName;
            this.ColumnID = columnID;
            this.DefaultValue = defaultValue;
            this.IsVirtual = virtualColumn;
            OracleColumnType oracleGenericType = OracleTypeUtils.OracleGenericType(dataType);
            if (oracleGenericType != OracleColumnType.Undefined)
            {
                this.Type = OracleTypeUtils.GetColumnType(oracleGenericType, dataLength, dataPrecision, dataScale, charUsed);
            }
            else
            {
                this.dataType = dataType;
                this.typeOwner = typeOwner;
            }
        }

        public override string ToString()
        {
            return this.Name;
        }

        public string Part
        {
            get { return Name; }
        }

        public bool IsUserDefinedType
        {
            get { return Type == null; }
        }

        public bool IsNestedTable
        {
            get { return IsUserDefinedType && UserType.TypeCode == UserTypeCode.CollectionType; }
        }

        public void FindType(List<UserDefinedType> userTypes, string user)
        {
            if (dataType != null)
            {
                if (user == typeOwner)
                {
                    UserType = userTypes.SingleOrDefault(type => type.Name == dataType);
                }
                else
                {
                    UserType = new UserDefinedType(dataType, typeOwner);
                }
                dataType = null;
            }
        }

        public string GetTypePartDDL()
        {
            if (!IsUserDefinedType)
                return Type.ToString();

            if (UserType.Owner == null)
                return UserType.Name.Quote();

            return UserType.Owner.Quote() + "." + UserType.Name.Quote();
        }

        public void FindStoreTable(List<Table> tables, List<UserNestedTable> nestedTables)
        {
            StoreTable = nestedTables.First(table => table.ParentTableName == TableName && table.ParentTableColumn == Name);
            var t = tables.FirstOrDefault(table => table.Name == StoreTable.TableName);
            if (t != null) {
                t.IsNestedStoreTable = true;
            }
        }
    }
}