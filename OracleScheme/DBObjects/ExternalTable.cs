﻿using System;

namespace OracleScheme.DBObjects
{
    [Serializable]
    public class ExternalTable
    {
        public readonly string TableName;
        public readonly string TypeName;
        public readonly string DefaultDirectoryName;
        public readonly string AccessParameters;
        public readonly string Location;
        public readonly string DirectoryName;

        public ExternalTable(string tableName, string typeName, string defaultDirectoryName, string accessParameters, string location, string directoryName)
        {
            TableName = tableName;
            TypeName = typeName;
            DefaultDirectoryName = defaultDirectoryName;
            AccessParameters = accessParameters;
            Location = location;
            DirectoryName = directoryName;
        }
    }
}