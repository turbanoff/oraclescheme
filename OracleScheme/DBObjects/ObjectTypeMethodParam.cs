﻿using System;

namespace OracleScheme.DBObjects
{
    [Serializable]
    public class ObjectTypeMethodParam
    {
        public readonly string TypeName;
        public readonly string MethodName;
        public readonly int MethodNo;
        public readonly string ParamName;
        public readonly int ParamNo;
        public readonly string ParamMode;
        public readonly string ParamTypeName;

        public ObjectTypeMethodParam(string typeName, string methodName, int methodNo, string paramName, int paramNo, string paramMode, string paramTypeName)
        {
            TypeName = typeName;
            MethodName = methodName;
            MethodNo = methodNo;
            ParamName = paramName;
            ParamNo = paramNo;
            ParamMode = paramMode;
            OracleColumnType genericType = OracleTypeUtils.OracleGenericType(paramTypeName);
            ParamTypeName = genericType == OracleColumnType.Undefined ? paramTypeName : OracleTypeUtils.ToString(genericType);
        }
    }
}