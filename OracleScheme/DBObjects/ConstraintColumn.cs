using System;
using System.Text.RegularExpressions;

namespace OracleScheme.DBObjects
{
    [Serializable]
    public class ConstraintColumn
    {
        public readonly string Name;
        public readonly string ConstraintName;
        public readonly string TableName;
        public readonly int? Positon;

        public ConstraintColumn(string name, string tableName, string constraintName, int? positon)
        {
            Name = name;
            TableName = tableName;
            ConstraintName = constraintName;
            Positon = positon;
        }

        public bool IsWithAutoNestedName
        {
            get { return Regex.IsMatch(Name, @"^SYS_NC\d{10,}\$$"); }
        }
    }
}