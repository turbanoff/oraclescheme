﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace OracleScheme.DBObjects
{
    [DebuggerDisplay("{ObjectType} = {Name}")]
    [Serializable]
    public abstract class WithDependencies
    {
        public abstract string ObjectType { get; }
        public string Name { get; protected set; }
        public abstract int SortOrder { get; }

        private readonly OrderedSet<WithDependencies> dependencies = new OrderedSet<WithDependencies>(); 

        public void AddDependency(WithDependencies newDependency)
        {
            dependencies.Add(newDependency);
        }

        public void AddDependencies(IEnumerable<WithDependencies> newDepencies)
        {
            foreach (var newDependency in newDepencies)
            {
                dependencies.Add(newDependency);
            }
        }

        public IEnumerable<WithDependencies> Dependencies
        {
            get { return dependencies; }
        }

        public abstract void Visit(IOracleObjectVisitor visitor);
    }
}