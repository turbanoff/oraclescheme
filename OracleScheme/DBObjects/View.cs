﻿using System;
using System.Collections.Generic;

namespace OracleScheme.DBObjects
{
    [Serializable]
    public class View : WithDependencies, IWithColumns
    {
        public override string ObjectType { get { return "VIEW"; } }

        public override int SortOrder { get { return 50; } }

        public readonly string SelectText;
        public List<TableColumn> Columns { get; set; }
        
        public View(string name, string selectText)
        {
            this.Name = name;
            this.SelectText = selectText;
        }

        public override void Visit(IOracleObjectVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}