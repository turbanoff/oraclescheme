﻿using System;

namespace OracleScheme.DBObjects
{
    [Serializable]
    public class Sequence : WithDependencies
    {
        public readonly decimal MinValue;
        public readonly decimal MaxValue;
        public readonly decimal IncrementBy;
        public readonly bool IsCycle;
        public readonly bool IsOrdered;
        public readonly decimal CacheSize;
        public readonly decimal LastNumber;

        public Sequence(string name, decimal minValue, decimal maxValue, decimal incrementBy, bool isCycle, bool isOrdered, decimal cacheSize, decimal lastNumber)
        {
            this.Name = name;
            this.MinValue = minValue;
            this.MaxValue = maxValue;
            this.IncrementBy = incrementBy;
            this.IsCycle = isCycle;
            this.IsOrdered = isOrdered;
            this.CacheSize = cacheSize;
            this.LastNumber = lastNumber;
        }

        public override string ObjectType { get { return "SEQUENCE"; } }

        public override int SortOrder { get { return 20; } }

        public override void Visit(IOracleObjectVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}