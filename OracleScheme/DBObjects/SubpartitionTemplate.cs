﻿using System;

namespace OracleScheme.DBObjects
{
    [Serializable]
    public class SubpartitionTemplate
    {
        public readonly string TableName;
        public readonly string Name;
        public readonly int Position;
        public readonly string HighBound;

        public SubpartitionTemplate(string tableName, string name, int position, string highBound)
        {
            TableName = tableName;
            Name = name;
            Position = position;
            HighBound = highBound;
        }
    }
}