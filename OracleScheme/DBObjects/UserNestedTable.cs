using System;

namespace OracleScheme.DBObjects
{
    [Serializable]
    public class UserNestedTable
    {
        public readonly string TableName;
        public readonly string ParentTableName;
        public readonly string ParentTableColumn;

        public UserNestedTable(string tableName, string parentTableName, string parentTableColumn)
        {
            this.TableName = tableName;
            this.ParentTableName = parentTableName;
            this.ParentTableColumn = parentTableColumn;
        }
    }
}