﻿using System;

namespace OracleScheme.DBObjects
{
    [Serializable]
    public class SubpartitionKeyColumn
    {
        public readonly string Name;
        public readonly string ObjectType;
        public readonly string ColumnName;
        public readonly int Postition;

        public SubpartitionKeyColumn(string name, string objectType, string columnName, int postition)
        {
            Name = name;
            ObjectType = objectType;
            ColumnName = columnName;
            Postition = postition;
        }
    }
}