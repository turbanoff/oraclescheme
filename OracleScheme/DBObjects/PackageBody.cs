﻿using System;

namespace OracleScheme.DBObjects
{
    [Serializable]
    public class PackageBody : WithDependencies
    {
        public override string ObjectType { get { return "PACKAGE BODY"; } }

        public override int SortOrder { get { return 100; } }

        public PackageBody(string name, string bodyCode)
        {
            this.Name = name;
            this.Code = bodyCode;
        }

        public string Code { get; private set; }

        public override void Visit(IOracleObjectVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}