﻿using System;

namespace OracleScheme.DBObjects
{
    [Serializable]
    public class UserDefinedTypeBody : WithDependencies
    {
        public readonly string Code;

        internal UserDefinedTypeBody(string name, string code)
        {
            this.Name = name;
            this.Code = code;
        }

        public override string ObjectType { get { return "TYPE BODY"; } }

        public override int SortOrder { get { return 110; } }

        public override void Visit(IOracleObjectVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}