﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace OracleScheme.DBObjects
{
    [Serializable]
    public class ObjectTypeMethod
    {
        public readonly string TypeName;
        public readonly string Name;
        public readonly int MethodNo;
        public readonly string MethodType;
        public readonly bool IsFunction;
        public readonly int ParametersCount;


        public ObjectTypeMethod(string typeName, string name, int methodNo, string methodType, bool isFunction, int parametersCount)
        {
            TypeName = typeName;
            Name = name;
            MethodNo = methodNo;
            MethodType = methodType;
            IsFunction = isFunction;
            ParametersCount = parametersCount;
        }

        public void FindParams(List<ObjectTypeMethodParam> parameters)
        {
            this.Parameters = parameters.Where(param => param.TypeName == TypeName && param.MethodName == Name && param.MethodNo == MethodNo).OrderBy(param => param.ParamNo).ToList();
            foreach (ObjectTypeMethodParam param in Parameters)
                parameters.Remove(param);
        }

        public List<ObjectTypeMethodParam> Parameters { get; private set; }

        public void FindResult(List<ObjectTypeMethodResult> results)
        {
            if (IsFunction)
                this.result = results.Single(result => result.TypeName == TypeName && result.MethodName == Name && result.MethodNo == MethodNo).ResultTypeName;
        }

        private string result;

        public string Result
        {
            get { return result == "PL/SQL BOOLEAN" ? "BOOLEAN" : result; }
        }
    }
}