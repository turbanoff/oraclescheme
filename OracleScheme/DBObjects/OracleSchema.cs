﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using NLog;
using OracleScheme.DBObjects.Constraint;

namespace OracleScheme.DBObjects
{
    [Serializable]
    public sealed class OracleSchema : WithDependencies
    {
        public override string ObjectType { get { return "SCHEMA"; } }

        public override int SortOrder { get { return 0; } }

        public override void Visit(IOracleObjectVisitor visitor)
        {
            visitor.Visit(this);
        }

        private static readonly Logger Log = LogManager.GetCurrentClassLogger();
        private List<Table> tables;
        private List<TableColumn> columns;
        private List<TableConstraint> constraints;
        private List<ConstraintColumn> constraintColumns;
        private List<TableIndex> indexes;
        private List<IndexColumn> indexColumns;
        private List<IndexExpression> indexExpressions;
        private List<UserDefinedType> userTypes;
        private List<ObjectTypeAttribute> objectTypeAttributes;
        private List<ObjectTypeMethod> objectTypeMethods;
        private List<ObjectTypeMethodParam> objectTypeMethodParams;
        private List<ObjectTypeMethodResult> objectTypeMethodResults;
        private List<CollectionElementType> collectionElementTypes;
        private List<UserNestedTable> nestedTables;
        private List<Lob> lobs;
        private List<View> views;
        private List<ObjectDependency> dependencies;
        private Dictionary<SourceObjectKey, string> sourceLines;
        private List<Sequence> sequences;
        private List<MaterializedView> materializedViews;
        private List<Procedure> procedures;
        private List<Function> functions;
        private List<Package> packages;
        private List<Trigger> triggers;
        private List<MaterializedViewLog> materializedViewLogs;
        private List<DatabaseLink> dblinks;
        private List<TablePartition> partitions;
        private List<PartTable> partTables;
        private List<PartKeyColumn> partKeyColumns;
        private List<TableSubpartition> subpartitions;
        private List<SubpartitionKeyColumn> subpartKeyColumns;
        private List<SubpartitionTemplate> subpartitionTemplates;
        private List<TriggerColumn> triggerColumns;
        private List<ExternalTable> externalTables;

        public OracleSchema(string user = null)
        {
            if (user != null)
                Name = user.Trim().ToUpper();
        }

        public void FillFromDatabase(DbConnection dbConnection)
        {
            if (Name == null)
                Name = GetAllObjectsEnumerable(dbConnection, r => r.GetString("USER"), "select USER from dual").First();

            tables = GetAllTables(dbConnection);
            columns = GetAllColumns(dbConnection);
            constraints = GetAllConstraints(dbConnection);
            constraintColumns = GetAllConstraintColumns(dbConnection);
            indexes = GetAllIndexes(dbConnection);
            indexColumns = GetAllIndexColumns(dbConnection);
            indexExpressions = GetAllIndexExpressions(dbConnection);
            userTypes = GetAllUserTypes(dbConnection);
            objectTypeMethods = GetAllObjectTypeMethods(dbConnection);
            objectTypeMethodParams = GetAllObjectTypeMethodParams(dbConnection);
            objectTypeMethodResults = GetAllObjectTypeMethodResults(dbConnection);
            objectTypeAttributes = GetAllObjectTypeAttributes(dbConnection);
            collectionElementTypes = GetAllCollTypes(dbConnection);
            nestedTables = GetAllNestedTables(dbConnection);
            lobs = GetAllLobs(dbConnection);
            views = GetAllViews(dbConnection);
            dependencies = GetAllDepencies(dbConnection);
            sourceLines = GetAllSourceLines(dbConnection);
            sequences = GetAllSequences(dbConnection);
            materializedViews = GetAllMaterializedViews(dbConnection);
            materializedViewLogs = GetAllMaterializedViewLogs(dbConnection);
            dblinks = GetAllDatabaseLinks(dbConnection);
            partitions = GetAllTablePartitions(dbConnection);
            partTables = GetAllPartTables(dbConnection);
            partKeyColumns = GetAllPartKeyColumns(dbConnection);
            subpartitions = GetAllSubpartitions(dbConnection);
            subpartKeyColumns = GetAllSubpartitionKeyColumns(dbConnection);
            subpartitionTemplates = GetAllSubpartitionTemplates(dbConnection);
            triggers = GetAllTriggers(dbConnection);
            triggerColumns = GetAllTriggerColumns(dbConnection);
            externalTables = GetAllExternalTables(dbConnection);

            GetAllPlsqlCodeObjects(dbConnection, out procedures, out functions, out packages);
        }

        public void Make()
        {
            Log.Info("Iterate MaterializedViewLog");
            //Удалим из списка таблиц таблицы с MLOG-ами
            foreach (MaterializedViewLog mlog in materializedViewLogs)
            {
                //Обычные логи
                Table mlogTable;
                try
                {
                    mlogTable = tables.Single(t => t.Name == mlog.LogTableName);
                }
                catch (InvalidOperationException)
                {
                    Log.Warn("MLog table (" + mlog.LogTableName + ") on " + mlog.MasterTableName + " missing in table list. Skip him");
                    continue;
                }
                tables.Remove(mlogTable);

                Table masterTable = tables.Single(t => t.Name == mlog.MasterTableName);
                mlog.MasterTable = masterTable;
                masterTable.MaterializedViewLog = mlog;

                //И временные логи RUPD$_
                if (mlog.TempLogTableName != null)
                {
                    Table tempLogTable = tables.Single(t => t.Name == mlog.TempLogTableName);
                    tables.Remove(tempLogTable);
                }
                else if (mlog.IsWithPrimaryKey)
                {
                    //Необходимо самим подобрать таблицу с временными млогами - так как доступа к SYS.MLOG$ может не быть
                    string rupdTable = "RUPD$_" + mlog.MasterTableName.Substring(0, Math.Min(20, mlog.MasterTableName.Length));
                    List<Table> list = tables.Where(t =>
                        {
                            //rupd$_ таблицы всегда - временные с сохранением строк при коммите
                            if (!t.Name.StartsWith(rupdTable) || !t.IsTemporary || !t.IsSessionDuration)
                                return false;
                            string rupdTail = t.Name.Substring(rupdTable.Length);
                            long tailNumber;
                            return rupdTail == "" || long.TryParse(rupdTail, out tailNumber);
                        }).ToList();
                    if (list.Count == 1) //Если таблиц больше чем 1 - мы точно не можем сказать, как именно для нашей таблицы
                    {
                        tables.Remove(list[0]);
                        mlog.TempLogTableName = list[0].Name;
                    }
                }
            }
            Log.Info("Iterate Package");
            //Ищем текст для пакетов заранее - так определяем есть ли тело у пакета
            List<PackageBody> packageBodies = new List<PackageBody>(packages.Count);
            foreach (Package package in packages)
            {
                PackageBody body = package.FindCode(sourceLines);
                if (body != null)
                    packageBodies.Add(body);
            }

            Log.Info("Iterate UserDefinedTypeBody");
            //Ищем текст тела пользовательских типов
            List<UserDefinedTypeBody> typeBodies = new List<UserDefinedTypeBody>(userTypes.Count);
            foreach (UserDefinedType userType in userTypes)
            {
                UserDefinedTypeBody body = userType.FindBodyCode(sourceLines);
                if (body != null)
                    typeBodies.Add(body);
            }

            Log.Info("Iterate Procedure");
            foreach (Procedure procedure in procedures)
            {
                procedure.FindCode(sourceLines);
            }

            Log.Info("Iterate Function");
            foreach (Function function in functions)
            {
                function.FindCode(sourceLines);
            }

            Log.Info("Iterate Trigger");
            foreach (Trigger trigger in triggers)
            {
                trigger.FindColumns(triggerColumns);
                trigger.FindTable(tables, views);
            }

            Log.Info("Iterate ObjectTypeMethod");
            foreach (ObjectTypeMethod method in objectTypeMethods)
            {
                method.FindParams(objectTypeMethodParams);
                method.FindResult(objectTypeMethodResults);
            }

            Log.Info("Iterate UserDefinedType");
            foreach (UserDefinedType userType in userTypes)
            {
                if (userType.TypeCode == UserTypeCode.ObjectType)
                {
                    userType.FindAttributes(objectTypeAttributes, userTypes);
                    userType.FindMethods(objectTypeMethods);
                }
                else
                {
                    userType.FindElementType(collectionElementTypes, userTypes);
                }
            }

            Log.Info("Iterate View");
            foreach (View view in views)
            {
                view.FindColumns(columns);
            }

            Log.Info("Iterate TableColumn");
            foreach (TableColumn tableColumn in columns.Where(column => column.IsUserDefinedType))
            {
                if (tableColumn.ParentTable == null || !(tableColumn.ParentTable is View)) //пропускаем все вьюхи - нет необходимости точно знать типы столбцов
                    tableColumn.FindType(userTypes, Name);
            }

            Log.Info("Iterate MaterializedView");
            foreach (MaterializedView mview in materializedViews)
            {
                Table container = tables.Single(table => table.Name == mview.ContainerName);
                mview.Container = container;
                container.ParentMaterializedView = mview;
            }

            Log.Info("Iterate Table");
            foreach (var table in tables)
            {
                table.FindColumns(columns);
                foreach (TableColumn tableColumn in table.Columns.Where(column => column.IsNestedTable))
                {
                    tableColumn.FindStoreTable(tables, nestedTables);
                }
                table.FindConstraints(constraints);
                table.FindIndexes(indexes, lobs);
                table.FindExternalDefinition(externalTables);
                foreach (var constraint in table.Constraints)
                {
                    constraint.FindColumns(constraintColumns, table.Columns);
                }
                foreach (var index in table.Indexes)
                {
                    index.FindColumns(indexColumns, columns, indexExpressions);
                }
                table.FindNestedTableIndexes();
                if (table.IsPartitioned)
                {
                    table.FindPartitions(partitions);
                    table.FindPart(partTables);
                    table.FindPartKeyColumns(partKeyColumns);
                    table.FindSubpartKeyColumns(subpartKeyColumns);
                    if (table.PartDescription.SubpartitioningType != null)
                    {
                        table.FindSubpartitionTemplates(subpartitionTemplates);
                        foreach (TablePartition partition in table.Partitions)
                        {
                            partition.FindSubpartitions(subpartitions);
                        }
                    }
                }
            }
            tables.RemoveAll(table => table.IsNestedStoreTable);

            Log.Info("Iterate Constraint");
            foreach (TableConstraint constraint in constraints)
            {
                constraint.FindIndex(indexes);
                if (constraint.ConstraintType == ConstraintType.ForeignKey)
                {
                    ((ReferentialIntegrityConstraint)constraint).FindRefConstraint(constraints);
                }
            }
            foreach (SourceObjectKey key in sourceLines.Keys.Where(s => s.ObjectType != "TYPE" && s.ObjectType != "TRIGGER")) //всю информацию о заголовках типов мы получаем
            {
                Log.Error("Unknown object with source: " + key.ObjectType + " " + key.ObjectName);
            }

            Log.Info("Ordering Table");
            //Упорядочим таблицы, чтобы reference были в нормальном порядке
            List<Table> result = new List<Table>(tables.Count);
            tables = tables.OrderBy(t => t.Name).ToList(); //preserve alphabet order on cyclic ref
            while (tables.Count != 0)
            {
                var list = tables.Select(table => new {table, nonResolved = NonResolvedConstraintCount(result, table)}).ToList();
                var resolvedList = list.Where(arg => arg.nonResolved == 0).ToList(); //Таблицы без циклических ссылок
                if (resolvedList.Count > 0) //Есть таблицы, без циклических ссылок
                {
                    foreach (var table in resolvedList)
                    {
                        Table nextTable = table.table;
                        nextTable.AddDependencies(
                            nextTable.Constraints.OfType<ReferentialIntegrityConstraint>()
                                     .Where(constraint => !constraint.IsCycleReferense)
                                     .Where(constraint => constraint.RefTableConstraint != null) //TODO: fix ref to table in another schema
                                     .Where(constraint => constraint.RefTableConstraint.ParentTable != nextTable)
                                     .Select(constraint => constraint.RefTableConstraint.ParentTable)
                            );
                        tables.Remove(nextTable);
                        result.Add(nextTable);
                    }
                }
                else
                {
                    //найдем таблицу, у которой неразрешенных constraint-ов меньше всего
                    var nextCyclicTable = list.OrderBy(arg => arg.nonResolved).First();
                    //пометим одно неразрешенное ограничение, как циклическое - оно будет добавляться после создания всех таблиц
                    var nextCyclicConstraint =
                        nextCyclicTable.table.Constraints.OfType<ReferentialIntegrityConstraint>()
                                       .First(constraint => IsNonResolvedConstraint(result, constraint));
                    nextCyclicConstraint.IsCycleReferense = true;
                }
            }
            tables = result;
            tables.RemoveAll(table => table.ParentMaterializedView != null && table.ParentMaterializedView.BuildMode != "PREBUILT");

            List<WithDependencies> listOfAll = new List<WithDependencies>();
            listOfAll.AddRange(dblinks);
            listOfAll.AddRange(sequences);
            listOfAll.AddRange(tables);
            listOfAll.AddRange(userTypes);
            listOfAll.AddRange(views);
            listOfAll.AddRange(procedures);
            listOfAll.AddRange(functions);
            listOfAll.AddRange(packages);
            listOfAll.AddRange(triggers);
            listOfAll.AddRange(packageBodies);
            listOfAll.AddRange(typeBodies);
            listOfAll.AddRange(materializedViews);
            //Generic dependecy sorting algorithm
            Log.Info("FindDependecies All");
            foreach (WithDependencies withDependency in listOfAll)
            {
                withDependency.FindDependecies(listOfAll, dependencies);
            }

            for (int i = 0; i < dependencies.Count; i++)
            {
                if (dependencies[i].ReferencedType != "NON-EXISTENT")
                {
                    Log.Warn("Non-used dependency " + dependencies[i]);
                    dependencies.RemoveAt(i);
                }
            }
            if (dependencies.Count != 0)
            {
                Log.Info("Non-used NON-EXISTENT dependencies\n" + String.Join("\n", dependencies.Select(d => d.ToStringWithoutRefType())));
            }

            Log.Info("Ordering All");
            List<WithDependencies> resultList = new List<WithDependencies>(listOfAll.Count);
            while (listOfAll.Count != 0)
            {
                List<WithDependencies> nextPart = listOfAll.Where(obj => !HasNonresolvedDep(resultList, obj)).OrderBy(o => o.SortOrder).ThenBy(o => o.Name).ToList();
                Log.Debug("listOfAll length = " + listOfAll.Count + "\t nextPart length = " + nextPart.Count);
                if (nextPart.Count == 0)
                {
                    Log.Error("Has Nonresolved objects");
                    resultList.AddRange(listOfAll);
                    break;
                }
                foreach (WithDependencies obj in nextPart)
                {
                    resultList.Add(obj);
                    listOfAll.Remove(obj);
                }
            }
            AddDependencies(resultList);
        }

        private bool IsNotDueToMissingTable(DbException e, string fallbackTable)
        {
            if (e.Message.Contains("ORA-00942"))
            {
                Log.Warn("Fallback to {0} due to {1}", fallbackTable, e.Message.Trim());
                return false;
            }
            return true;
        }

        private bool IsNotDueToMissingColumn(DbException e, string fallbackTable)
        {
            if (e.Message.Contains("ORA-00904"))
            {
                Log.Warn("Fallback to {0} due to {1}", fallbackTable, e.Message.Trim());
                return false;
            }
            return true;
        }

        private List<ExternalTable> GetAllExternalTables(DbConnection dbConnection)
        {
            Func<IDataRecord, ExternalTable> createTable =
                record => new ExternalTable(
                    record.GetString("TABLE_NAME"),
                    record.GetString("TYPE_NAME"),
                    record.GetString("DEFAULT_DIRECTORY_NAME"),
                    record.GetString("ACCESS_PARAMETERS"),
                    record.GetString("LOCATION"),
                    record.GetString("DIRECTORY_NAME")
                );
            try
            {
                const string selectText = "select T.TABLE_NAME, T.TYPE_NAME, T.DEFAULT_DIRECTORY_NAME, T.ACCESS_PARAMETERS, L.LOCATION, L.DIRECTORY_NAME" +
                                          " from SYS.DBA_EXTERNAL_TABLES T, SYS.DBA_EXTERNAL_LOCATIONS L" +
                                          " where T.TABLE_NAME = L.TABLE_NAME" +
                                          "   and T.OWNER = :OWNER" +
                                          "   and L.OWNER = T.OWNER";
                return GetAllObjects(dbConnection, createTable, selectText);
            }
            catch (DbException e)
            {
                if (IsNotDueToMissingTable(e, "ALL_EXTERNAL_TABLES")) throw;
                const string selectText = "select T.TABLE_NAME, T.TYPE_NAME, T.DEFAULT_DIRECTORY_NAME, T.ACCESS_PARAMETERS, L.LOCATION, L.DIRECTORY_NAME" +
                                          " from SYS.ALL_EXTERNAL_TABLES T, SYS.ALL_EXTERNAL_LOCATIONS L" +
                                          " where T.TABLE_NAME = L.TABLE_NAME" +
                                          "   and T.OWNER = :OWNER" +
                                          "   and L.OWNER = T.OWNER";
                return GetAllObjects(dbConnection, createTable, selectText);
            }
        }

        private List<TriggerColumn> GetAllTriggerColumns(DbConnection dbConnection)
        {
            Func<IDataRecord, TriggerColumn> createColumn =
                record => new TriggerColumn(
                    record.GetString("TRIGGER_NAME"),
                    record.GetString("COLUMN_NAME")
                );
            try
            {
                const string selectText = "select TRIGGER_NAME, COLUMN_NAME from SYS.DBA_TRIGGER_COLS where COLUMN_LIST = 'YES' and TABLE_OWNER = :OWNER";
                return GetAllObjects(dbConnection, createColumn, selectText);
            }
            catch (DbException e)
            {
                if (IsNotDueToMissingTable(e, "ALL_TRIGGER_COLS")) throw;
                const string selectText = "select TRIGGER_NAME, COLUMN_NAME from SYS.ALL_TRIGGER_COLS where COLUMN_LIST = 'YES' and TABLE_OWNER = :OWNER";
                return GetAllObjects(dbConnection, createColumn, selectText);
            }
        }

        private List<Trigger> GetAllTriggers(DbConnection dbConnection)
        {
            Func<IDataRecord, Trigger> createTrigger =
                record => new Trigger(
                    record.GetString("TRIGGER_NAME"),
                    record.GetString("TRIGGER_TYPE"),
                    record.GetString("TRIGGERING_EVENT"),
                    record.GetString("TABLE_NAME"),
                    record.GetString("BASE_OBJECT_TYPE"),
                    record.GetString("REFERENCING_NAMES"),
                    record.GetString("WHEN_CLAUSE"),
                    record.GetString("STATUS") == "ENABLED",
                    record.GetString("TRIGGER_BODY")
                );
            try
            {
                const string selectText = "select TRIGGER_NAME, TRIGGER_TYPE, TRIGGERING_EVENT, TABLE_NAME, BASE_OBJECT_TYPE, REFERENCING_NAMES, WHEN_CLAUSE, STATUS, TRIGGER_BODY from SYS.DBA_TRIGGERS where OWNER = :OWNER";
                return GetAllObjects(dbConnection, createTrigger, selectText);
            }
            catch (DbException e)
            {
                if (IsNotDueToMissingTable(e, "ALL_TRIGGERS")) throw;
                const string selectText = "select TRIGGER_NAME, TRIGGER_TYPE, TRIGGERING_EVENT, TABLE_NAME, BASE_OBJECT_TYPE, REFERENCING_NAMES, WHEN_CLAUSE, STATUS, TRIGGER_BODY from SYS.ALL_TRIGGERS where OWNER = :OWNER";
                return GetAllObjects(dbConnection, createTrigger, selectText);
            }

        }

        private List<SubpartitionTemplate> GetAllSubpartitionTemplates(DbConnection dbConnection)
        {
            Func<IDataRecord, SubpartitionTemplate> createTemplate =
                record => new SubpartitionTemplate(
                    record.GetString("TABLE_NAME"),
                    record.GetString("SUBPARTITION_NAME"),
                    record.GetInt32("SUBPARTITION_POSITION"),
                    record.GetString("HIGH_BOUND")
                );
            try
            {
                const string selectText = "select TABLE_NAME, SUBPARTITION_NAME, SUBPARTITION_POSITION, HIGH_BOUND from SYS.DBA_SUBPARTITION_TEMPLATES where USER_NAME = :OWNER";
                return GetAllObjects(dbConnection, createTemplate, selectText);
            }
            catch (DbException e)
            {
                if (IsNotDueToMissingTable(e, "ALL_SUBPARTITION_TEMPLATES")) throw;
                const string selectText = "select TABLE_NAME, SUBPARTITION_NAME, SUBPARTITION_POSITION, HIGH_BOUND from SYS.ALL_SUBPARTITION_TEMPLATES where USER_NAME = :OWNER";
                return GetAllObjects(dbConnection, createTemplate, selectText);
            }
        }

        private List<SubpartitionKeyColumn> GetAllSubpartitionKeyColumns(DbConnection dbConnection)
        {
            Func<IDataRecord, SubpartitionKeyColumn> createColumn =
                record => new SubpartitionKeyColumn(
                    record.GetString("NAME"),
                    record.GetString("OBJECT_TYPE"),
                    record.GetString("COLUMN_NAME"),
                    record.GetInt32("COLUMN_POSITION")
                );
            try
            {
                const string selectText = "select NAME, OBJECT_TYPE, COLUMN_NAME, COLUMN_POSITION from SYS.DBA_SUBPART_KEY_COLUMNS where OWNER = :OWNER";
                return GetAllObjects(dbConnection, createColumn, selectText);
            }
            catch (DbException e)
            {
                if (IsNotDueToMissingTable(e, "ALL_SUBPART_KEY_COLUMNS")) throw;
                const string selectText = "select NAME, OBJECT_TYPE, COLUMN_NAME, COLUMN_POSITION from SYS.ALL_SUBPART_KEY_COLUMNS where OWNER = :OWNER";
                return GetAllObjects(dbConnection, createColumn, selectText);
            }
        }

        private List<TableSubpartition> GetAllSubpartitions(DbConnection dbConnection)
        {
            Func<IDataRecord, TableSubpartition> createSubpartition =
                record => new TableSubpartition(
                    record.GetString("TABLE_NAME"),
                    record.GetString("PARTITION_NAME"),
                    record.GetString("SUBPARTITION_NAME"),
                    record.GetString("HIGH_VALUE"),
                    record.GetInt32("SUBPARTITION_POSITION")
                );
            try
            {
                const string selectText = "select TABLE_NAME, PARTITION_NAME, SUBPARTITION_NAME, HIGH_VALUE, SUBPARTITION_POSITION from SYS.DBA_TAB_SUBPARTITIONS where TABLE_OWNER = :OWNER";
                return GetAllObjects(dbConnection, createSubpartition, selectText);
            }
            catch (DbException e)
            {
                if (IsNotDueToMissingTable(e, "ALL_TAB_SUBPARTITIONS")) throw;
                const string selectText = "select TABLE_NAME, PARTITION_NAME, SUBPARTITION_NAME, HIGH_VALUE, SUBPARTITION_POSITION from SYS.ALL_TAB_SUBPARTITIONS where TABLE_OWNER = :OWNER";
                return GetAllObjects(dbConnection, createSubpartition, selectText);
            }
        }

        private List<PartKeyColumn> GetAllPartKeyColumns(DbConnection dbConnection)
        {
            Func<IDataRecord, PartKeyColumn> createColumn =
                record => new PartKeyColumn(
                    record.GetString("NAME"),
                    record.GetString("OBJECT_TYPE"),
                    record.GetString("COLUMN_NAME"),
                    record.GetInt32("COLUMN_POSITION")
                );
            try
            {
                const string selectText = "select NAME, OBJECT_TYPE, COLUMN_NAME, COLUMN_POSITION from SYS.DBA_PART_KEY_COLUMNS where OWNER = :OWNER";
                return GetAllObjects(dbConnection, createColumn, selectText);
            }
            catch (DbException e)
            {
                if (IsNotDueToMissingTable(e, "ALL_PART_KEY_COLUMNS")) throw;
                const string selectText = "select NAME, OBJECT_TYPE, COLUMN_NAME, COLUMN_POSITION from SYS.ALL_PART_KEY_COLUMNS where OWNER = :OWNER";
                return GetAllObjects(dbConnection, createColumn, selectText);
            }
        }

        private List<PartTable> GetAllPartTables(DbConnection dbConnection)
        {
            Func<IDataRecord, PartTable> createPartTable =
                record => new PartTable(
                    record.GetString("TABLE_NAME"),
                    record.GetString("PARTITIONING_TYPE"),
                    record.GetString("SUBPARTITIONING_TYPE"),
                    record.GetString("REF_PTN_CONSTRAINT_NAME"),
                    record.GetString("INTERVAL")
                );
            try
            {
                const string selectText = "select TABLE_NAME, PARTITIONING_TYPE, '' SUBPARTITIONING_TYPE, '' REF_PTN_CONSTRAINT_NAME, '' INTERVAL from SYS.DBA_PART_TABLES where OWNER = :OWNER";
                List<PartTable> result = GetAllObjects(dbConnection, createPartTable, selectText);
                try
                {
                    const string select = "select TABLE_NAME, PARTITIONING_TYPE, SUBPARTITIONING_TYPE, REF_PTN_CONSTRAINT_NAME, INTERVAL from SYS.DBA_PART_TABLES where OWNER = :OWNER";
                    return GetAllObjects(dbConnection, createPartTable, select);
                }
                catch (DbException e)
                {
                    if (IsNotDueToMissingColumn(e, "DBA_PART_TABLES without SUBPARTITIONING_TYPE column")) throw;
                    return result;
                }
            }
            catch (DbException e)
            {
                if (IsNotDueToMissingTable(e, "ALL_PART_TABLES")) throw;
                try
                {
                    const string selectText = "select TABLE_NAME, PARTITIONING_TYPE, SUBPARTITIONING_TYPE, REF_PTN_CONSTRAINT_NAME, INTERVAL from SYS.ALL_PART_TABLES where OWNER = :OWNER";
                    return GetAllObjects(dbConnection, createPartTable, selectText);
                }
                catch (DbException ex)
                {
                    if (IsNotDueToMissingColumn(ex, "ALL_PART_TABLES without SUBPARTITIONING_TYPE column")) throw;
                    const string selectText = "select TABLE_NAME, PARTITIONING_TYPE, '' SUBPARTITIONING_TYPE, '' REF_PTN_CONSTRAINT_NAME, '' INTERVAL from SYS.ALL_PART_TABLES where OWNER = :OWNER";
                    return GetAllObjects(dbConnection, createPartTable, selectText);
                }
            }
        }

        private List<TablePartition> GetAllTablePartitions(DbConnection dbConnection)
        {
            Func<IDataRecord, TablePartition> createPartition =
                record => new TablePartition(
                    record.GetString("TABLE_NAME"),
                    record.GetString("PARTITION_NAME"),
                    record.GetInt32("SUBPARTITION_COUNT"),
                    record.GetString("HIGH_VALUE"),
                    record.GetInt32("PARTITION_POSITION")
                );
            try
            {
                const string selectText = "select TABLE_NAME, PARTITION_NAME, SUBPARTITION_COUNT, HIGH_VALUE, PARTITION_POSITION from SYS.DBA_TAB_PARTITIONS where TABLE_OWNER = :OWNER";
                return GetAllObjects(dbConnection, createPartition, selectText);
            }
            catch (DbException e)
            {
                if (IsNotDueToMissingTable(e, "ALL_TAB_PARTITIONS")) throw;
                const string selectText = "select TABLE_NAME, PARTITION_NAME, SUBPARTITION_COUNT, HIGH_VALUE, PARTITION_POSITION from SYS.ALL_TAB_PARTITIONS where TABLE_OWNER = :OWNER";
                return GetAllObjects(dbConnection, createPartition, selectText);
            }
        }

        private List<ObjectTypeMethodResult> GetAllObjectTypeMethodResults(DbConnection dbConnection)
        {
            Func<IDataRecord, ObjectTypeMethodResult> createResult =
                record => new ObjectTypeMethodResult(
                    record.GetString("TYPE_NAME"),
                    record.GetString("METHOD_NAME"),
                    record.GetInt32("METHOD_NO"),
                    record.GetString("RESULT_TYPE_NAME")
                );
            try
            {
                const string selectText = "select TYPE_NAME, METHOD_NAME, METHOD_NO, RESULT_TYPE_NAME from SYS.DBA_METHOD_RESULTS where OWNER = :OWNER";
                return GetAllObjects(dbConnection, createResult, selectText);
            }
            catch (DbException e)
            {
                if (IsNotDueToMissingTable(e, "ALL_METHOD_RESULTS")) throw;
                const string selectText = "select TYPE_NAME, METHOD_NAME, METHOD_NO, RESULT_TYPE_NAME from SYS.ALL_METHOD_RESULTS where OWNER = :OWNER";
                return GetAllObjects(dbConnection, createResult, selectText);
            }
        }

        private List<ObjectTypeMethodParam> GetAllObjectTypeMethodParams(DbConnection dbConnection)
        {
            Func<IDataRecord, ObjectTypeMethodParam> createParam =
                record => new ObjectTypeMethodParam(
                    record.GetString("TYPE_NAME"),
                    record.GetString("METHOD_NAME"),
                    record.GetInt32("METHOD_NO"),
                    record.GetString("PARAM_NAME"),
                    record.GetInt32("PARAM_NO"),
                    record.GetString("PARAM_MODE"),
                    record.GetString("PARAM_TYPE_NAME")
                );
            try
            {
                const string selectText = "select TYPE_NAME, METHOD_NAME, METHOD_NO, PARAM_NAME, PARAM_NO, PARAM_MODE, PARAM_TYPE_NAME from SYS.DBA_METHOD_PARAMS where OWNER = :OWNER";
                return GetAllObjects(dbConnection, createParam, selectText);
            }
            catch (DbException e)
            {
                if (IsNotDueToMissingTable(e, "ALL_METHOD_PARAMS")) throw;
                const string selectText = "select TYPE_NAME, METHOD_NAME, METHOD_NO, PARAM_NAME, PARAM_NO, PARAM_MODE, PARAM_TYPE_NAME from SYS.ALL_METHOD_PARAMS where OWNER = :OWNER";
                return GetAllObjects(dbConnection, createParam, selectText);
            }
        }

        private List<ObjectTypeMethod> GetAllObjectTypeMethods(DbConnection dbConnection)
        {
            Func<IDataRecord, ObjectTypeMethod> createMethod =
                record => new ObjectTypeMethod(
                    record.GetString("TYPE_NAME"),
                    record.GetString("METHOD_NAME"),
                    record.GetInt32("METHOD_NO"),
                    record.GetString("METHOD_TYPE"),
                    record.GetInt32("RESULTS") == 1,
                    record.GetInt32("PARAMETERS")
                );
            try
            {
                const string selectText = "select TYPE_NAME, METHOD_NAME, METHOD_NO, METHOD_TYPE, RESULTS, PARAMETERS from SYS.DBA_TYPE_METHODS where OWNER = :OWNER";
                return GetAllObjects(dbConnection, createMethod, selectText);
            }
            catch (DbException e)
            {
                if (IsNotDueToMissingTable(e, "ALL_TYPE_METHODS")) throw;
                const string selectText = "select TYPE_NAME, METHOD_NAME, METHOD_NO, METHOD_TYPE, RESULTS, PARAMETERS from SYS.ALL_TYPE_METHODS where OWNER = :OWNER";
                return GetAllObjects(dbConnection, createMethod, selectText);
            }
        }

        private List<MaterializedViewLog> GetAllMaterializedViewLogs(DbConnection dbConnection)
        {
            Func<IDataRecord, MaterializedViewLog> createLog =
                record => new MaterializedViewLog(
                    record.GetString("MASTER"),
                    record.GetString("LOG_TABLE"),
                    record.GetString("TEMP_LOG"),
                    record.GetString("ROWIDS") == "YES",
                    record.GetString("PRIMARY_KEY") == "YES",
                    record.GetString("SEQUENCE") == "YES",
                    record.GetString("INCLUDE_NEW_VALUES") == "YES"
                 );
            try
            {
                const string selectText = "select MASTER, LOG LOG_TABLE, TEMP_LOG" +
                                          "     , decode(bitand(FLAG, 1), 0, 'NO', 'YES') ROWIDS" +
                                          "     , decode(bitand(FLAG, 2), 0, 'NO', 'YES') PRIMARY_KEY" +
                                          "     , decode(bitand(FLAG, 1024), 0, 'NO', 'YES') SEQUENCE" +
                                          "     , decode(bitand(FLAG, 16), 0, 'NO', 'YES') INCLUDE_NEW_VALUES" +
                                          " from SYS.MLOG$" +
                                          " where MOWNER = :OWNER";
                return GetAllObjects(dbConnection, createLog, selectText);
            }
            catch (DbException e)
            {
                if (IsNotDueToMissingTable(e, "DBA_MVIEW_LOGS")) throw;
                try
                {
                    const string selectText = "select MASTER, LOG_TABLE, '' TEMP_LOG, ROWIDS, PRIMARY_KEY, SEQUENCE, INCLUDE_NEW_VALUES from SYS.DBA_MVIEW_LOGS where LOG_OWNER = :OWNER";
                    return GetAllObjects(dbConnection, createLog, selectText);
                }
                catch (DbException ex)
                {
                    if (IsNotDueToMissingTable(ex, "ALL_MVIEW_LOGS")) throw;
                    const string selectText = "select MASTER, LOG_TABLE, '' TEMP_LOG, ROWIDS, PRIMARY_KEY, SEQUENCE, INCLUDE_NEW_VALUES from SYS.ALL_MVIEW_LOGS where LOG_OWNER = :OWNER";
                    return GetAllObjects(dbConnection, createLog, selectText);
                }
            }
        }

        private List<MaterializedView> GetAllMaterializedViews(DbConnection dbConnection)
        {
            Func<IDataRecord, MaterializedView> createMatView =
                record => new MaterializedView(
                    record.GetString("MVIEW_NAME"),
                    record.GetString("CONTAINER_NAME"),
                    record.GetString("QUERY"),
                    record.GetChar("REWRITE_ENABLED") == 'Y',
                    record.GetString("REFRESH_MODE"),
                    record.GetString("REFRESH_METHOD"),
                    record.GetString("BUILD_MODE")
                );
            try
            {
                const string selectText = "select MVIEW_NAME, CONTAINER_NAME, QUERY, REWRITE_ENABLED, REFRESH_MODE, REFRESH_METHOD, BUILD_MODE from SYS.DBA_MVIEWS where OWNER = :OWNER";
                return GetAllObjects(dbConnection, createMatView, selectText);
            }
            catch (DbException e)
            {
                if (IsNotDueToMissingTable(e, "ALL_MVIEWS")) throw;
                const string selectText = "select MVIEW_NAME, CONTAINER_NAME, QUERY, REWRITE_ENABLED, REFRESH_MODE, REFRESH_METHOD, BUILD_MODE from SYS.ALL_MVIEWS where OWNER = :OWNER";
                return GetAllObjects(dbConnection, createMatView, selectText);
            }
        }

        private List<Sequence> GetAllSequences(DbConnection dbConnection)
        {
            Func<IDataRecord, Sequence> createSequence =
                record => new Sequence(
                    record.GetString("SEQUENCE_NAME"),
                    record.GetDecimal("MIN_VALUE"),
                    record.GetDecimal("MAX_VALUE"),
                    record.GetDecimal("INCREMENT_BY"),
                    record.GetChar("CYCLE_FLAG") == 'Y',
                    record.GetChar("ORDER_FLAG") == 'Y',
                    record.GetDecimal("CACHE_SIZE"),
                    record.GetDecimal("LAST_NUMBER")
                );
            try
            {
                const string selectText = "select SEQUENCE_NAME, MIN_VALUE, MAX_VALUE, INCREMENT_BY, CYCLE_FLAG, ORDER_FLAG, CACHE_SIZE, LAST_NUMBER from SYS.DBA_SEQUENCES where SEQUENCE_OWNER = :OWNER";
                return GetAllObjects(dbConnection, createSequence, selectText);
            }
            catch (DbException e)
            {
                if (IsNotDueToMissingTable(e, "ALL_SEQUENCES")) throw;
                const string selectText = "select SEQUENCE_NAME, MIN_VALUE, MAX_VALUE, INCREMENT_BY, CYCLE_FLAG, ORDER_FLAG, CACHE_SIZE, LAST_NUMBER from SYS.ALL_SEQUENCES where SEQUENCE_OWNER = :OWNER";
                return GetAllObjects(dbConnection, createSequence, selectText);
            }
        }

        private Dictionary<SourceObjectKey, string> GetAllSourceLines(DbConnection dbConnection)
        {
            try
            {
                const string selectText = "select NAME, TYPE, TEXT from SYS.DBA_SOURCE where OWNER = :OWNER order by NAME, TYPE, LINE";
                return ReadAllSourceLines(dbConnection, selectText);
            }
            catch (DbException e)
            {
                if (IsNotDueToMissingTable(e, "ALL_SOURCE")) throw;
                const string selectText = "select NAME, TYPE, TEXT from SYS.ALL_SOURCE where OWNER = :OWNER order by NAME, TYPE, LINE";
                return ReadAllSourceLines(dbConnection, selectText);
            }
        }

        private Dictionary<SourceObjectKey, string> ReadAllSourceLines(DbConnection dbConnection, string selectText)
        {
            //Весь исходный текст, сразу группируем по имени и типу объекта, и объединяем в одну строку
            return GetAllObjectsEnumerable(dbConnection,
                record => new
                    {
                        Key = new SourceObjectKey(record.GetString("TYPE"), record.GetString("NAME")),
                        Text = record.GetString("TEXT")
                    },
                selectText)
                .GroupBy(line => line.Key)
                .ToDictionary(g => g.Key, g => string.Join("", g.Select(line => line.Text)));
        }

        private void GetAllPlsqlCodeObjects(DbConnection dbConnection, out List<Procedure> procedures, out List<Function> functions, out List<Package> packages)
        {
            procedures = new List<Procedure>();
            functions = new List<Function>();
            packages = new List<Package>();
            try
            {
                const string selectText = "select OBJECT_NAME, OBJECT_TYPE from SYS.DBA_OBJECTS where OBJECT_TYPE in ('FUNCTION', 'PROCEDURE', 'PACKAGE') and OWNER = :OWNER";
                ReadAllPlsqlObjects(dbConnection, procedures, functions, packages, selectText);
            }
            catch (DbException e)
            {
                if (IsNotDueToMissingTable(e, "ALL_OBJECTS")) throw;
                const string selectText = "select OBJECT_NAME, OBJECT_TYPE from SYS.ALL_OBJECTS where OBJECT_TYPE in ('FUNCTION', 'PROCEDURE', 'PACKAGE') and OWNER = :OWNER";
                ReadAllPlsqlObjects(dbConnection, procedures, functions, packages, selectText);
            }
        }

        private void ReadAllPlsqlObjects(DbConnection dbConnection, List<Procedure> procedures, List<Function> functions, List<Package> packages, string selectText)
        {
            using (var dbCommand = dbConnection.CreateCommand())
            {
                dbCommand.CommandText = selectText;
                DbParameter parameter = dbCommand.CreateParameter();
                parameter.Value = Name;
                parameter.ParameterName = "OWNER";
                dbCommand.Parameters.Add(parameter);
                using (var reader = dbCommand.ExecuteReader())
                {
                    int i = 1;
                    int n = 100;
                    Log.Info("Read all (PlsqlCodeObjects) from database");
                    while (reader.Read())
                    {
                        string objectName = reader.GetString("OBJECT_NAME");
                        string objectType = reader.GetString("OBJECT_TYPE");

                        switch (objectType)
                        {
                            case "FUNCTION":
                                functions.Add(new Function(objectName));
                                break;
                            case "PROCEDURE":
                                procedures.Add(new Procedure(objectName));
                                break;
                            case "PACKAGE":
                                packages.Add(new Package(objectName));
                                break;
                            default:
                                Log.Error("Unknown object type " + objectType);
                                break;
                        }
                        if (i % n == 0)
                        {
                            Log.Info("read {0} records", i);
                            if (i == n * 10)
                                n *= 10;
                        }
                        i++;
                    }
                    Log.Info("End read (PlsqlCodeObjects)");
                }
            }
        }

        private static bool HasNonresolvedDep(List<WithDependencies> all, WithDependencies obj)
        {
            return obj.Dependencies.Any(dependency => !all.Contains(dependency));
        }

        private List<ObjectDependency> GetAllDepencies(DbConnection dbConnection)
        {
            Func<IDataRecord, ObjectDependency> createDependency =
                record => new ObjectDependency(
                    record.GetString("NAME"),
                    record.GetString("TYPE"),
                    record.GetString("REFERENCED_NAME"),
                    record.GetString("REFERENCED_TYPE")
                );
            try
            {
                const string selectText = "select NAME, TYPE, REFERENCED_NAME, REFERENCED_TYPE from SYS.DBA_DEPENDENCIES where :OWNER in (OWNER, REFERENCED_OWNER)";
                return GetAllObjects(dbConnection, createDependency, selectText);
            }
            catch (DbException e)
            {
                if (IsNotDueToMissingTable(e, "ALL_DEPENDENCIES")) throw;
                const string selectText = "select NAME, TYPE, REFERENCED_NAME, REFERENCED_TYPE from SYS.ALL_DEPENDENCIES where :OWNER in (OWNER, REFERENCED_OWNER)";
                return GetAllObjects(dbConnection, createDependency, selectText);
            }
        }

        private List<View> GetAllViews(DbConnection dbConnection)
        {
            Func<IDataRecord, View> createView =
                record => new View(
                    record.GetString("VIEW_NAME"),
                    record.GetString("TEXT")
                );
            try
            {
                const string selectText = "select VIEW_NAME, TEXT from SYS.DBA_VIEWS where OWNER = :OWNER";
                return GetAllObjects(dbConnection, createView, selectText);
            }
            catch (DbException e)
            {
                if (IsNotDueToMissingTable(e, "ALL_VIEWS")) throw;
                const string selectText = "select VIEW_NAME, TEXT from SYS.ALL_VIEWS where OWNER = :OWNER";
                return GetAllObjects(dbConnection, createView, selectText);
            }
        }

        private List<Lob> GetAllLobs(DbConnection dbConnection)
        {
            Func<IDataRecord, Lob> createLob =
                record => new Lob(
                    record.GetString("TABLE_NAME"),
                    record.GetString("INDEX_NAME")
                );
            try
            {
                const string selectText = "select TABLE_NAME, COLUMN_NAME, INDEX_NAME from SYS.DBA_LOBS where OWNER = :OWNER";
                return GetAllObjects(dbConnection, createLob, selectText);
            }
            catch (DbException e)
            {
                if (IsNotDueToMissingTable(e, "ALL_LOBS")) throw;
                const string selectText = "select TABLE_NAME, COLUMN_NAME, INDEX_NAME from SYS.ALL_LOBS where OWNER = :OWNER";
                return GetAllObjects(dbConnection, createLob, selectText);
            }
        }

        private List<UserNestedTable> GetAllNestedTables(DbConnection dbConnection)
        {
            Func<IDataRecord, UserNestedTable> createNestedTable =
                record => new UserNestedTable(
                    record.GetString("TABLE_NAME"),
                    record.GetString("PARENT_TABLE_NAME"),
                    record.GetString("PARENT_TABLE_COLUMN")
                );
            try
            {
                const string selectText = "select TABLE_NAME, PARENT_TABLE_NAME, PARENT_TABLE_COLUMN from SYS.DBA_NESTED_TABLES where :OWNER in (OWNER, TABLE_TYPE_OWNER)";
                return GetAllObjects(dbConnection, createNestedTable, selectText);
            }
            catch (DbException e)
            {
                if (IsNotDueToMissingTable(e, "ALL_NESTED_TABLES")) throw;
                const string selectText = "select TABLE_NAME, PARENT_TABLE_NAME, PARENT_TABLE_COLUMN from SYS.ALL_NESTED_TABLES where :OWNER in (OWNER, TABLE_TYPE_OWNER)";
                return GetAllObjects(dbConnection, createNestedTable, selectText);
            }
        }

        private List<CollectionElementType> GetAllCollTypes(DbConnection dbConnection)
        {
            Func<IDataRecord, CollectionElementType> createElementType =
            record => new CollectionElementType(
                record.GetString("TYPE_NAME"),
                record.GetString("COLL_TYPE"),
                record.GetNullableInt("UPPER_BOUND"),
                record.GetString("ELEM_TYPE_NAME"),
                record.GetNullableInt("LENGTH"),
                record.GetNullableInt("PRECISION"),
                record.GetNullableInt("SCALE"),
                record.GetChar("CHAR_USED")
            );
            try
            {
                const string selectText = "select TYPE_NAME, COLL_TYPE, UPPER_BOUND, ELEM_TYPE_NAME, LENGTH, PRECISION, SCALE, 'C' CHAR_USED from SYS.DBA_COLL_TYPES where :OWNER in (OWNER, ELEM_TYPE_OWNER)";
                List<CollectionElementType> collTypes = GetAllObjects(dbConnection, createElementType, selectText);
                try
                {
                    const string select = "select TYPE_NAME, COLL_TYPE, UPPER_BOUND, ELEM_TYPE_NAME, LENGTH, PRECISION, SCALE, CHAR_USED from SYS.DBA_COLL_TYPES where :OWNER in (OWNER, ELEM_TYPE_OWNER)";
                    return GetAllObjects(dbConnection, createElementType, select);
                }
                catch (DbException e)
                {
                    if (IsNotDueToMissingColumn(e, "DBA_COLL_TYPES without CHAR_USED column")) throw;
                    return collTypes;
                }
            }
            catch (DbException e)
            {
                if (IsNotDueToMissingTable(e, "ALL_COLL_TYPES")) throw;
                try
                {
                    const string selectText = "select TYPE_NAME, COLL_TYPE, UPPER_BOUND, ELEM_TYPE_NAME, LENGTH, PRECISION, SCALE, CHAR_USED from SYS.ALL_COLL_TYPES where :OWNER in (OWNER, ELEM_TYPE_OWNER)";
                    return GetAllObjects(dbConnection, createElementType, selectText);
                }
                catch (DbException ex)
                {
                    if (IsNotDueToMissingColumn(ex, "ALL_COLL_TYPES without CHAR_USED column")) throw;
                    const string selectText = "select TYPE_NAME, COLL_TYPE, UPPER_BOUND, ELEM_TYPE_NAME, LENGTH, PRECISION, SCALE, 'C' CHAR_USED from SYS.ALL_COLL_TYPES where :OWNER in (OWNER, ELEM_TYPE_OWNER)";
                    return GetAllObjects(dbConnection, createElementType, selectText);
                }
            }
        }

        private List<UserDefinedType> GetAllUserTypes(DbConnection dbConnection)
        {
            Func<IDataRecord, UserDefinedType> createType =
                record => new UserDefinedType(
                    record.GetString("TYPE_NAME"),
                    record.GetString("TYPECODE"),
                    record.GetInt32("ATTRIBUTES"),
                    record.GetInt32("METHODS")
                );
            try
            {
                const string selectText = "select TYPE_NAME, TYPECODE, ATTRIBUTES, METHODS from SYS.DBA_TYPES where OWNER = :OWNER";
                return GetAllObjects(dbConnection, createType, selectText);
            }
            catch (DbException e)
            {
                if (IsNotDueToMissingTable(e, "ALL_TYPES")) throw;
                const string selectText = "select TYPE_NAME, TYPECODE, ATTRIBUTES, METHODS from SYS.ALL_TYPES where OWNER = :OWNER";
                return GetAllObjects(dbConnection, createType, selectText);
            }
        }

        private List<ObjectTypeAttribute> GetAllObjectTypeAttributes(DbConnection dbConnection)
        {
            Func<IDataRecord, ObjectTypeAttribute> createAttribute =
                record => new ObjectTypeAttribute(
                    record.GetString("TYPE_NAME"),
                    record.GetString("ATTR_NAME"),
                    record.GetString("ATTR_TYPE_NAME"),
                    record.GetNullableInt("LENGTH"),
                    record.GetNullableInt("PRECISION"),
                    record.GetNullableInt("SCALE"),
                    record.GetInt32("ATTR_NO"),
                    record.GetChar("CHAR_USED")
                );
            try
            {
                const string selectText = "select TYPE_NAME, ATTR_NAME, ATTR_TYPE_NAME, LENGTH, PRECISION, SCALE, ATTR_NO, 'C' CHAR_USED from SYS.DBA_TYPE_ATTRS where :OWNER in (OWNER, ATTR_TYPE_OWNER)";
                List<ObjectTypeAttribute> typeAttributes = GetAllObjects(dbConnection, createAttribute, selectText);
                try
                {
                    const string select = "select TYPE_NAME, ATTR_NAME, ATTR_TYPE_NAME, LENGTH, PRECISION, SCALE, ATTR_NO, CHAR_USED from SYS.DBA_TYPE_ATTRS where :OWNER in (OWNER, ATTR_TYPE_OWNER)";
                    return GetAllObjects(dbConnection, createAttribute, select);
                }
                catch (DbException e)
                {
                    if (IsNotDueToMissingColumn(e, "DBA_TYPE_ATTRS without CHAR_USED column")) throw;
                    return typeAttributes;
                }
            }
            catch (DbException e)
            {
                if (IsNotDueToMissingTable(e, "ALL_TYPE_ATTRS")) throw;
                try
                {
                    const string selectText = "select TYPE_NAME, ATTR_NAME, ATTR_TYPE_NAME, LENGTH, PRECISION, SCALE, ATTR_NO, CHAR_USED from SYS.ALL_TYPE_ATTRS where :OWNER in (OWNER, ATTR_TYPE_OWNER)";
                    return GetAllObjects(dbConnection, createAttribute, selectText);
                }
                catch (DbException ex)
                {
                    if (IsNotDueToMissingColumn(ex, "ALL_TYPE_ATTRS without CHAR_USED column")) throw;
                    const string selectText = "select TYPE_NAME, ATTR_NAME, ATTR_TYPE_NAME, LENGTH, PRECISION, SCALE, ATTR_NO, 'C' CHAR_USED from SYS.ALL_TYPE_ATTRS where :OWNER in (OWNER, ATTR_TYPE_OWNER)";
                    return GetAllObjects(dbConnection, createAttribute, selectText);
                }
            }
        }

        private List<IndexExpression> GetAllIndexExpressions(DbConnection dbConnection)
        {
            Func<IDataRecord, IndexExpression> createIndexExpression =
                record => new IndexExpression(
                    record.GetString("INDEX_NAME"),
                    record.GetString("TABLE_NAME"),
                    record.GetString("COLUMN_EXPRESSION"),
                    record.GetInt32("COLUMN_POSITION")
                );
            try
            {
                const string selectText = "select INDEX_NAME, TABLE_NAME, COLUMN_EXPRESSION, COLUMN_POSITION from SYS.DBA_IND_EXPRESSIONS where :OWNER in (INDEX_OWNER, TABLE_OWNER)";
                return GetAllObjects(dbConnection, createIndexExpression, selectText);
            }
            catch (DbException e)
            {
                if (IsNotDueToMissingTable(e, "ALL_IND_EXPRESSIONS")) throw;
                const string selectText = "select INDEX_NAME, TABLE_NAME, COLUMN_EXPRESSION, COLUMN_POSITION from SYS.ALL_IND_EXPRESSIONS where :OWNER in (INDEX_OWNER, TABLE_OWNER)";
                return GetAllObjects(dbConnection, createIndexExpression, selectText);
            }
        }

        private List<IndexColumn> GetAllIndexColumns(DbConnection dbConnection)
        {
            Func<IDataRecord, IndexColumn> createIndexColumn =
                record => new IndexColumn(
                    record.GetString("INDEX_NAME"),
                    record.GetString("COLUMN_NAME"),
                    record.GetInt32("COLUMN_POSITION")
                );
            try
            {
                const string selectText = "select INDEX_NAME, COLUMN_NAME, COLUMN_POSITION from SYS.DBA_IND_COLUMNS where :OWNER in (INDEX_OWNER, TABLE_OWNER)";
                return GetAllObjects(dbConnection, createIndexColumn, selectText);
            }
            catch (DbException e)
            {
                if (IsNotDueToMissingTable(e, "ALL_IND_COLUMNS")) throw;
                const string selectText = "select INDEX_NAME, COLUMN_NAME, COLUMN_POSITION from SYS.ALL_IND_COLUMNS where :OWNER in (INDEX_OWNER, TABLE_OWNER)";
                return GetAllObjects(dbConnection, createIndexColumn, selectText);
            }
        }

        private List<TableIndex> GetAllIndexes(DbConnection dbConnection)
        {
            Func<IDataRecord, TableIndex> createIndex =
                record => new TableIndex(
                    record.GetString("INDEX_NAME"),
                    record.GetString("TABLE_NAME"),
                    record.GetString("INDEX_TYPE"),
                    record.GetString("UNIQUENESS") == "UNIQUE"
                );
            try
            {
                const string selectText = "select INDEX_NAME, TABLE_NAME, INDEX_TYPE, UNIQUENESS from SYS.DBA_INDEXES where :OWNER in (OWNER, TABLE_OWNER)";
                return GetAllObjects(dbConnection, createIndex, selectText);
            }
            catch (DbException e)
            {
                if (IsNotDueToMissingTable(e, "ALL_INDEXES")) throw;
                const string selectText = "select INDEX_NAME, TABLE_NAME, INDEX_TYPE, UNIQUENESS from SYS.ALL_INDEXES where :OWNER in (OWNER, TABLE_OWNER)";
                return GetAllObjects(dbConnection, createIndex, selectText);
            }
        }

        private static int NonResolvedConstraintCount(List<Table> result, Table table)
        {
            return table.Constraints.OfType<ReferentialIntegrityConstraint>().Count(c => IsNonResolvedConstraint(result, c));
        }

        private static bool IsNonResolvedConstraint(List<Table> result, ReferentialIntegrityConstraint c)
        {
            if (c == null || c.RefTableConstraint == null) //TODO: fix constraint to table in another schema
                return false;
            Table refTable = c.RefTableConstraint.ParentTable;
            return (refTable != c.ParentTable && !result.Contains(refTable) && !c.IsCycleReferense);
        }

        private List<ConstraintColumn> GetAllConstraintColumns(DbConnection dbConnection)
        {
            Func<IDataRecord, ConstraintColumn> createConstraintColumn =
                record => new ConstraintColumn(
                    record.GetString("COLUMN_NAME"),
                    record.GetString("TABLE_NAME"),
                    record.GetString("CONSTRAINT_NAME"),
                    record.GetNullableInt("POSITION")
                );
            try
            {
                const string selectText = "select CONSTRAINT_NAME, TABLE_NAME, COLUMN_NAME, POSITION from SYS.DBA_CONS_COLUMNS where OWNER = :OWNER";
                return GetAllObjects(dbConnection, createConstraintColumn, selectText);
            }
            catch (DbException e)
            {
                if (IsNotDueToMissingTable(e, "ALL_CONS_COLUMNS")) throw;
                const string selectText = "select CONSTRAINT_NAME, TABLE_NAME, COLUMN_NAME, POSITION POSITION from SYS.ALL_CONS_COLUMNS where OWNER = :OWNER";
                return GetAllObjects(dbConnection, createConstraintColumn, selectText);
            }
        }

        /// <summary>Список всех constraint'ов</summary>
        private List<TableConstraint> GetAllConstraints(DbConnection dbConnection)
        {
            try
            {
                const string selectText = "select TABLE_NAME, CONSTRAINT_NAME, CONSTRAINT_TYPE, SEARCH_CONDITION, R_CONSTRAINT_NAME, INDEX_NAME, DELETE_RULE, STATUS from SYS.DBA_CONSTRAINTS where OWNER = :OWNER";
                return GetAllObjects(dbConnection, TableConstraint.Create, selectText);
            }
            catch (DbException e)
            {
                if (IsNotDueToMissingTable(e, "ALL_CONSTRAINTS")) throw;
                const string selectText = "select TABLE_NAME, CONSTRAINT_NAME, CONSTRAINT_TYPE, SEARCH_CONDITION, R_CONSTRAINT_NAME, INDEX_NAME, DELETE_RULE, STATUS from SYS.ALL_CONSTRAINTS where OWNER = :OWNER";
                return GetAllObjects(dbConnection, TableConstraint.Create, selectText);
            }
        }

        /// <summary>Список всех таблиц, созданных пользователем</summary>
        private List<Table> GetAllTables(DbConnection dbConnection)
        {
            Func<IDataRecord, Table> createTable =
                record => new Table(
                    record.GetString("TABLE_NAME"),
                    record.GetString("DURATION"),
                    record.GetString("IOT_TYPE"),
                    record.GetString("PARTITIONED") == "YES"
                );
            try
            {
                const string selectText = "select TABLE_NAME, DURATION, IOT_TYPE, PARTITIONED from SYS.DBA_TABLES where OWNER = :OWNER";
                return GetAllObjects(dbConnection, createTable, selectText);
            }
            catch (DbException e)
            {
                if (IsNotDueToMissingTable(e, "ALL_TABLES")) throw;
                const string selectText = "select TABLE_NAME, DURATION, IOT_TYPE, PARTITIONED from SYS.ALL_TABLES where OWNER = :OWNER";
                return GetAllObjects(dbConnection, createTable, selectText);
            }
        }

        /// <summary>Читает сразу всю информацию из базы обо всех столбцах - для скорости</summary>
        private List<TableColumn> GetAllColumns(DbConnection dbConnection)
        {
            Func<IDataRecord, TableColumn> createColumn =
                record => new TableColumn(
                    record.GetString("COLUMN_NAME"),
                    record.GetString("DATA_TYPE"),
                    record.GetString("DATA_TYPE_OWNER"),
                    record.GetString("TABLE_NAME"),
                    record.GetInt32("COLUMN_ID"),
                    record.GetInt32("DATA_LENGTH"),
                    record.GetNullableInt("DATA_PRECISION"),
                    record.GetNullableInt("DATA_SCALE"),
                    record.GetChar("CHAR_USED"),
                    record.GetString("DATA_DEFAULT"),
                    record.GetString("VIRTUAL_COLUMN") == "YES"
                );
            try
            {
                const string selectText = "select COLUMN_NAME, DATA_TYPE, DATA_TYPE_OWNER, TABLE_NAME, DATA_LENGTH, DATA_PRECISION, DATA_SCALE, CHAR_USED, COLUMN_ID, DATA_DEFAULT, VIRTUAL_COLUMN from SYS.DBA_TAB_COLS where HIDDEN_COLUMN = 'NO' and :OWNER in (OWNER, DATA_TYPE_OWNER)";
                return GetAllObjects(dbConnection, createColumn, selectText);
            }
            catch (DbException e)
            {
                if (IsNotDueToMissingTable(e, "ALL_TAB_COLS")) throw;
                const string selectText = "select COLUMN_NAME, DATA_TYPE, DATA_TYPE_OWNER, TABLE_NAME, DATA_LENGTH, DATA_PRECISION, DATA_SCALE, CHAR_USED, COLUMN_ID, DATA_DEFAULT, VIRTUAL_COLUMN from SYS.ALL_TAB_COLS where HIDDEN_COLUMN = 'NO' and :OWNER in (OWNER, DATA_TYPE_OWNER)";
                return GetAllObjects(dbConnection, createColumn, selectText);
            }
        }

        private List<DatabaseLink> GetAllDatabaseLinks(DbConnection dbConnection)
        {
            Func<IDataRecord, DatabaseLink> createDblink =
                record => new DatabaseLink(
                    record.GetString("DB_LINK"),
                    record.GetString("USERNAME"),
                    record.GetString("HOST")
                );
            try
            {
                const string selectText = "select DB_LINK, USERNAME, HOST from SYS.DBA_DB_LINKS where OWNER = :OWNER";
                return GetAllObjects(dbConnection, createDblink, selectText);
            }
            catch (DbException e)
            {
                if (IsNotDueToMissingTable(e, "ALL_DB_LINKS")) throw;
                const string selectText = "select DB_LINK, USERNAME, HOST from SYS.ALL_DB_LINKS where OWNER = :OWNER";
                return GetAllObjects(dbConnection, createDblink, selectText);
            }
        }

        private List<T> GetAllObjects<T>(DbConnection dbConnection, Func<IDataRecord, T> generate, string selectText)
        {
            return GetAllObjectsEnumerable(dbConnection, generate, selectText).ToList();
        }

        private IEnumerable<T> GetAllObjectsEnumerable<T>(DbConnection dbConnection, Func<IDataRecord, T> generate, string selectText)
        {
            using (var dbCommand = dbConnection.CreateCommand())
            {
                dbCommand.CommandText = selectText;
                if (selectText.Contains(":"))
                {
                    DbParameter parameter = dbCommand.CreateParameter();
                    parameter.Value = Name;
                    parameter.ParameterName = "OWNER";
                    dbCommand.Parameters.Add(parameter);
                }
                using (var reader = dbCommand.ExecuteReader())
                {
                    int i = 1;
                    int n = 100;
                    Log.Info("Read all ({0}) from database", typeof(T));
                    while (reader.Read())
                    {
                        T next = generate(reader);
                        yield return next;

                        if (i%n == 0)
                        {
                            Log.Info("read {0} records", i);
                            if (i == n*10)
                                n *= 10;
                        }
                        i++;
                    }
                    Log.Info("End read ({0})", typeof(T));
                }
            }
        }
    }
}