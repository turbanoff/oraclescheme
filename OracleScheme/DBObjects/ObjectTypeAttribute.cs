﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace OracleScheme.DBObjects
{
    /// <summary>Поле объектного пользовательского типа</summary>
    [Serializable]
    public class ObjectTypeAttribute
    {
        public readonly string TypeName;
        public readonly string Name;
        public readonly int Position;
        public readonly ColumnType AttributeType;
        private string attributeTypeName;
        public UserDefinedType UserDefinedAttributeType;

        public ObjectTypeAttribute(string typeName, string name, string attributeTypeName, int? length, int? precision, int? scale, int attributeNo, char charUsed)
        {
            this.TypeName = typeName;
            this.Name = name;
            this.Position = attributeNo;
            OracleColumnType oracleGenericType = OracleTypeUtils.OracleGenericType(attributeTypeName);
            if (oracleGenericType == OracleColumnType.Undefined) //Элемент коллекции - пользовательский тип
            {
                this.attributeTypeName = attributeTypeName;
            }
            else
            {
                this.AttributeType = OracleTypeUtils.GetColumnType(oracleGenericType, length, precision, scale, charUsed);
            }
        }

        public void FindType(List<UserDefinedType> userTypes)
        {
            if (attributeTypeName != null)
            {
                UserDefinedAttributeType = userTypes.First(userType => userType.Name == attributeTypeName);
                attributeTypeName = null;
            }
        }

        public string GetTypePartDDL()
        {
            if (UserDefinedAttributeType == null)
            {
                return AttributeType.ToString();
            }
            else
            {
                return UserDefinedAttributeType.Name.Quote();
            }
        }

        public bool IsUserType()
        {
            return UserDefinedAttributeType != null;
        }
    }
}