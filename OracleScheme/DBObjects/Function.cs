﻿using System;
using System.Collections.Generic;

namespace OracleScheme.DBObjects
{
    [Serializable]
    public class Function : WithDependencies
    {
        public string Code { get; private set; }

        public Function(string name)
        {
            Name = name;
        }

        public void FindCode(Dictionary<SourceObjectKey, string> sourceLines)
        {
            SourceObjectKey key = new SourceObjectKey("FUNCTION", Name);
            this.Code = sourceLines[key];
            sourceLines.Remove(key);
        }

        public override string ObjectType { get { return "FUNCTION"; } }

        public override int SortOrder { get { return 70; } }

        public override void Visit(IOracleObjectVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}