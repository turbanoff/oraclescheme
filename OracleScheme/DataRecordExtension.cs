using System.Data;

namespace OracleScheme
{
    public static class DataRecordExtension
    {
        public static string GetString(this IDataRecord record, string columnName)
        {
            var i = record.GetOrdinal(columnName);
            if (record.IsDBNull(i))
                return null;
            
            return (string) record[i];
        }

        public static decimal GetDecimal(this IDataRecord record, string columnName)
        {
            return (decimal) record[columnName];
        }

        public static int GetInt32(this IDataRecord record, string columnName)
        {
            return (int)(decimal)record[columnName];
        }

        public static char GetChar(this IDataRecord record, string columnName)
        {
            var i = record.GetOrdinal(columnName);
            if (record.IsDBNull(i))
                return default(char);

            object o = record[i];
            return o is string ? ((string) o)[0] : (char) o;
        }

        public static int? GetNullableInt(this IDataRecord record, string columnName)
        {
            var i = record.GetOrdinal(columnName);
            if (record.IsDBNull(i))
                return null;
            return (int)(decimal)record[i];
        }
    }
}