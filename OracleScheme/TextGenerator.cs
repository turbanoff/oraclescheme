﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using OracleScheme.DBObjects;
using OracleScheme.DBObjects.Constraint;

namespace OracleScheme
{
    public sealed class TextGenerator : IOracleObjectVisitor
    {
        readonly StringBuilder sb = new StringBuilder();

        readonly string schemaName;

        public string Script { get { return sb.ToString().Trim(); } }

        public TextGenerator(string schemaName)
        {
            this.schemaName = (schemaName == null) ? null : schemaName.ToUpper();
        }

        /// <summary>Текст для индексов таблицы</summary>
        private void ExportIndexes(Table table, List<TableIndex> usedIndexes)
        {
            foreach (var index in table.Indexes.Except(usedIndexes))
            {
                if (index.Columns.Count == 0)
                {
                    sb.AppendLine("--TODO: index without columns " + index.Name);
                    continue;
                }
                sb.AppendLine().Append("create ");
                if (index.IsUnique)
                {
                    sb.Append("unique ");
                }
                else if (index.IsBitmap)
                {
                    sb.Append("bitmap ");
                }
                sb.Append("index ").AppendSchema(schemaName).AppendIdentifier(index.Name);
                sb.Append(" on ").AppendSchema(schemaName).AppendIdentifier(index.TableName).Append("(");
                foreach (var column in index.Columns)
                {
                    sb.Append(column.Part.Trim()).Append(',');
                }
                sb[sb.Length - 1] = ')';
                sb.Append(';');
            }
        }

        /// <summary>Текст для ограничений таблицы</summary>
        private void ExportConstraints(Table table, List<TableIndex> usedIndexes, out List<ReferenceableConstraint> afterIndexesConstraints)
        {
            afterIndexesConstraints = new List<ReferenceableConstraint>();
            var nonExported = table.Constraints.Where(constraint => !constraint.IsCanBeInlined).Where(IsNonCycleConstraint).OrderBy(c => c.Name);

            foreach (TableConstraint constraint in nonExported)
            {
                if (constraint.Columns.Count == 0 && constraint.ConstraintType != ConstraintType.Check)
                {
                    if (!(constraint.IsWithAutoName &&
                          constraint.ConstraintType == ConstraintType.Unique &&
                          constraint.NonUsedColumns.Count == 1 &&
                          constraint.NonUsedColumns[0].IsWithAutoNestedName &&
                          table.Columns.Any(column => column.IsNestedTable)))
                    {
                        sb.AppendLine("--TODO: fix constraint without columns " + constraint.Name);
                    }
                }
                else if (IsAfterIndexConstraint(constraint))
                {
                    afterIndexesConstraints.Add((ReferenceableConstraint)constraint);
                }
                else
                {
                    ExportConstraint(constraint);
                    if (constraint is ReferenceableConstraint && constraint.IsEnabled)
                        usedIndexes.Add(constraint.Index);
                }
            }
        }

        //для PK и Unique ограничений может быть индекс, не совпадающий по имени или неуникальный,
        //тогда ограничение нужно добавлять после создания индекса
        private static bool IsAfterIndexConstraint(TableConstraint constraint)
        {
            if (!constraint.IsEnabled) return false;
            if (!(constraint is ReferenceableConstraint)) return false;
            if (!constraint.Index.IsUnique) return true;
            if (constraint.IsWithAutoName && constraint.Index.IsWithAutoName) return false;
            return constraint.Name != constraint.Index.Name;
        }

        private void ExportConstraint(TableConstraint constraint)
        {
            if (!constraint.IsWithAutoName)
            {
                sb.Append("constraint ").AppendIdentifier(constraint.Name).Append(' ');
            }
            sb.AppendLine(constraint.GetDDL());
            sb.Append(',');
        }

        private static readonly string[] MostUsedAsDefaultFunctions = {"to_date", "sys_context", "userenv", "sysdate", "sys_guid", "localtimestamp", "systimestamp", "to_char"};

        /// <summary>Текст для столбцов таблицы</summary>
        private void ExportColumns(Table table, out List<TableIndex> usedIndexes)
        {
            usedIndexes = new List<TableIndex>();
            foreach (var column in table.Columns)
            {
                sb.AppendIdentifier(column.Name).Append(' ').Append(column.GetTypePartDDL());
                //Дефолтное значение
                if (column.DefaultValue != null)
                {
                    string defaultValue = column.DefaultValue.Trim();
                    if (!defaultValue.Equals("null", StringComparison.InvariantCultureIgnoreCase) || column.IsVirtual)
                    {
                        sb.Append(column.IsVirtual ? " as (" : " default ");

                        foreach (string func in MostUsedAsDefaultFunctions)
                        {
                            if (defaultValue.StartsWith(func, StringComparison.InvariantCultureIgnoreCase))
                            {
                                sb.Append(func);
                                defaultValue = defaultValue.Substring(func.Length).TrimStart();
                                break;
                            }
                        }
                        sb.Append(defaultValue);
                        
                        if (column.IsVirtual)
                        sb.Append(')');
                    }
                }
                //нужно найти ситемные ограничения, для встаивания прямо в код ddl столбца
                TableColumn localForeachColumn = column;
                var autocons = table.Constraints
                    .Where(constraint => constraint.IsCanBeInlined)
                    .Where(constraint => !IsAfterIndexConstraint(constraint))
                    .Where(IsNonCycleConstraint);
                var thisColumnConstraints =
                    autocons
                    .Where(constraint => constraint.Columns[0] == localForeachColumn)
                    .OrderBy(c => c.ConstraintType == ConstraintType.Check && ((CheckConstraint)c).IsNotNull ? 0 : 1)
                    .ThenBy(constraint => constraint.ConstraintType)
                    .ThenBy(constraint => constraint.ConstraintType == ConstraintType.Check ? ((CheckConstraint)constraint).CheckText : "")
                    .ThenBy(constraint => constraint.Name)
                    .ToList();

                if (thisColumnConstraints.Count > 0)
                {
                    foreach (var thisColumnConstraint in thisColumnConstraints)
                    {
                        sb.Append(' ').Append(thisColumnConstraint.GetColumnDDL());
                        //нужно проверить, есть ли индексы, которые сами создадутся при создании unique/primary key ограничений
                        if (thisColumnConstraint is ReferenceableConstraint && thisColumnConstraint.Index.IsUnique)
                            usedIndexes.Add(thisColumnConstraint.Index);
                    }
                }

                sb.AppendLine().Append(',');
            }
        }

        private static bool IsNonCycleConstraint(TableConstraint constraint)
        {
            var refConstraint = constraint as ReferentialIntegrityConstraint;
            return (refConstraint == null) || !refConstraint.IsCycleReferense;
        }

        /// <summary>Генерирует DDL для ограничения в виде alter table TABLE add constraint</summary>
        private string GenerateDDL(TableConstraint constraint)
        {
            if (constraint.IsCanBeInlined)
            {
                return "alter table " + (schemaName == null ? "" : schemaName + '.') + constraint.TableName.Quote() +
                       " modify " + constraint.Columns[0].Name.Quote() +
                       " " + constraint.GetColumnDDL();
            }
            else if (constraint.IsWithAutoName)
            {
                return "alter table " + (schemaName == null ? "" : schemaName + '.') + constraint.TableName.Quote() +
                   " add " + constraint.GetDDL();
            }
            return "alter table " + (schemaName == null ? "" : schemaName + '.') + constraint.TableName.Quote() +
                   " add constraint " + constraint.Name.Quote() +
                   " " + constraint.GetDDL();
        }

        public void Visit(OracleSchema schema)
        {
            List<WithDependencies> all = schema.Dependencies.ToList();
            sb.Clear()
                .AppendLine("set define off")
                .AppendLine("set sqlblanklines on")
                .AppendLine();

            all.ForEach(obj =>
                        {
                            obj.Visit(this);
                            sb.AppendLine().AppendLine();
                        });
            //Циклические foreign key
            foreach (var constraint in all.OfType<Table>()
                                        .SelectMany(table => table.Constraints)
                                        .Where(constraint => (constraint is ReferentialIntegrityConstraint) && (constraint as ReferentialIntegrityConstraint).IsCycleReferense))
            {
                sb.Append(GenerateDDL(constraint)).AppendLine(";");
            }
        }

        public void Visit(Table table)
        {
            sb.Append(table.IsTemporary ? "create global temporary table " : "create table ");

            sb.AppendSchema(schemaName).AppendIdentifier(table.Name).AppendLine().Append("(");
            List<TableIndex> usedIndexes;
            List<ReferenceableConstraint> afterIndexesConstraints;
            int beforeContentLenght = sb.Length;
            ExportColumns(table, out usedIndexes);
            ExportConstraints(table, usedIndexes, out afterIndexesConstraints);
            if (sb.Length == beforeContentLenght)
            {
                //It's strange but sometime happends
                sb.Append(')');
            }
            else
            {
                sb[sb.Length - 1] = ')';
            }
            if (table.IsTemporary)
            {
                sb.AppendLine().Append(table.IsSessionDuration ? "on commit preserve rows" : "on commit delete rows");
            }
            else
            {
                if (table.IsIndexOrganization)
                {
                    sb.AppendLine().Append("organization index");
                }
                ExternalTable externalDefinition = table.ExternalDefinition;
                if (externalDefinition != null)
                {
                    sb.AppendLine().AppendLine("organization external (");
                    if (externalDefinition.TypeName != "ORACLE_LOADER")
                        sb.Append("  type ").AppendLine(externalDefinition.TypeName);

                    sb.Append("  default directory ").AppendIdentifier(externalDefinition.DefaultDirectoryName).AppendLine()
                      .AppendLine("  access parameters (").Append("    ")
                      .AppendLine(externalDefinition.AccessParameters.Trim())
                      .AppendLine("  )")
                      .Append("  location (").AppendIdentifier(externalDefinition.DirectoryName)
                      .Append(":'").Append(externalDefinition.Location).AppendLine("')")
                      .Append(")");
                }
                if (table.IsPartitioned)
                {
                    ExportPartitions(table);
                }
                //описание nested таблиц
                foreach (TableColumn tableColumn in table.Columns.Where(column => column.IsNestedTable))
                {
                    sb.AppendLine()
                        .Append("nested table ").AppendIdentifier(tableColumn.Name)
                        .Append(" store as ").AppendSchema(schemaName).AppendIdentifier(tableColumn.StoreTable.TableName);
                }
            }
            sb.Append(';');

            ExportIndexes(table, usedIndexes);
            ExportAlterConstraints(afterIndexesConstraints);

            MaterializedViewLog mlog = table.MaterializedViewLog;
            if (mlog != null)
            {
                sb.AppendLine().Append("create materialized view log on ").AppendIdentifier(table.Name);
                if (mlog.IsWithRowId) //with PK by default
                {
                    sb.Append(" with rowid, ");
                    if (mlog.IsWithPrimaryKey)
                        sb.Append("primary key, ");
                    if (mlog.IsWithSequence)
                        sb.Append("sequence, ");
                    sb.Remove(sb.Length - 2, 2);
                }
                else if (mlog.IsWithSequence)
                {
                    sb.Append(" with sequence");
                }
                if (mlog.IsIncludeNewValues)
                    sb.Append(" including new values");
                sb.Append(";");
            }
        }

        private void ExportAlterConstraints(List<ReferenceableConstraint> alterConstraints)
        {
            foreach (TableConstraint constraint in alterConstraints)
            {
                sb.AppendLine().Append(GenerateDDL(constraint)).Append(';');
            }
        }

        /// <summary>Текст для партиций/субпартиций таблицы</summary>
        private void ExportPartitions(Table table)
        {
            PartTable partDescription = table.PartDescription;
            sb.AppendLine().Append("partition by ").Append(partDescription.PartitioningType.ToLower());
            if (partDescription.PartitioningType == "HASH")
            {
                ExportPartColumns(table);
                if (table.Partitions.All(p => p.IsWithAutoName))
                {
                    sb.Append(" partitions ").Append(table.Partitions.Count);
                }
                else
                {
                    sb.AppendLine().Append('(');
                    foreach (TablePartition partition in table.Partitions)
                    {
                        sb.Append("partition ").AppendIdentifier(partition.Name).AppendLine().Append(',');
                    }
                    sb[sb.Length - 1] = ')';
                }
            }
            else if (partDescription.PartitioningType == "LIST")
            {
                ExportPartColumns(table);
                if (partDescription.SubpartitioningType != null)
                {
                    sb.AppendLine().Append("subpartition by ").Append(partDescription.SubpartitioningType.ToLower()).Append(" (");
                    foreach (SubpartitionKeyColumn keyColumn in table.SubpartColulmns)
                    {
                        sb.AppendIdentifier(keyColumn.ColumnName).Append(',');
                    }
                    sb[sb.Length - 1] = ')';
                    if (table.SubpartitonTemplates.Count != 0)
                    {
                        ExportSubpartitionTemplates(table);
                    }
                    else if (IsHashSubpartitonsCountMatch(table))
                    {
                        sb.Append(" subpartitions ").Append(table.Partitions[0].SubpartitionCount);
                    }
                }
                sb.AppendLine().Append('(');
                foreach (TablePartition partition in table.Partitions)
                {
                    sb.Append("partition");
                    if (!partition.IsWithAutoName)
                    {
                        sb.Append(' ').AppendIdentifier(partition.Name);
                    }
                    sb.Append(" values (").Append(partition.HighValue == "DEFAULT" ? "default" : partition.HighValue).AppendLine(")");
                    if (partDescription.SubpartitioningType != null)
                    {
                        ExportSubpartitions(table, partition);
                    }
                    sb.Append(',');
                }
                sb[sb.Length - 1] = ')';
            }
            else if (partDescription.PartitioningType == "RANGE")
            {
                ExportPartColumns(table);
                if (partDescription.Interval != null)
                {
                    sb.Append(" interval (").Append(partDescription.Interval).Append(")");
                }
                if (partDescription.SubpartitioningType != null)
                {
                    sb.AppendLine().Append("subpartition by ").Append(partDescription.SubpartitioningType.ToLower()).Append(" (");
                    foreach (SubpartitionKeyColumn keyColumn in table.SubpartColulmns)
                    {
                        sb.AppendIdentifier(keyColumn.ColumnName).Append(',');
                    }
                    sb[sb.Length - 1] = ')';
                    if (table.SubpartitonTemplates.Count != 0)
                    {
                        ExportSubpartitionTemplates(table);
                    }
                    else if (IsHashSubpartitonsCountMatch(table))
                    {
                        sb.Append(" subpartitions ").Append(table.Partitions[0].SubpartitionCount);
                    }
                }
                sb.AppendLine().Append('(');
                foreach (TablePartition partition in table.Partitions)
                {
                    sb.Append("partition");
                    if (!partition.IsWithAutoName)
                    {
                        sb.Append(' ').AppendIdentifier(partition.Name);
                    }
                    sb.Append(" values less than (").Append(partition.HighValue == "MAXVALUE" ? "maxvalue" : partition.HighValue).AppendLine(")");
                    if (partDescription.SubpartitioningType != null)
                    {
                        ExportSubpartitions(table, partition);
                    }
                    sb.Append(',');
                }
                sb[sb.Length - 1] = ')';
            }
            else if (partDescription.PartitioningType == "REFERENCE")
            {
                sb.Append(" (").AppendIdentifier(partDescription.RefConstraintName).Append(')');
                //нужно найти таблицу, по которую формируются партиции для нашей таблицы
                TableConstraint partConstraint = table.Constraints.Single(c => c.Name == partDescription.RefConstraintName && c.ConstraintType == ConstraintType.ForeignKey);
                List<TablePartition> refTablePartitions = ((ReferentialIntegrityConstraint)partConstraint).RefTableConstraint.ParentTable.Partitions;

                //Перечислять партиции нужны только, если они не совпадают с именами партиций ссылочной таблицы
                bool sameName = true;
                for (int i = 0; i < refTablePartitions.Count; i++)
                {
                    if (refTablePartitions[i].Name != table.Partitions[i].Name && (!refTablePartitions[i].IsWithAutoName || !table.Partitions[i].IsWithAutoName) )
                    {
                        sameName = false;
                        break;
                    }
                }
                if (!sameName)
                {
                    sb.AppendLine().Append('(');
                    foreach (TablePartition partition in table.Partitions)
                    {
                        sb.Append("partition");
                        if (!partition.IsWithAutoName)
                        {
                            sb.Append(' ').AppendIdentifier(partition.Name);
                        }
                        sb.AppendLine().Append(',');
                    }
                    sb[sb.Length - 1] = ')';
                }
            }
            else if (partDescription.PartitioningType == "SYSTEM")
            {
                if (table.Partitions.All(p => p.IsWithAutoName))
                {
                    sb.Append(" partitions ").Append(table.Partitions.Count);
                }
                else
                {
                    sb.AppendLine().Append('(');
                    foreach (TablePartition partition in table.Partitions)
                    {
                        sb.Append("partition ").AppendIdentifier(partition.Name).AppendLine().Append(',');
                    }
                    sb[sb.Length - 1] = ')';
                }
            }
            else
            {
                sb.Append("--TODO unknown partitioning type");
            }
        }

        /// <summary>Проверяет одинаково ли число всех субпатиций, и тип субпатицирования - по хэшу</summary>
        private static bool IsHashSubpartitonsCountMatch(Table table)
        {
            if (table.PartDescription.SubpartitioningType != "HASH")
                return false;
            if (table.SubpartitonTemplates.Count != 0)
                return false;
            return (table.Partitions.Select(p => p.SubpartitionCount).Distinct().Count() == 1) &&
                    table.Partitions.SelectMany(p => p.Subpartitions).All(s => s.IsWithAutoName);
        }

        /// <summary>Текст для шаблонов субпартиций</summary>
        private void ExportSubpartitionTemplates(Table table)
        {
            sb.AppendLine().Append("subpartition template")
              .AppendLine().Append('(');
            foreach (SubpartitionTemplate template in table.SubpartitonTemplates)
            {
                sb.Append("subpartition ").AppendIdentifier(template.Name);
                if (table.PartDescription.SubpartitioningType == "LIST")
                    sb.Append(" values (").Append(template.HighBound).Append(')');
                else if (table.PartDescription.SubpartitioningType == "RANGE")
                    sb.Append(" values less than (").Append(template.HighBound).Append(')');
                sb.AppendLine().Append(',');
            }
            sb[sb.Length - 1] = ')';
        }

        /// <summary>Text for subpartitions for one specific partition</summary>
        private void ExportSubpartitions(Table table, TablePartition partition)
        {
            if (IsHashSubpartitonsCountMatch(table))
                return;
            //Нужно проверить подходит ли шаблон субпартиций для текущий субпартиции - если да, то можно не выводить
            if (table.SubpartitonTemplates.Count > 0 && partition.SubpartitionCount == table.SubpartitonTemplates.Count )
            {
                bool isTemplatePartition = true;
                for (int i = 0; i < partition.Subpartitions.Count; i++)
                {
                    //Если не совпадают верхние границы -> не из шаблона
                    //Если партиция с автоименем, то и субпартиция должна быть с автоименем. Иначе -> не из шаблона
                    //Если партиция не с автоименем, то имя субпартиций должно быть ${имя_партиции}_${имя_шаблонной_субпартиции}. Иначе -> не из шаблона
                    if ((partition.Subpartitions[i].HighValue != table.SubpartitonTemplates[i].HighBound) ||
                        (partition.IsWithAutoName && !partition.Subpartitions[i].IsWithAutoName) ||
                        (!partition.IsWithAutoName && partition.Subpartitions[i].Name != partition.Name + "_" + table.SubpartitonTemplates[i].Name)
                       )
                    {
                        isTemplatePartition = false;
                        break;
                    }
                }
                if (isTemplatePartition)
                    return;
            }

            //Одна субпартиция создается по умолчанию (если не задан шаблон)
            if (partition.SubpartitionCount == 1 && partition.Subpartitions[0].IsWithAutoName && table.SubpartitonTemplates.Count == 0)
            {
                if (partition.Subpartitions[0].HighValue == "MAXVALUE" && table.PartDescription.SubpartitioningType == "RANGE")
                    return;
                if (partition.Subpartitions[0].HighValue == "DEFAULT" && table.PartDescription.SubpartitioningType == "LIST")
                    return;
                if (table.PartDescription.SubpartitioningType == "HASH")
                    return;
            }
                
            sb.Append("  (");
            foreach (TableSubpartition subpart in partition.Subpartitions)
            {
                sb.Append("subpartition ");
                if (!subpart.IsWithAutoName)
                    sb.AppendIdentifier(subpart.Name);
                if (table.PartDescription.SubpartitioningType != "HASH")
                {
                    sb.Append(" values ");
                    if (table.PartDescription.SubpartitioningType == "RANGE")
                        sb.Append("less than ");
                    string defaultSubpartKey = subpart.HighValue;
                    if (defaultSubpartKey.ToUpper() == "MAXVALUE")
                        defaultSubpartKey = "maxvalue";
                    else if (defaultSubpartKey.ToUpper() == "DEFAULT")
                        defaultSubpartKey = "default";
                    sb.Append("(").Append(defaultSubpartKey).Append(')');
                }
                sb.AppendLine().Append("  ,");
            }
            sb[sb.Length - 1] = ')';
            sb.AppendLine();
        }

        private void ExportPartColumns(Table table)
        {
            sb.Append(" (");
            foreach (PartKeyColumn keyColumn in table.PartColumns)
            {
                sb.AppendIdentifier(keyColumn.ColumnName).Append(',');
            }
            sb[sb.Length - 1] = ')';
        }

        public void Visit(View view)
        {
            sb.Append("create or replace view ").AppendSchema(schemaName).AppendIdentifier(view.Name).AppendLine().Append('(');
            foreach (TableColumn column in view.Columns)
            {
                sb.AppendIdentifier(column.Name).AppendLine().Append(',');
            }
            sb[sb.Length - 1] = ')';
            string selectText = view.SelectText.Trim().Trim('\0');
            sb.AppendLine(" as").Append(selectText).Append(';');
        }

        public void Visit(Procedure procedure)
        {
            sb.Append("create or replace ");
            string code = procedure.Code.Trim();
            Regex r = new Regex(@"^procedure\s*(?:\s|"")" + Regex.Escape(procedure.Name) + @"""?\s*(.*)", RegexOptions.IgnoreCase | RegexOptions.Singleline);
            Match match = r.Match(code);
            if (match.Success) {
                sb.Append("procedure ").AppendSchema(schemaName).Append(procedure.Name.Upper());
                string body = match.Groups[1].Value;
                if (!body.StartsWith("("))
                    sb.AppendLine();
                sb.Append(body);
            } else {
                sb.Append(code);
            }
            sb.AppendLine().Append('/');
        }

        public void Visit(Function function)
        {
            sb.Append("create or replace ");
            string code = function.Code.Trim();
            Regex r = new Regex(@"^function\s*(?:\s|"")" + Regex.Escape(function.Name) + @"""?\s*(.*)", RegexOptions.IgnoreCase | RegexOptions.Singleline);
            Match match = r.Match(code);
            if (match.Success) {
                sb.Append("function ").AppendSchema(schemaName).Append(function.Name.Upper());
                string body = match.Groups[1].Value;
                if (!body.StartsWith("("))
                    sb.AppendLine();
                sb.Append(body);
            } else {
                sb.Append(code);
            }
            sb.AppendLine().Append('/');
        }

        public void Visit(Package package)
        {
            sb.Append("create or replace ");
            string code = package.Code.Trim();
            Regex r = new Regex(@"^package\s*(?:\s|"")" + Regex.Escape(package.Name) + @"(?:\s|"")\s*(as|is)\s+(.*)", RegexOptions.IgnoreCase | RegexOptions.Singleline);
            Match match = r.Match(code);
            if (match.Success) {
                sb.Append("package ").AppendSchema(schemaName).AppendLine(package.Name.Upper())
                    .Append(match.Groups[1].Value.ToLower()).AppendLine().Append(match.Groups[2].Value);
            } else {
                sb.Append(code);
            }
            sb.AppendLine().Append('/');
        }

        public void Visit(Trigger trigger)
        {
            sb.Append("create or replace trigger ").AppendSchema(schemaName).AppendIdentifier(trigger.Name).AppendLine();
            switch (trigger.Type)
            {
                case TriggerType.BeforeEachRow:
                case TriggerType.BeforeStatement:
                case TriggerType.BeforeEvent:
                    sb.Append("before "); break;
                case TriggerType.AfterEachRow:
                case TriggerType.AfterStatement:
                case TriggerType.AfterEvent:
                    sb.Append("after "); break;
                case TriggerType.InsteadOf:
                    sb.Append("instead of "); break;
            }
            if ((trigger.TriggeringEvent & TriggeringEventAction.Insert) != 0)
                sb.Append("insert or ");
            if ((trigger.TriggeringEvent & TriggeringEventAction.Delete) != 0)
                sb.Append("delete or ");
            if ((trigger.TriggeringEvent & TriggeringEventAction.Update) != 0)
            {
                sb.Append("update ");
                if (trigger.Columns.Count > 0)
                    sb.Append("of ").Append(String.Join(",", trigger.Columns)).Append(" ");
                sb.Append("or ");
            }

            if ((trigger.TriggeringEvent & TriggeringEventAction.Create) != 0)
                sb.Append("create or ");
            if ((trigger.TriggeringEvent & TriggeringEventAction.DDL) != 0)
                sb.Append("ddl or ");
            if ((trigger.TriggeringEvent & TriggeringEventAction.Alter) != 0)
                sb.Append("alter or ");
            if ((trigger.TriggeringEvent & TriggeringEventAction.Truncate) != 0)
                sb.Append("truncate or ");
            if ((trigger.TriggeringEvent & TriggeringEventAction.Rename) != 0)
                sb.Append("rename or ");
            if ((trigger.TriggeringEvent & TriggeringEventAction.Drop) != 0)
                sb.Append("drop or ");
            if ((trigger.TriggeringEvent & TriggeringEventAction.Logon) != 0)
                sb.Append("logon or ");
            if ((trigger.TriggeringEvent & TriggeringEventAction.Startup) != 0)
                sb.Append("startup or ");

            sb[sb.Length - 2] = 'n'; // or -> on

            if (trigger.TriggerTable != null)
                sb.AppendIdentifier(trigger.TriggerTable.Name).AppendLine();
            else
                sb.Append(trigger.BaseObjectType.Trim().ToLower()).AppendLine();
            
            if (trigger.ReferencingNames != "REFERENCING NEW AS NEW OLD AS OLD")
                sb.AppendLine(trigger.ReferencingNames);
            if (trigger.Type == TriggerType.AfterEachRow || trigger.Type == TriggerType.BeforeEachRow)
                sb.AppendLine("for each row");
            if (!trigger.Enabled)
                sb.AppendLine("disable");
            if (trigger.WhenClause != null)
                sb.Append("when (").Append(trigger.WhenClause.Trim()).AppendLine(")");
            sb.Append(trigger.Body.Trim()).AppendLine().Append('/');
        }

        public void Visit(PackageBody body)
        {
            sb.Append("create or replace ");
            string code = body.Code.Trim();
            Regex r = new Regex(@"^package\s+body\s*(?:\s|"")" + Regex.Escape(body.Name) + @"(?:\s|"")\s*(as|is)\s+(.*)", RegexOptions.IgnoreCase | RegexOptions.Singleline);
            Match match = r.Match(code);
            if (match.Success)
            {
                sb.Append("package body ").AppendSchema(schemaName).AppendLine(body.Name.Upper())
                    .Append(match.Groups[1].Value.ToLower()).AppendLine().Append(match.Groups[2].Value);
            }
            else
            {
                sb.Append(code);
            }
            sb.AppendLine().Append('/');
        }

        public void Visit(Sequence sequence)
        {
            sb.Append("create sequence ").AppendSchema(schemaName).AppendIdentifier(sequence.Name);

            if (sequence.IncrementBy > 0)
            {
                if (sequence.MinValue != 1)
                    sb.Append(" minvalue ").Append(sequence.MinValue);
                if (sequence.LastNumber != sequence.MinValue)
                    sb.Append(" start with ").Append(sequence.LastNumber);
                if (sequence.MaxValue != 9999999999999999999999999999m || sequence.IsCycle)//10^28 - 1
                    sb.Append(" maxvalue ").Append(sequence.MaxValue);
            }
            else
            {
                if (sequence.MinValue != -999999999999999999999999999m)//-10^27 + 1
                    sb.Append(" minvalue ").Append(sequence.MinValue);
                if (sequence.LastNumber != sequence.MaxValue)
                    sb.Append(" start with ").Append(sequence.LastNumber);
                if (sequence.MaxValue != -1 || sequence.IsCycle)
                    sb.Append(" maxvalue ").Append(sequence.MaxValue);
            }

            if (sequence.IncrementBy != 1)
                sb.Append(" increment by ").Append(sequence.IncrementBy);

            if (sequence.IsCycle)
                sb.Append(" cycle");
            if (sequence.IsOrdered)
                sb.Append(" order");

            if (sequence.CacheSize == 0)
                sb.Append(" nocache");
            else if (sequence.CacheSize != 20)
                sb.Append(" cache ").Append(sequence.CacheSize);
            sb.Append(';');
        }

        public void Visit(UserDefinedType userType)
        {
            sb.Append("create or replace type ").AppendSchema(schemaName).AppendIdentifier(userType.Name).Append(" as ");
            if (userType.TypeCode == UserTypeCode.CollectionType)
            {
                CollectionElementType collectionElementType = userType.ElementType;
                switch (collectionElementType.CollType)
                {
                    case CollectionType.Table:
                        sb.Append("table of ");
                        break;
                    case CollectionType.Varray:
                        sb.Append("varray(").Append(collectionElementType.UpperBound).Append(") of ");
                        break;
                    default:
                        sb.Append("undefined of ");
                        break;
                }
                sb.Append(collectionElementType);

            }
            else if (userType.TypeCode == UserTypeCode.ObjectType)
            {
                sb.Append("object");
                if (userType.Attributes.Count > 0 || userType.Methods.Count > 0)
                {
                    sb.AppendLine().Append('(');
                    List<ObjectTypeAttribute> attributes = userType.Attributes;
                    foreach (ObjectTypeAttribute attribute in attributes)
                    {
                        sb.AppendIdentifier(attribute.Name).Append(' ').AppendLine(attribute.GetTypePartDDL()).Append(',');
                    }
                    List<ObjectTypeMethod> methods = userType.Methods;
                    foreach (ObjectTypeMethod method in methods)
                    {
                        List<ObjectTypeMethodParam> parameters = method.Parameters.ToList();
                        if (method.ParametersCount != parameters.Count)
                            sb.AppendLine("--Parameters count mismatch");
    
                        bool isMember = parameters.Count > 0 && parameters[0].ParamName == "SELF" && parameters[0].ParamTypeName == userType.Name;
                        bool isConstructor = isMember && method.IsFunction && method.Name == userType.Name;
                        if (isConstructor)
                        {
                            sb.Append("constructor function ");
                        }
                        else
                        {
                            sb.Append(isMember ? "member " : "static ");
                            sb.Append(method.IsFunction ? "function " : "procedure ");
                        }
                        sb.AppendIdentifier(method.Name);
                        //Первый параметр SELF можно опустить в member функциях
                        if (isMember && parameters.Count > 0 && parameters[0].ParamName == "SELF" && parameters[0].ParamMode == "IN OUT")
                            parameters.RemoveAt(0);
    
                        if (parameters.Count > 0)
                        {
                            sb.Append('(');
                            foreach (ObjectTypeMethodParam t in parameters)
                            {
                                sb.AppendIdentifier(t.ParamName).Append(' ')
                                  .Append(t.ParamMode.ToLower()).Append(' ')
                                  .Append(t.ParamTypeName).Append(", ");
                            }
                            sb.Remove(sb.Length - 2, 2);
                            sb.Append(')');
                        }
                        if (method.IsFunction)
                        {
                            sb.Append(" return ");
                            sb.Append(isConstructor ? "SELF as result" : method.Result);
                        }
                        sb.AppendLine().Append(',');
                    }
                    sb[sb.Length - 1] = ')';
                }
            }
            sb.AppendLine(";").Append('/');

            if (userType.TypeCode == UserTypeCode.ObjectType)
            {
                if (userType.Attributes.Count != userType.AttrubutesCount)
                {
                    sb.AppendLine("--Attrubutes count mismatch");
                }
                if (userType.Methods.Count != userType.MethodsCount)
                {
                    sb.AppendLine("--Methods count mismatch");
                }
            }
        }

        public void Visit(UserDefinedTypeBody typeBody)
        {
            sb.Append("create or replace ");
            Regex r = new Regex(@"^type\s+body\s*(?:\s|"")" + Regex.Escape(typeBody.Name) + @"(?:\s|"")\s*(as|is)\s+(.*)", RegexOptions.IgnoreCase | RegexOptions.Singleline);
            string code = typeBody.Code.Trim();
            Match match = r.Match(code);
            if (match.Success)
            {
                sb.Append("type body ").AppendSchema(schemaName).AppendIdentifier(typeBody.Name)
                    .Append(' ').AppendLine(match.Groups[1].Value).Append(match.Groups[2].Value);
            }
            else
            {
                sb.Append(code);
            }
            sb.AppendLine().Append('/');
        }

        public void Visit(MaterializedView mview)
        {
            sb.Append("create materialized view ").AppendSchema(schemaName).AppendIdentifier(mview.Name).AppendLine().Append('(');
            foreach (TableColumn column in mview.Container.Columns)
            {
                sb.AppendIdentifier(column.Name).AppendLine().Append(',');
            }
            sb[sb.Length - 1] = ')';
            if (mview.Container.IsPartitioned)
            {
                ExportPartitions(mview.Container);
            }
            sb.AppendLine();
            if (mview.BuildMode == "PREBUILT")
            {
                sb.AppendLine("on prebuilt table");
            }
            else
            {
                sb.Append("build ").AppendLine(mview.BuildMode.ToLower());
            }
            sb.Append("refresh ").Append(mview.RefreshMethod.ToLower())
              .Append(" on ").AppendLine(mview.RefreshMode.ToLower());

            if (mview.IsRewriteEnabled)
                sb.AppendLine("enable query rewrite");

            sb.Append("as ").Append(mview.Query.Trim()).Append(';');
        }

        public void Visit(DatabaseLink dblink)
        {
            sb.Append("create database link ").AppendSchema(schemaName).AppendIdentifier(dblink.Name)
                .Append(" connect to ").AppendIdentifier(dblink.Username)
                .Append(" identified by <PWD>")
                .Append(" using '").Append(dblink.Host).Append("';");
        }
    }
}