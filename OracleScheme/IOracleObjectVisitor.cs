﻿using OracleScheme.DBObjects;

namespace OracleScheme
{
    public interface IOracleObjectVisitor
    {
        void Visit(OracleSchema schema);
        void Visit(Table table);
        void Visit(View view);
        void Visit(Procedure procedure);
        void Visit(Function function);
        void Visit(Package package);
        void Visit(Trigger trigger);
        void Visit(PackageBody body);
        void Visit(Sequence sequence);
        void Visit(UserDefinedType type);
        void Visit(UserDefinedTypeBody typeBody);
        void Visit(MaterializedView mview);
        void Visit(DatabaseLink dblink);
    }
}