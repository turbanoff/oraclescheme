﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using OracleScheme.DBObjects;

namespace OracleScheme.Test
{
    [TestFixture]
    public class TextGeneratorTriggerTest
    {

        private static readonly string SCHEMA_NAME = "SN";
        TextGenerator generator;
        TextGenerator generatorWithSchema;

        [SetUp]
        public void Setup()
        {
            generator = new TextGenerator(null);
            generatorWithSchema = new TextGenerator(SCHEMA_NAME);
        }

        [Test]
        public void BeforeEachRow()
        {
            var trigger = new Trigger("MY_TRIGGER", "BEFORE EACH ROW", "INSERT", "MY_TABLE", "TABLE", "REFERENCING NEW AS NEW OLD AS OLD", null, true, "begin null; end;");
            var table = new Table("MY_TABLE", null, null, false);
            trigger.FindTable(new List<Table>{table}, new List<View>(0));
            generator.Visit(trigger);
            generatorWithSchema.Visit(trigger);
            Assert.AreEqual(
                "create or replace trigger MY_TRIGGER" + Environment.NewLine +
                "before insert on MY_TABLE" + Environment.NewLine +
                "for each row" + Environment.NewLine +
                "begin null; end;" + Environment.NewLine +
                "/",
                generator.Script
            );
            Assert.AreEqual(
                "create or replace trigger SN.MY_TRIGGER" + Environment.NewLine +
                "before insert on MY_TABLE" + Environment.NewLine +
                "for each row" + Environment.NewLine +
                "begin null; end;" + Environment.NewLine +
                "/",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void AfterEachRow()
        {
            var trigger = new Trigger("MY_TRIGGER", "AFTER EACH ROW", "INSERT", "MY_TABLE", "TABLE", "REFERENCING NEW AS NEW OLD AS OLD", null, true, "begin null; end;");
            var table = new Table("MY_TABLE", null, null, false);
            trigger.FindTable(new List<Table>{table}, new List<View>(0));
            generator.Visit(trigger);
            generatorWithSchema.Visit(trigger);
            Assert.AreEqual(
                "create or replace trigger MY_TRIGGER" + Environment.NewLine +
                "after insert on MY_TABLE" + Environment.NewLine +
                "for each row" + Environment.NewLine +
                "begin null; end;" + Environment.NewLine +
                "/",
                generator.Script
            );
            Assert.AreEqual(
                "create or replace trigger SN.MY_TRIGGER" + Environment.NewLine +
                "after insert on MY_TABLE" + Environment.NewLine +
                "for each row" + Environment.NewLine +
                "begin null; end;" + Environment.NewLine +
                "/",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void BeforeStatement()
        {
            var trigger = new Trigger("AFTER_TRIGGER", "BEFORE STATEMENT", "UPDATE", "MY_TABLE", "TABLE", "REFERENCING NEW AS NEW OLD AS OLD", null, true, "begin null; end;");
            var table = new Table("MY_TABLE", null, null, false);
            trigger.FindTable(new List<Table>{table}, new List<View>(0));
            trigger.FindColumns(new List<TriggerColumn>(0));
            generator.Visit(trigger);
            generatorWithSchema.Visit(trigger);
            Assert.AreEqual(
                "create or replace trigger AFTER_TRIGGER" + Environment.NewLine +
                "before update on MY_TABLE" + Environment.NewLine +
                "begin null; end;" + Environment.NewLine +
                "/",
                generator.Script
            );
            Assert.AreEqual(
                "create or replace trigger SN.AFTER_TRIGGER" + Environment.NewLine +
                "before update on MY_TABLE" + Environment.NewLine +
                "begin null; end;" + Environment.NewLine +
                "/",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void AfterStatement()
        {
            var trigger = new Trigger("AFTER_TRIGGER", "AFTER STATEMENT", "UPDATE", "MY_TABLE", "TABLE", "REFERENCING NEW AS NEW OLD AS OLD", null, true, "begin null; end;");
            var table = new Table("MY_TABLE", null, null, false);
            trigger.FindTable(new List<Table>{table}, new List<View>(0));
            trigger.FindColumns(new List<TriggerColumn>(0));
            generator.Visit(trigger);
            generatorWithSchema.Visit(trigger);
            Assert.AreEqual(
                "create or replace trigger AFTER_TRIGGER" + Environment.NewLine +
                "after update on MY_TABLE" + Environment.NewLine +
                "begin null; end;" + Environment.NewLine +
                "/",
                generator.Script
            );
            Assert.AreEqual(
                "create or replace trigger SN.AFTER_TRIGGER" + Environment.NewLine +
                "after update on MY_TABLE" + Environment.NewLine +
                "begin null; end;" + Environment.NewLine +
                "/",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void OnLogon()
        {
            var trigger = new Trigger("AFTER_LOGON", "AFTER EVENT", "LOGON ", null, "DATABASE", "REFERENCING NEW AS NEW OLD AS OLD", null, true, "begin null; end;");
            generator.Visit(trigger);
            generatorWithSchema.Visit(trigger);
            Assert.AreEqual(
                "create or replace trigger AFTER_LOGON" + Environment.NewLine +
                "after logon on database" + Environment.NewLine +
                "begin null; end;" + Environment.NewLine +
                "/",
                generator.Script
            );
            Assert.AreEqual(
                "create or replace trigger SN.AFTER_LOGON" + Environment.NewLine +
                "after logon on database" + Environment.NewLine +
                "begin null; end;" + Environment.NewLine +
                "/",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void OnDrop()
        {
            var trigger = new Trigger("BEFORE_DROP", "BEFORE EVENT", "DROP", null, "DATABASE", "REFERENCING NEW AS NEW OLD AS OLD", null, true, "begin null; end;");
            generator.Visit(trigger);
            generatorWithSchema.Visit(trigger);
            Assert.AreEqual(
                "create or replace trigger BEFORE_DROP" + Environment.NewLine +
                "before drop on database" + Environment.NewLine +
                "begin null; end;" + Environment.NewLine +
                "/",
                generator.Script
            );
            Assert.AreEqual(
                "create or replace trigger SN.BEFORE_DROP" + Environment.NewLine +
                "before drop on database" + Environment.NewLine +
                "begin null; end;" + Environment.NewLine +
                "/",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void OnAlter()
        {
            var trigger = new Trigger("BEFORE_ALTER", "BEFORE EVENT", "ALTER", null, "DATABASE", "REFERENCING NEW AS NEW OLD AS OLD", null, true, "begin null; end;");
            generator.Visit(trigger);
            generatorWithSchema.Visit(trigger);
            Assert.AreEqual(
                "create or replace trigger BEFORE_ALTER" + Environment.NewLine +
                "before alter on database" + Environment.NewLine +
                "begin null; end;" + Environment.NewLine +
                "/",
                generator.Script
            );
            Assert.AreEqual(
                "create or replace trigger SN.BEFORE_ALTER" + Environment.NewLine +
                "before alter on database" + Environment.NewLine +
                "begin null; end;" + Environment.NewLine +
                "/",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void OnDdl()
        {
            var trigger = new Trigger("BEFORE_DDL", "BEFORE EVENT", "DDL ", null, "DATABASE", "REFERENCING NEW AS NEW OLD AS OLD", null, true, "begin null; end;");
            generator.Visit(trigger);
            generatorWithSchema.Visit(trigger);
            Assert.AreEqual(
                "create or replace trigger BEFORE_DDL" + Environment.NewLine +
                "before ddl on database" + Environment.NewLine +
                "begin null; end;" + Environment.NewLine +
                "/",
                generator.Script
            );
            Assert.AreEqual(
                "create or replace trigger SN.BEFORE_DDL" + Environment.NewLine +
                "before ddl on database" + Environment.NewLine +
                "begin null; end;" + Environment.NewLine +
                "/",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void OnTruncate()
        {
            var trigger = new Trigger("AFTER_TRUNCATE", "AFTER EVENT", "TRUNCATE", null, "DATABASE", "REFERENCING NEW AS NEW OLD AS OLD", null, true, "begin null; end;");
            generator.Visit(trigger);
            generatorWithSchema.Visit(trigger);
            Assert.AreEqual(
                "create or replace trigger AFTER_TRUNCATE" + Environment.NewLine +
                "after truncate on database" + Environment.NewLine +
                "begin null; end;" + Environment.NewLine +
                "/",
                generator.Script
            );
            Assert.AreEqual(
                "create or replace trigger SN.AFTER_TRUNCATE" + Environment.NewLine +
                "after truncate on database" + Environment.NewLine +
                "begin null; end;" + Environment.NewLine +
                "/",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void OnRename()
        {
            var trigger = new Trigger("AFTER_RENAME", "AFTER EVENT", "RENAME", null, "DATABASE", "REFERENCING NEW AS NEW OLD AS OLD", null, true, "begin null; end;");
            generator.Visit(trigger);
            generatorWithSchema.Visit(trigger);
            Assert.AreEqual(
                "create or replace trigger AFTER_RENAME" + Environment.NewLine +
                "after rename on database" + Environment.NewLine +
                "begin null; end;" + Environment.NewLine +
                "/",
                generator.Script
            );
            Assert.AreEqual(
                "create or replace trigger SN.AFTER_RENAME" + Environment.NewLine +
                "after rename on database" + Environment.NewLine +
                "begin null; end;" + Environment.NewLine +
                "/",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void OnCreate()
        {
            var trigger = new Trigger("AFTER_CREATE", "AFTER EVENT", "CREATE ", null, "DATABASE", "REFERENCING NEW AS NEW OLD AS OLD", null, true, "begin null; end;");
            generator.Visit(trigger);
            generatorWithSchema.Visit(trigger);
            Assert.AreEqual(
                "create or replace trigger AFTER_CREATE" + Environment.NewLine +
                "after create on database" + Environment.NewLine +
                "begin null; end;" + Environment.NewLine +
                "/",
                generator.Script
            );
            Assert.AreEqual(
                "create or replace trigger SN.AFTER_CREATE" + Environment.NewLine +
                "after create on database" + Environment.NewLine +
                "begin null; end;" + Environment.NewLine +
                "/",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void OnStartup()
        {
            var trigger = new Trigger("AFTER_STARTUP", "AFTER EVENT", "STARTUP", null, "DATABASE", "REFERENCING NEW AS NEW OLD AS OLD", null, true, "begin null; end;");
            generator.Visit(trigger);
            generatorWithSchema.Visit(trigger);
            Assert.AreEqual(
                "create or replace trigger AFTER_STARTUP" + Environment.NewLine +
                "after startup on database" + Environment.NewLine +
                "begin null; end;" + Environment.NewLine +
                "/",
                generator.Script
            );
            Assert.AreEqual(
                "create or replace trigger SN.AFTER_STARTUP" + Environment.NewLine +
                "after startup on database" + Environment.NewLine +
                "begin null; end;" + Environment.NewLine +
                "/",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void InsteadOf()
        {
            var trigger = new Trigger("INSTEAD_OF_TRIGGER", "INSTEAD OF", "INSERT OR UPDATE OR DELETE", "MY_VIEW", "VIEW", "REFERENCING NEW AS NEW OLD AS OLD", null, true, "begin null; end;");
            var view = new View("MY_VIEW", "select * from DUAL");
            trigger.FindTable(new List<Table>(0), new List<View>{view});
            trigger.FindColumns(new List<TriggerColumn>(0));
            generator.Visit(trigger);
            generatorWithSchema.Visit(trigger);
            Assert.AreEqual(
                "create or replace trigger INSTEAD_OF_TRIGGER" + Environment.NewLine +
                "instead of insert or delete or update on MY_VIEW" + Environment.NewLine +
                "begin null; end;" + Environment.NewLine +
                "/",
                generator.Script
            );
            Assert.AreEqual(
                "create or replace trigger SN.INSTEAD_OF_TRIGGER" + Environment.NewLine +
                "instead of insert or delete or update on MY_VIEW" + Environment.NewLine +
                "begin null; end;" + Environment.NewLine +
                "/",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void Disabled()
        {
            var trigger = new Trigger("TR_DISABLED", "BEFORE EVENT", "DDL ", null, "DATABASE", "REFERENCING NEW AS NEW OLD AS OLD", null, false, "begin null; end;");
            generator.Visit(trigger);
            generatorWithSchema.Visit(trigger);
            Assert.AreEqual(
                "create or replace trigger TR_DISABLED" + Environment.NewLine +
                "before ddl on database" + Environment.NewLine +
                "disable" + Environment.NewLine +
                "begin null; end;" + Environment.NewLine +
                "/",
                generator.Script
            );
            Assert.AreEqual(
                "create or replace trigger SN.TR_DISABLED" + Environment.NewLine +
                "before ddl on database" + Environment.NewLine +
                "disable" + Environment.NewLine +
                "begin null; end;" + Environment.NewLine +
                "/",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void TwoTriggers()
        {
            var trigger1 = new Trigger("MY_TRIGGER1", "BEFORE EACH ROW", "INSERT", "MY_TABLE", "TABLE", "REFERENCING NEW AS NEW OLD AS OLD", null, true, "begin null; end;");
            var trigger2 = new Trigger("MY_TRIGGER2", "BEFORE EACH ROW", "DELETE", "MY_TABLE", "TABLE", "REFERENCING NEW AS NEW OLD AS OLD", null, true, "begin null; end;");
            var table = new Table("MY_TABLE", null, null, false);
            trigger1.FindTable(new List<Table>{table}, new List<View>(0));
            trigger2.FindTable(new List<Table>{table}, new List<View>(0));

            var schema = new OracleSchema();
            schema.AddDependency(trigger1);
            schema.AddDependency(trigger2);
            
            generator.Visit(schema);
            generatorWithSchema.Visit(schema);
            Assert.AreEqual(
                "set define off" + Environment.NewLine +
                "set sqlblanklines on" + Environment.NewLine + Environment.NewLine +
                "create or replace trigger MY_TRIGGER1" + Environment.NewLine +
                "before insert on MY_TABLE" + Environment.NewLine +
                "for each row" + Environment.NewLine +
                "begin null; end;" + Environment.NewLine +
                "/" + Environment.NewLine + Environment.NewLine +
                "create or replace trigger MY_TRIGGER2" + Environment.NewLine +
                "before delete on MY_TABLE" + Environment.NewLine +
                "for each row" + Environment.NewLine +
                "begin null; end;" + Environment.NewLine +
                "/",
                generator.Script
            );
            Assert.AreEqual(
                "set define off" + Environment.NewLine +
                "set sqlblanklines on" + Environment.NewLine + Environment.NewLine +
                "create or replace trigger SN.MY_TRIGGER1" + Environment.NewLine +
                "before insert on MY_TABLE" + Environment.NewLine +
                "for each row" + Environment.NewLine +
                "begin null; end;" + Environment.NewLine +
                "/" + Environment.NewLine + Environment.NewLine +
                "create or replace trigger SN.MY_TRIGGER2" + Environment.NewLine +
                "before delete on MY_TABLE" + Environment.NewLine +
                "for each row" + Environment.NewLine +
                "begin null; end;" + Environment.NewLine +
                "/",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void WithColumnList()
        {
            var trigger = new Trigger("WITH_COLUMN_LIST", "BEFORE STATEMENT", "UPDATE", "MY_TABLE", "TABLE", "REFERENCING NEW AS NEW OLD AS OLD", null, true, "begin null; end;");
            var table = new Table("MY_TABLE", null, null, false);
            trigger.FindTable(new List<Table>{table}, new List<View>(0));
            var triggerColumn = new TriggerColumn("WITH_COLUMN_LIST", "C");
            trigger.FindColumns(new List<TriggerColumn>{triggerColumn});
            generator.Visit(trigger);
            generatorWithSchema.Visit(trigger);
            Assert.AreEqual(
                "create or replace trigger WITH_COLUMN_LIST" + Environment.NewLine +
                "before update of C on MY_TABLE" + Environment.NewLine +
                "begin null; end;" + Environment.NewLine +
                "/",
                generator.Script
            );
            Assert.AreEqual(
                "create or replace trigger SN.WITH_COLUMN_LIST" + Environment.NewLine +
                "before update of C on MY_TABLE" + Environment.NewLine +
                "begin null; end;" + Environment.NewLine +
                "/",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void NonDefaultReferencingNames()
        {
            var trigger = new Trigger("MY_TRIGGER", "BEFORE EACH ROW", "INSERT", "MY_TABLE", "TABLE", "referencing new as N old as P", null, true, "begin null; end;");
            var table = new Table("MY_TABLE", null, null, false);
            trigger.FindTable(new List<Table>{table}, new List<View>(0));
            generator.Visit(trigger);
            generatorWithSchema.Visit(trigger);
            Assert.AreEqual(
                "create or replace trigger MY_TRIGGER" + Environment.NewLine +
                "before insert on MY_TABLE" + Environment.NewLine +
                "referencing new as N old as P" + Environment.NewLine +
                "for each row" + Environment.NewLine +
                "begin null; end;" + Environment.NewLine +
                "/",
                generator.Script
            );
            Assert.AreEqual(
                "create or replace trigger SN.MY_TRIGGER" + Environment.NewLine +
                "before insert on MY_TABLE" + Environment.NewLine +
                "referencing new as N old as P" + Environment.NewLine +
                "for each row" + Environment.NewLine +
                "begin null; end;" + Environment.NewLine +
                "/",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void WithWhenClause()
        {
            var trigger = new Trigger("WITH_WHEN_CLAUSE", "BEFORE EACH ROW", "INSERT", "MY_TABLE", "TABLE", "REFERENCING NEW AS NEW OLD AS OLD", "NEW.C is null", true, "begin null; end;");
            var table = new Table("MY_TABLE", null, null, false);
            trigger.FindTable(new List<Table>{table}, new List<View>(0));
            generator.Visit(trigger);
            generatorWithSchema.Visit(trigger);
            Assert.AreEqual(
                "create or replace trigger WITH_WHEN_CLAUSE" + Environment.NewLine +
                "before insert on MY_TABLE" + Environment.NewLine +
                "for each row" + Environment.NewLine +
                "when (NEW.C is null)" + Environment.NewLine +
                "begin null; end;" + Environment.NewLine +
                "/",
                generator.Script
            );
            Assert.AreEqual(
                "create or replace trigger SN.WITH_WHEN_CLAUSE" + Environment.NewLine +
                "before insert on MY_TABLE" + Environment.NewLine +
                "for each row" + Environment.NewLine +
                "when (NEW.C is null)" + Environment.NewLine +
                "begin null; end;" + Environment.NewLine +
                "/",
                generatorWithSchema.Script
            );
        }
    }
}
