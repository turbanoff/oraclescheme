﻿create table SYS.DBA_TABLES
(TABLE_NAME text
,DURATION text
,IOT_TYPE text
,PARTITIONED text
,OWNER text
);

create table SYS.DBA_TAB_COLS
(COLUMN_NAME text
,DATA_TYPE text
,DATA_TYPE_OWNER text
,TABLE_NAME text
,DATA_LENGTH decimal
,DATA_PRECISION decimal
,DATA_SCALE decimal
,CHAR_USED char
,COLUMN_ID decimal
,DATA_DEFAULT text
,VIRTUAL_COLUMN text
,HIDDEN_COLUMN text
,OWNER text
);

create table SYS.DBA_CONSTRAINTS
(TABLE_NAME text
,CONSTRAINT_NAME text
,CONSTRAINT_TYPE char
,SEARCH_CONDITION text
,R_CONSTRAINT_NAME text
,INDEX_NAME text
,DELETE_RULE text
,STATUS text
,OWNER text
);

create table SYS.DBA_CONS_COLUMNS
(CONSTRAINT_NAME text
,TABLE_NAME text
,COLUMN_NAME text
,POSITION decimal
,OWNER text
);

create table SYS.DBA_INDEXES
(INDEX_NAME text
,TABLE_NAME text
,INDEX_TYPE text
,UNIQUENESS text
,TABLE_OWNER text
,OWNER text
);

create table SYS.DBA_IND_COLUMNS
(INDEX_NAME text
,COLUMN_NAME text
,COLUMN_POSITION decimal
,INDEX_OWNER text
,TABLE_OWNER text
);

create table SYS.DBA_IND_EXPRESSIONS
(INDEX_NAME text
,TABLE_NAME text
,COLUMN_EXPRESSION text
,COLUMN_POSITION decimal
,INDEX_OWNER text
,TABLE_OWNER text
);

create table SYS.DBA_TYPES
(TYPE_NAME text
,TYPECODE text
,ATTRIBUTES decimal
,METHODS decimal
,OWNER text
);

create table SYS.DBA_TYPE_METHODS
(TYPE_NAME text
,METHOD_NAME text
,METHOD_NO decimal
,METHOD_TYPE text
,RESULTS decimal
,PARAMETERS decimal
,OWNER text
);

create table SYS.DBA_METHOD_PARAMS
(TYPE_NAME text
,METHOD_NAME text
,METHOD_NO decimal
,PARAM_NAME text
,PARAM_NO decimal
,PARAM_MODE text
,PARAM_TYPE_NAME text
,OWNER text
);

create table SYS.DBA_METHOD_RESULTS
(TYPE_NAME text
,METHOD_NAME text
,METHOD_NO decimal
,RESULT_TYPE_NAME text
,OWNER text
);

create table SYS.DBA_TYPE_ATTRS
(TYPE_NAME text
,ATTR_NAME text
,ATTR_TYPE_NAME text
,LENGTH decimal
,PRECISION decimal
,SCALE decimal
,ATTR_NO decimal
,CHAR_USED char
,ATTR_TYPE_OWNER text
,OWNER text
);

create table SYS.DBA_COLL_TYPES
(TYPE_NAME text
,COLL_TYPE text
,UPPER_BOUND text
,ELEM_TYPE_NAME text
,LENGTH decimal
,PRECISION decimal
,SCALE decimal
,CHAR_USED char
,ELEM_TYPE_OWNER text
,OWNER text
);

create table SYS.DBA_NESTED_TABLES
(TABLE_NAME text
,PARENT_TABLE_NAME text
,PARENT_TABLE_COLUMN text
,TABLE_TYPE_OWNER text
,OWNER text
);

create table SYS.DBA_LOBS
(TABLE_NAME text
,COLUMN_NAME text
,INDEX_NAME text
,OWNER text
);

create table SYS.DBA_VIEWS
(VIEW_NAME text
,TEXT text
,OWNER text
);

create table SYS.DBA_DEPENDENCIES
(NAME text
,TYPE text
,REFERENCED_NAME text
,REFERENCED_TYPE text
,REFERENCED_OWNER text
,OWNER text
);

create table SYS.DBA_SOURCE
(NAME text
,TYPE text
,TEXT text
,LINE decimal
,OWNER text
);

create table SYS.DBA_SEQUENCES
(SEQUENCE_NAME text
,MIN_VALUE decimal
,MAX_VALUE decimal
,INCREMENT_BY decimal
,CYCLE_FLAG char
,ORDER_FLAG char
,CACHE_SIZE decimal
,LAST_NUMBER decimal
,SEQUENCE_OWNER text
);

create table SYS.DBA_MVIEW_LOGS
(MASTER text
,LOG_TABLE text
,ROWIDS text
,PRIMARY_KEY text
,SEQUENCE text
,INCLUDE_NEW_VALUES text
,LOG_OWNER text
);

create table SYS.DBA_MVIEWS
(MVIEW_NAME text
,CONTAINER_NAME text
,QUERY text
,REWRITE_ENABLED char
,REFRESH_MODE text
,REFRESH_METHOD text
,BUILD_MODE text
,OWNER text
);

create table SYS.MLOG$
(MASTER text
,LOG text
,TEMP_LOG text
,FLAG int
,MOWNER text
);

create table SYS.DBA_DB_LINKS
(DB_LINK text
,USERNAME text
,HOST text
,OWNER text
);

create table SYS.DBA_TAB_PARTITIONS
(TABLE_NAME text
,PARTITION_NAME text
,SUBPARTITION_COUNT decimal
,HIGH_VALUE text
,PARTITION_POSITION decimal
,TABLE_OWNER text
);

create table SYS.DBA_PART_TABLES
(TABLE_NAME text
,PARTITIONING_TYPE text
,SUBPARTITIONING_TYPE text
,REF_PTN_CONSTRAINT_NAME text
,INTERVAL text
,OWNER text
);

create table SYS.DBA_PART_KEY_COLUMNS
(NAME text
,OBJECT_TYPE text
,COLUMN_NAME text
,COLUMN_POSITION decimal
,OWNER text
);

create table SYS.DBA_TAB_SUBPARTITIONS
(TABLE_NAME text
,PARTITION_NAME text
,SUBPARTITION_NAME text
,HIGH_VALUE text
,SUBPARTITION_POSITION decimal
,TABLE_OWNER text
);

create table SYS.DBA_SUBPART_KEY_COLUMNS
(NAME text
,OBJECT_TYPE text
,COLUMN_NAME text
,COLUMN_POSITION decimal
,OWNER text
);

create table SYS.DBA_SUBPARTITION_TEMPLATES
(TABLE_NAME text
,SUBPARTITION_NAME text
,SUBPARTITION_POSITION decimal
,HIGH_BOUND text
,USER_NAME text
);

create table SYS.DBA_TRIGGERS
(TRIGGER_NAME text
,TRIGGER_TYPE text
,TRIGGERING_EVENT text
,TABLE_NAME text
,BASE_OBJECT_TYPE text
,REFERENCING_NAMES text
,WHEN_CLAUSE text
,STATUS text
,TRIGGER_BODY text
,OWNER text
);

create table SYS.DBA_TRIGGER_COLS
(TRIGGER_NAME text
,COLUMN_NAME text
,COLUMN_LIST text
,TABLE_OWNER text
);

create table SYS.DBA_EXTERNAL_TABLES
(TABLE_NAME text
,TYPE_NAME text
,DEFAULT_DIRECTORY_NAME text
,ACCESS_PARAMETERS text
,OWNER text
);

create table SYS.DBA_EXTERNAL_LOCATIONS
(LOCATION text
,DIRECTORY_NAME text
,TABLE_NAME text
,OWNER text
);

create table SYS.DBA_OBJECTS
(OBJECT_NAME text
,OBJECT_TYPE text
,OWNER text
);