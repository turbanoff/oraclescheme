﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using CsvHelper;
using NUnit.Framework;
using OracleScheme.DBObjects;

namespace OracleScheme.Test
{
    [TestFixture]
    public class OracleSchemaTest
    {
        private readonly Dictionary<string, string> inserts = new Dictionary<string, string>
        {
            {"SYS.DBA_SEQUENCES", "insert into SYS.DBA_SEQUENCES (SEQUENCE_NAME, MIN_VALUE, MAX_VALUE, INCREMENT_BY, CYCLE_FLAG, ORDER_FLAG, CACHE_SIZE, LAST_NUMBER, SEQUENCE_OWNER) values (?,?,?,?,?,?,?,?,?)"},
            {"SYS.DBA_TABLES", "insert into SYS.DBA_TABLES (TABLE_NAME, DURATION, IOT_TYPE, PARTITIONED, OWNER) values (?,?,?,?,?)"},
            {"SYS.DBA_TAB_COLS", "insert into SYS.DBA_TAB_COLS (COLUMN_NAME, DATA_TYPE, DATA_TYPE_OWNER, TABLE_NAME, DATA_LENGTH, DATA_PRECISION, DATA_SCALE, CHAR_USED, COLUMN_ID, DATA_DEFAULT, VIRTUAL_COLUMN, HIDDEN_COLUMN, OWNER) values (?,?,?,?,?,?,?,?,?,?,?,?,?)"},
            {"SYS.DBA_CONSTRAINTS", "insert into SYS.DBA_CONSTRAINTS (TABLE_NAME, CONSTRAINT_NAME, CONSTRAINT_TYPE, SEARCH_CONDITION, R_CONSTRAINT_NAME, INDEX_NAME, DELETE_RULE, STATUS, OWNER) values (?,?,?,?,?,?,?,?,?)"},
            {"SYS.DBA_CONS_COLUMNS", "insert into SYS.DBA_CONS_COLUMNS (CONSTRAINT_NAME, TABLE_NAME, COLUMN_NAME, POSITION, OWNER) values (?,?,?,?,?)"},
            {"SYS.DBA_INDEXES", "insert into SYS.DBA_INDEXES (INDEX_NAME, TABLE_NAME, INDEX_TYPE, UNIQUENESS, TABLE_OWNER, OWNER) values (?,?,?,?,?,?)"},
            {"SYS.DBA_IND_COLUMNS", "insert into SYS.DBA_IND_COLUMNS (INDEX_NAME, COLUMN_NAME, COLUMN_POSITION, INDEX_OWNER, TABLE_OWNER) values (?,?,?,?,?)"},
            {"SYS.DBA_TYPES", "insert into SYS.DBA_TYPES (TYPE_NAME, TYPECODE, ATTRIBUTES, METHODS, OWNER) values (?,?,?,?,?)"},
            {"SYS.DBA_COLL_TYPES", "insert into SYS.DBA_COLL_TYPES (TYPE_NAME, COLL_TYPE, UPPER_BOUND, ELEM_TYPE_NAME, LENGTH, PRECISION, SCALE, CHAR_USED, ELEM_TYPE_OWNER, OWNER) values (?,?,?,?,?,?,?,?,?,?)"},
            {"SYS.DBA_NESTED_TABLES", "insert into SYS.DBA_NESTED_TABLES (TABLE_NAME, PARENT_TABLE_NAME, PARENT_TABLE_COLUMN, TABLE_TYPE_OWNER, OWNER) values (?,?,?,?,?)"},
            {"SYS.DBA_DEPENDENCIES", "insert into SYS.DBA_DEPENDENCIES (NAME, TYPE, REFERENCED_NAME, REFERENCED_TYPE, REFERENCED_OWNER, OWNER) values (?,?,?,?,?,?)"},
            {"SYS.DBA_MVIEW_LOGS", "insert into SYS.DBA_MVIEW_LOGS (MASTER, LOG_TABLE, ROWIDS, PRIMARY_KEY, SEQUENCE, INCLUDE_NEW_VALUES) values (?,?,?,?,?,?)"},
            {"SYS.MLOG$", "insert into SYS.MLOG$ (MASTER, LOG, TEMP_LOG, FLAG, MOWNER) values (?,?,?,?,?)"},
            {"SYS.DBA_PART_TABLES", "insert into SYS.DBA_PART_TABLES (TABLE_NAME, PARTITIONING_TYPE, SUBPARTITIONING_TYPE, REF_PTN_CONSTRAINT_NAME, INTERVAL, OWNER) values (?,?,?,?,?,?)"},
            {"SYS.DBA_TAB_PARTITIONS", "insert into SYS.DBA_TAB_PARTITIONS (TABLE_NAME, PARTITION_NAME, SUBPARTITION_COUNT, HIGH_VALUE, PARTITION_POSITION, TABLE_OWNER) values (?,?,?,?,?,?)"},
            {"SYS.DBA_PART_KEY_COLUMNS", "insert into SYS.DBA_PART_KEY_COLUMNS (NAME, OBJECT_TYPE, COLUMN_NAME, COLUMN_POSITION, OWNER) values (?,?,?,?,?)"},
            {"SYS.DBA_TAB_SUBPARTITIONS", "insert into SYS.DBA_TAB_SUBPARTITIONS (TABLE_NAME, PARTITION_NAME, SUBPARTITION_NAME, HIGH_VALUE, SUBPARTITION_POSITION, TABLE_OWNER) values (?,?,?,?,?,?)"},
            {"SYS.DBA_SUBPART_KEY_COLUMNS", "insert into SYS.DBA_SUBPART_KEY_COLUMNS (NAME, OBJECT_TYPE, COLUMN_NAME, COLUMN_POSITION, OWNER) values (?,?,?,?,?)"},
            {"SYS.DBA_SUBPARTITION_TEMPLATES", "insert into SYS.DBA_SUBPARTITION_TEMPLATES (TABLE_NAME, SUBPARTITION_NAME, SUBPARTITION_POSITION, HIGH_BOUND, USER_NAME) values (?,?,?,?,?)"},
            {"SYS.DBA_EXTERNAL_TABLES", "insert into SYS.DBA_EXTERNAL_TABLES (TABLE_NAME, TYPE_NAME, DEFAULT_DIRECTORY_NAME, ACCESS_PARAMETERS, OWNER) values (?,?,?,?,?)"},
            {"SYS.DBA_EXTERNAL_LOCATIONS", "insert into SYS.DBA_EXTERNAL_LOCATIONS (TABLE_NAME, LOCATION, DIRECTORY_NAME, OWNER) values (?,?,?,?)"},
        };
        const string USER_NAME = "SOME";

        [Test, TestCaseSource("GetSqliteTests")]
        public void TestWithSqlite(string expectedResultFile)
        {
            string sourceFile = Path.ChangeExtension(expectedResultFile, "csv");

            var schema = new OracleSchema(USER_NAME);
            using (var conn = new SQLiteConnection("Data Source=:memory:; BaseSchemaName=SYS"))
            {
                conn.Open();
                using (var ddl = conn.CreateCommand())
                {
                    ddl.CommandText = File.ReadAllText("../../sqlite_schema.sql");
                    ddl.ExecuteNonQuery();
                }
                using (var begin = conn.BeginTransaction())
                {
                    FillFromFile(conn, sourceFile);
                    begin.Commit();
                }

                conn.BindFunction(new SQLiteFunctionAttribute("bitand", 2, FunctionType.Scalar), new BitandFunction());
                conn.BindFunction(new SQLiteFunctionAttribute("decode", 4, FunctionType.Scalar), new DecodeFunction());

                schema.FillFromDatabase(conn);
            }
            schema.Make();
            var generator = new TextGenerator(null);
            schema.Visit(generator);
            string expected = File.ReadAllText(expectedResultFile);
            Assert.AreEqual(expected, generator.Script);
        }

        void FillFromFile(SQLiteConnection conn, string sourceFile)
        {
            using (var streamReader = new StreamReader(sourceFile))
            using (var reader = new CsvReader(streamReader))
            {
                reader.Configuration.HasHeaderRecord = false;
                reader.Configuration.IgnoreBlankLines = false;
                bool canContinue;
                do
                {
                    reader.Read(); // read row with table name
                    string tableName = reader[0];
                    int rows;
                    canContinue = readTable(conn, reader, tableName, out rows);
                    Console.WriteLine("Rows loaded into " + tableName + " = " + rows);
                }
                while (canContinue);
            }
        }

        private bool readTable(SQLiteConnection conn, CsvReader reader, string tableName, out int rows)
        {
            rows = 0;
            string insertText = inserts[tableName];
            int columns = insertText.Count(c => c == '?');
            using (var insert = new SQLiteCommand(insertText, conn))
            {
                while (reader.Read())
                {
                    if (reader[0] == null) //empty line between tables
                    {
                        return true;
                    }
                    Console.WriteLine( string.Join(",", reader.CurrentRecord));
                    for (int i = 0; i < columns - 1; i++)
                    {
                        string value = reader[i];
                        if (value == "") value = null;
                        var parameter = new SQLiteParameter {Value = value};
                        insert.Parameters.Add(parameter);
                    }
                    var owner = new SQLiteParameter {Value = USER_NAME};
                    insert.Parameters.Add(owner);
                    insert.ExecuteNonQuery();
                    insert.Parameters.Clear();
                    rows++;
                }
            }
            return false;
        }

        public static IEnumerable<string> GetSqliteTests()
        {
            return new DirectoryInfo("../../TestData").EnumerateFiles("Test*.sql").Select(f => f.FullName);
        }
    }

    public class BitandFunction : SQLiteFunction
    {
        public override object Invoke(object[] args)
        {
            long value = (long)args[0];
            long bitMask = (long)args[1];
            return value & bitMask;
        }
    }

    public class DecodeFunction : SQLiteFunction
    {
        public override object Invoke(object[] args)
        {
            long value = (long)args[0];
            int i;
            for (i = 1; i < args.Length - 1; i+=2)
            {
                if (value == (long) args[i])
                    return args[i+1];
            }
            return (i == args.Length - 1) ? args[i] : null;
        }
    }
}
