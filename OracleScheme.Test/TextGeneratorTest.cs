﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using OracleScheme.DBObjects;

namespace OracleScheme.Test
{
    [TestFixture]
    public class TextGeneratorTest
    {
        private static readonly string SCHEMA_NAME = "SN";
        TextGenerator generator;
        TextGenerator generatorWithSchema;

        [SetUp]
        public void Setup()
        {
            generator = new TextGenerator(null);
            generatorWithSchema = new TextGenerator(SCHEMA_NAME);
        }

        [Test]
        public void TestEmptyGenerator()
        {
            Assert.AreEqual("", generator.Script);
            Assert.AreEqual("", generatorWithSchema.Script);
        }

        [Test]
        public void TestEmptySchema()
        {
            var schema = new OracleSchema();
            generator.Visit(schema);
            generatorWithSchema.Visit(schema);
            Assert.AreEqual("set define off" + Environment.NewLine + "set sqlblanklines on", generator.Script);
            Assert.AreEqual("set define off" + Environment.NewLine + "set sqlblanklines on", generatorWithSchema.Script);
        }

        [Test]
        public void TestDatabaseLink()
        {
            var link = new DatabaseLink("DB_LINK", "DB_USER", "DB_ADDRESS");
            generator.Visit(link);
            generatorWithSchema.Visit(link);
            Assert.AreEqual("create database link DB_LINK connect to DB_USER identified by <PWD> using 'DB_ADDRESS';", generator.Script);
            Assert.AreEqual("create database link SN.DB_LINK connect to DB_USER identified by <PWD> using 'DB_ADDRESS';", generatorWithSchema.Script);
        }

        [Test]
        public void TestSequenceAndLink()
        {
            var seq = new Sequence("MY_SEQ#", 1, 10, 1, true, true, 20, 5);
            var link = new DatabaseLink("DB.LINK", "DB_USER", "localhost:1521/XE");
            var schema = new OracleSchema();
            schema.AddDependency(seq);
            schema.AddDependency(link);
            generator.Visit(schema);
            generatorWithSchema.Visit(schema);
            Assert.AreEqual(
                "set define off" + Environment.NewLine +
                "set sqlblanklines on" + Environment.NewLine + Environment.NewLine +
                "create sequence MY_SEQ# start with 5 maxvalue 10 cycle order;" + Environment.NewLine + Environment.NewLine +
                "create database link \"DB.LINK\" connect to DB_USER identified by <PWD> using 'localhost:1521/XE';",
                generator.Script
            );
            Assert.AreEqual(
                "set define off" + Environment.NewLine +
                "set sqlblanklines on" + Environment.NewLine + Environment.NewLine +
                "create sequence SN.MY_SEQ# start with 5 maxvalue 10 cycle order;" + Environment.NewLine + Environment.NewLine +
                "create database link SN.\"DB.LINK\" connect to DB_USER identified by <PWD> using 'localhost:1521/XE';",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void TestEmptyPackage()
        {
            var package = new Package("EMPTY_PACK");
            var sources = new Dictionary<SourceObjectKey, string>
            {
                {new SourceObjectKey("PACKAGE", "EMPTY_PACK"), "package EMPTY_PACK as end;"}
            };
            package.FindCode(sources);
            generator.Visit(package);
            generatorWithSchema.Visit(package);
            Assert.AreEqual(
                "create or replace package EMPTY_PACK" + Environment.NewLine +
                "as" + Environment.NewLine +
                "end;" + Environment.NewLine +
                "/",
                generator.Script
            );
            Assert.AreEqual(
                "create or replace package SN.EMPTY_PACK" + Environment.NewLine +
                "as" + Environment.NewLine +
                "end;" + Environment.NewLine +
                "/",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void TestObjectTypeAndPackage()
        {
            var type = new UserDefinedType("Object", "OBJECT", 3, 0);
            var fld1 = new ObjectTypeAttribute("Object", "create", "INTERVAL YEAR TO MONTH", null, 2, null, 1, 'B');
            var fld2 = new ObjectTypeAttribute("Object", "OR", "INTERVAL YEAR TO MONTH", null, 0, null, 2, 'C');
            var fld3 = new ObjectTypeAttribute("Object", "replacE", "INTERVAL YEAR(2) TO MONTH", null, 2, null, 3, default(char));
            type.FindAttributes(new List<ObjectTypeAttribute>{fld1, fld2, fld3}, null);

            var package = new Package("Package");
            var sources = new Dictionary<SourceObjectKey, string>()
            {
                {new SourceObjectKey("PACKAGE", "Package"), "package   \"Package\"  \t as   a int; end;"}
            };
            package.FindCode(sources);

            var schema = new OracleSchema();
            schema.AddDependency(type);
            schema.AddDependency(package);

            generator.Visit(schema);
            generatorWithSchema.Visit(schema);
            Assert.AreEqual(
                "set define off" + Environment.NewLine +
                "set sqlblanklines on" + Environment.NewLine + Environment.NewLine +
                "create or replace type \"Object\" as object" + Environment.NewLine +
                "(\"create\" interval year to month" + Environment.NewLine +
                ",\"OR\" interval year(0) to month" + Environment.NewLine +
                ",\"replacE\" interval year to month" + Environment.NewLine +
                ");" + Environment.NewLine +
                "/" + Environment.NewLine + Environment.NewLine +
                "create or replace package \"Package\"" + Environment.NewLine +
                "as" + Environment.NewLine +
                "a int; end;" + Environment.NewLine +
                "/",
                 generator.Script
            );
            Assert.AreEqual(
                "set define off" + Environment.NewLine +
                "set sqlblanklines on" + Environment.NewLine + Environment.NewLine +
                "create or replace type SN.\"Object\" as object" + Environment.NewLine +
                "(\"create\" interval year to month" + Environment.NewLine +
                ",\"OR\" interval year(0) to month" + Environment.NewLine +
                ",\"replacE\" interval year to month" + Environment.NewLine +
                ");" + Environment.NewLine +
                "/" + Environment.NewLine + Environment.NewLine +
                "create or replace package SN.\"Package\"" + Environment.NewLine +
                "as" + Environment.NewLine +
                "a int; end;" + Environment.NewLine +
                "/",
                 generatorWithSchema.Script
            );
        }

        [Test]
        public void TestPackageWithBody()
        {
            var package = new Package("PACKAGE_WITH_BODY");
            var sources = new Dictionary<SourceObjectKey, string>()
            {
                {new SourceObjectKey("PACKAGE", "PACKAGE_WITH_BODY"), "package PACKAGE_WITH_BODY as procedure a; end;"},
                {new SourceObjectKey("PACKAGE BODY", "PACKAGE_WITH_BODY"), "package body PACKAGE_WITH_BODY as procedure a as begin null; end; end PACKAGE_WITH_BODY;"}
            };
            PackageBody body = package.FindCode(sources);

            var schema = new OracleSchema();
            schema.AddDependency(package);
            schema.AddDependency(body);

            generator.Visit(schema);
            generatorWithSchema.Visit(schema);
            Assert.AreEqual(
                "set define off" + Environment.NewLine +
                "set sqlblanklines on" + Environment.NewLine + Environment.NewLine +
                "create or replace package PACKAGE_WITH_BODY" + Environment.NewLine +
                "as" + Environment.NewLine +
                "procedure a; end;" + Environment.NewLine +
                "/" + Environment.NewLine + Environment.NewLine +
                "create or replace package body PACKAGE_WITH_BODY" + Environment.NewLine +
                "as" + Environment.NewLine +
                "procedure a as begin null; end; end PACKAGE_WITH_BODY;" + Environment.NewLine +
                "/",
                generator.Script
            );
            Assert.AreEqual(
                "set define off" + Environment.NewLine +
                "set sqlblanklines on" + Environment.NewLine + Environment.NewLine +
                "create or replace package SN.PACKAGE_WITH_BODY" + Environment.NewLine +
                "as" + Environment.NewLine +
                "procedure a; end;" + Environment.NewLine +
                "/" + Environment.NewLine + Environment.NewLine +
                "create or replace package body SN.PACKAGE_WITH_BODY" + Environment.NewLine +
                "as" + Environment.NewLine +
                "procedure a as begin null; end; end PACKAGE_WITH_BODY;" + Environment.NewLine +
                "/",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void TestView()
        {
            var view = new View("MY_VIEW", "select DUMMY from DUAL");
            var col1 = new TableColumn("COL1", "CHAR", null, "MY_VIEW", 1, 1, null, null, 'B', null, false);
            var columns = new List<TableColumn>{col1};
            view.FindColumns(columns);

            generator.Visit(view);
            generatorWithSchema.Visit(view);
            Assert.AreEqual(
                "create or replace view MY_VIEW" + Environment.NewLine +
                "(COL1" + Environment.NewLine +
                ") as" + Environment.NewLine +
                "select DUMMY from DUAL;",
                generator.Script
            );
            Assert.AreEqual(
                "create or replace view SN.MY_VIEW" + Environment.NewLine +
                "(COL1" + Environment.NewLine +
                ") as" + Environment.NewLine +
                "select DUMMY from DUAL;",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void TestTwoViews()
        {
            var view1 = new View("MY_VIEW1", "select DUMMY from DUAL");
            var col1 = new TableColumn("COL1", "CHAR", null, "MY_VIEW1", 1, 1, null, null, 'B', null, false);
            view1.FindColumns(new List<TableColumn>{col1});
            var view2 = new View("MY_VIEW2", "select DUMMY from DUAL");
            var col2 = new TableColumn("COL1", "CHAR", null, "MY_VIEW2", 1, 1, null, null, 'B', null, false);
            view2.FindColumns(new List<TableColumn>{col2});
            var schema = new OracleSchema();
            schema.AddDependency(view1);
            schema.AddDependency(view2);

            generator.Visit(schema);
            generatorWithSchema.Visit(schema);
            Assert.AreEqual(
                "set define off" + Environment.NewLine +
                "set sqlblanklines on" + Environment.NewLine + Environment.NewLine +
                "create or replace view MY_VIEW1" + Environment.NewLine +
                "(COL1" + Environment.NewLine +
                ") as" + Environment.NewLine +
                "select DUMMY from DUAL;" + Environment.NewLine + Environment.NewLine +
                "create or replace view MY_VIEW2" + Environment.NewLine +
                "(COL1" + Environment.NewLine +
                ") as" + Environment.NewLine +
                "select DUMMY from DUAL;",
                generator.Script
            );
            Assert.AreEqual(
                "set define off" + Environment.NewLine +
                "set sqlblanklines on" + Environment.NewLine + Environment.NewLine +
                "create or replace view SN.MY_VIEW1" + Environment.NewLine +
                "(COL1" + Environment.NewLine +
                ") as" + Environment.NewLine +
                "select DUMMY from DUAL;" + Environment.NewLine + Environment.NewLine +
                "create or replace view SN.MY_VIEW2" + Environment.NewLine +
                "(COL1" + Environment.NewLine +
                ") as" + Environment.NewLine +
                "select DUMMY from DUAL;",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void TestFunction()
        {
            var func = new Function("MY_FUNC");
            var sources = new Dictionary<SourceObjectKey, string>
            {
                {new SourceObjectKey("FUNCTION", "MY_FUNC"), "function MY_FUNC return integer as begin return null; end;"}
            };
            func.FindCode(sources);
            generator.Visit(func);
            generatorWithSchema.Visit(func);
            Assert.AreEqual(
                "create or replace function MY_FUNC" + Environment.NewLine +
                "return integer as begin return null; end;" + Environment.NewLine +
                "/",
                generator.Script
            );
            Assert.AreEqual(
                "create or replace function SN.MY_FUNC" + Environment.NewLine +
                "return integer as begin return null; end;" + Environment.NewLine +
                "/",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void TestFunctionWithParameter()
        {
            var func = new Function("WITH_PARAM");
            var sources = new Dictionary<SourceObjectKey, string>
            {
                {new SourceObjectKey("FUNCTION", "WITH_PARAM"), "function WITH_PARAM(A in number) return integer as begin return a; end;"}
            };
            func.FindCode(sources);
            generator.Visit(func);
            generatorWithSchema.Visit(func);
            Assert.AreEqual(
                "create or replace function WITH_PARAM(A in number) return integer as begin return a; end;" + Environment.NewLine +
                "/",
                generator.Script
            );
            Assert.AreEqual(
                "create or replace function SN.WITH_PARAM(A in number) return integer as begin return a; end;" + Environment.NewLine +
                "/",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void TestFunctionWithComment()
        {
            var func = new Function("WITH_COMMENT");
            var sources = new Dictionary<SourceObjectKey, string>
            {
                {new SourceObjectKey("FUNCTION", "WITH_COMMENT"), "function/*some comment*/WITH_COMMENT return integer as begin return null; end;"}
            };
            func.FindCode(sources);
            generator.Visit(func);
            generatorWithSchema.Visit(func);
            Assert.AreEqual(
                "create or replace function/*some comment*/WITH_COMMENT return integer as begin return null; end;" + Environment.NewLine +
                "/",
                generator.Script
            );
            Assert.AreEqual(
                "create or replace function/*some comment*/WITH_COMMENT return integer as begin return null; end;" + Environment.NewLine +
                "/",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void TestFunctionWithingQuotes()
        {
            var func = new Function("WITHOUT_SPACE");
            var sources = new Dictionary<SourceObjectKey, string>
            {
                {new SourceObjectKey("FUNCTION", "WITHOUT_SPACE"), "function\"WITHOUT_SPACE\" return integer as begin return null; end;"}
            };
            func.FindCode(sources);
            generator.Visit(func);
            generatorWithSchema.Visit(func);
            Assert.AreEqual(
                "create or replace function WITHOUT_SPACE" + Environment.NewLine +
                "return integer as begin return null; end;" + Environment.NewLine +
                "/",
                generator.Script
            );
            Assert.AreEqual(
                "create or replace function SN.WITHOUT_SPACE" + Environment.NewLine +
                "return integer as begin return null; end;" + Environment.NewLine +
                "/",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void TestFunctionAndPackage()
        {
            var func = new Function("MY_FUNC");
            var package = new Package("MY_PACK");
            var sources = new Dictionary<SourceObjectKey, string>
            {
                {new SourceObjectKey("FUNCTION", "MY_FUNC"), "function MY_FUNC return char as begin return null end;"},
                {new SourceObjectKey("PACKAGE", "MY_PACK"), "package MY_PACK as end;"},
            };
            func.FindCode(sources);
            package.FindCode(sources);
            var schema = new OracleSchema();
            schema.AddDependency(func);
            schema.AddDependency(package);

            generator.Visit(schema);
            generatorWithSchema.Visit(schema);
            Assert.AreEqual(
                "set define off" + Environment.NewLine +
                "set sqlblanklines on" + Environment.NewLine + Environment.NewLine +
                "create or replace function MY_FUNC" + Environment.NewLine +
                "return char as begin return null end;" + Environment.NewLine +
                "/" + Environment.NewLine + Environment.NewLine +
                "create or replace package MY_PACK" + Environment.NewLine +
                "as" + Environment.NewLine +
                "end;" + Environment.NewLine +
                "/",
                generator.Script
            );
            Assert.AreEqual(
                "set define off" + Environment.NewLine +
                "set sqlblanklines on" + Environment.NewLine + Environment.NewLine +
                "create or replace function SN.MY_FUNC" + Environment.NewLine +
                "return char as begin return null end;" + Environment.NewLine +
                "/" + Environment.NewLine + Environment.NewLine +
                "create or replace package SN.MY_PACK" + Environment.NewLine +
                "as" + Environment.NewLine +
                "end;" + Environment.NewLine +
                "/",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void TestProcedure()
        {
            var proc = new Procedure("MY_PROC");
            var sources = new Dictionary<SourceObjectKey, string>
            {
                {new SourceObjectKey("PROCEDURE", "MY_PROC"), "ProceDure MY_PROC IS begin null; end;"},
            };
            proc.FindCode(sources);

            generator.Visit(proc);
            generatorWithSchema.Visit(proc);
            Assert.AreEqual(
                "create or replace procedure MY_PROC" + Environment.NewLine +
                "IS begin null; end;" + Environment.NewLine +
                "/",
                generator.Script
            );
            Assert.AreEqual(
                "create or replace procedure SN.MY_PROC" + Environment.NewLine +
                "IS begin null; end;" + Environment.NewLine +
                "/",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void TestProcedureWithComment()
        {
            var proc = new Procedure("WITH_COMMENTS");
            var sources = new Dictionary<SourceObjectKey, string>
            {
                {new SourceObjectKey("PROCEDURE", "WITH_COMMENTS"), "procedure/*comment*/WITH_COMMENTS is begin null; end;"},
            };
            proc.FindCode(sources);

            generator.Visit(proc);
            generatorWithSchema.Visit(proc);
            Assert.AreEqual(
                "create or replace procedure/*comment*/WITH_COMMENTS is begin null; end;" + Environment.NewLine +
                "/",
                generator.Script
            );
            Assert.AreEqual(
                "create or replace procedure/*comment*/WITH_COMMENTS is begin null; end;" + Environment.NewLine +
                "/",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void TestProcedureWithinQuotas()
        {
            var proc = new Procedure("WITH_QUOTAS");
            var sources = new Dictionary<SourceObjectKey, string>
            {
                {new SourceObjectKey("PROCEDURE", "WITH_QUOTAS"), "procedure\"WITH_QUOTAS\"   is   begin null; end;"},
            };
            proc.FindCode(sources);

            generator.Visit(proc);
            generatorWithSchema.Visit(proc);
            Assert.AreEqual(
                "create or replace procedure WITH_QUOTAS" + Environment.NewLine +
                "is   begin null; end;" + Environment.NewLine +
                "/",
                generator.Script
            );
            Assert.AreEqual(
                "create or replace procedure SN.WITH_QUOTAS" + Environment.NewLine +
                "is   begin null; end;" + Environment.NewLine +
                "/",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void TestTwoProcedures()
        {
            var proc1 = new Procedure("MY_PROC1");
            var proc2 = new Procedure("MY_PROC2");
            var sources = new Dictionary<SourceObjectKey, string>
            {
                {new SourceObjectKey("PROCEDURE", "MY_PROC1"), "PROCEDURE MY_PROC1(A in number) as begin return; end;"},
                {new SourceObjectKey("PROCEDURE", "MY_PROC2"), "procedurE MY_PROC2() IS begin null; end;"},
            };
            proc1.FindCode(sources);
            proc2.FindCode(sources);

            var schema = new OracleSchema();
            schema.AddDependency(proc1);
            schema.AddDependency(proc2);

            generator.Visit(schema);
            generatorWithSchema.Visit(schema);
            Assert.AreEqual(
                "set define off" + Environment.NewLine +
                "set sqlblanklines on" + Environment.NewLine + Environment.NewLine +
                "create or replace procedure MY_PROC1(A in number) as begin return; end;" + Environment.NewLine +
                "/" + Environment.NewLine + Environment.NewLine+
                "create or replace procedure MY_PROC2() IS begin null; end;" + Environment.NewLine +
                "/",
                generator.Script
            );
            Assert.AreEqual(
                "set define off" + Environment.NewLine +
                "set sqlblanklines on" + Environment.NewLine + Environment.NewLine +
                "create or replace procedure SN.MY_PROC1(A in number) as begin return; end;" + Environment.NewLine +
                "/" + Environment.NewLine + Environment.NewLine+
                "create or replace procedure SN.MY_PROC2() IS begin null; end;" + Environment.NewLine +
                "/",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void TestPackageWithBodyAndSequence()
        {
            var package = new Package("MY_PACK");
            var sources = new Dictionary<SourceObjectKey, string>()
            {
                {new SourceObjectKey("PACKAGE", "MY_PACK"), "package MY_PACK as procedure a; end;"},
                {new SourceObjectKey("PACKAGE BODY", "MY_PACK"), "package body MY_PACK as procedure a as begin null; end; end;"}
            };
            PackageBody body = package.FindCode(sources);
            var seq = new Sequence("MY_SEQ", 0, 9999999999999999999999999999m, 1, false, false, 20, 1);

            var schema = new OracleSchema();
            schema.AddDependency(package);
            schema.AddDependency(body);
            schema.AddDependency(seq);

            generator.Visit(schema);
            generatorWithSchema.Visit(schema);
            Assert.AreEqual(
                "set define off" + Environment.NewLine +
                "set sqlblanklines on" + Environment.NewLine + Environment.NewLine +
                "create or replace package MY_PACK" + Environment.NewLine +
                "as" + Environment.NewLine +
                "procedure a; end;" + Environment.NewLine +
                "/" + Environment.NewLine + Environment.NewLine +
                "create or replace package body MY_PACK" + Environment.NewLine +
                "as" + Environment.NewLine +
                "procedure a as begin null; end; end;" + Environment.NewLine +
                "/" + Environment.NewLine + Environment.NewLine +
                "create sequence MY_SEQ minvalue 0 start with 1;",
                generator.Script
            );
            Assert.AreEqual(
                "set define off" + Environment.NewLine +
                "set sqlblanklines on" + Environment.NewLine + Environment.NewLine +
                "create or replace package SN.MY_PACK" + Environment.NewLine +
                "as" + Environment.NewLine +
                "procedure a; end;" + Environment.NewLine +
                "/" + Environment.NewLine + Environment.NewLine +
                "create or replace package body SN.MY_PACK" + Environment.NewLine +
                "as" + Environment.NewLine +
                "procedure a as begin null; end; end;" + Environment.NewLine +
                "/" + Environment.NewLine + Environment.NewLine +
                "create sequence SN.MY_SEQ minvalue 0 start with 1;",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void TestPackageWithComment()
        {
            var package = new Package("MY_PACK");
            var sources = new Dictionary<SourceObjectKey, string>()
            {
                {new SourceObjectKey("PACKAGE", "MY_PACK"), "package/*comment*/MY_PACK as procedure a; end;"},
                {new SourceObjectKey("PACKAGE BODY", "MY_PACK"), "package body/*comment*/MY_PACK as procedure a as begin null; end; end;"}
            };
            PackageBody body = package.FindCode(sources);

            var schema = new OracleSchema();
            schema.AddDependency(package);
            schema.AddDependency(body);

            generator.Visit(schema);
            generatorWithSchema.Visit(schema);
            Assert.AreEqual(
                "set define off" + Environment.NewLine +
                "set sqlblanklines on" + Environment.NewLine + Environment.NewLine +
                "create or replace package/*comment*/MY_PACK as procedure a; end;" + Environment.NewLine +
                "/" + Environment.NewLine + Environment.NewLine +
                "create or replace package body/*comment*/MY_PACK as procedure a as begin null; end; end;" + Environment.NewLine +
                "/",
                generator.Script
            );
            Assert.AreEqual(
                "set define off" + Environment.NewLine +
                "set sqlblanklines on" + Environment.NewLine + Environment.NewLine +
                "create or replace package/*comment*/MY_PACK as procedure a; end;" + Environment.NewLine +
                "/" + Environment.NewLine + Environment.NewLine +
                "create or replace package body/*comment*/MY_PACK as procedure a as begin null; end; end;" + Environment.NewLine +
                "/",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void TestPackageWithQuotas()
        {
            var package = new Package("MY_PACK");
            var sources = new Dictionary<SourceObjectKey, string>()
            {
                {new SourceObjectKey("PACKAGE", "MY_PACK"), "package\"MY_PACK\"as procedure a; end;"},
                {new SourceObjectKey("PACKAGE BODY", "MY_PACK"), "package body\"MY_PACK\"as procedure a as begin null; end; end;"}
            };
            PackageBody body = package.FindCode(sources);

            var schema = new OracleSchema();
            schema.AddDependency(package);
            schema.AddDependency(body);

            generator.Visit(schema);
            generatorWithSchema.Visit(schema);
            Assert.AreEqual(
                "set define off" + Environment.NewLine +
                "set sqlblanklines on" + Environment.NewLine + Environment.NewLine +
                "create or replace package MY_PACK" + Environment.NewLine +
                "as" + Environment.NewLine +
                "procedure a; end;" + Environment.NewLine +
                "/" + Environment.NewLine + Environment.NewLine +
                "create or replace package body MY_PACK" + Environment.NewLine +
                "as" + Environment.NewLine +
                "procedure a as begin null; end; end;" + Environment.NewLine +
                "/",
                generator.Script
            );
            Assert.AreEqual(
                "set define off" + Environment.NewLine +
                "set sqlblanklines on" + Environment.NewLine + Environment.NewLine +
                "create or replace package SN.MY_PACK" + Environment.NewLine +
                "as" + Environment.NewLine +
                "procedure a; end;" + Environment.NewLine +
                "/" + Environment.NewLine + Environment.NewLine +
                "create or replace package body SN.MY_PACK" + Environment.NewLine +
                "as" + Environment.NewLine +
                "procedure a as begin null; end; end;" + Environment.NewLine +
                "/",
                generatorWithSchema.Script
            );
        }
    }
}
