﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Moq;
using NUnit.Framework;
using OracleScheme.DBObjects;
using OracleScheme.DBObjects.Constraint;

namespace OracleScheme.Test
{
    [TestFixture]
    public class TextGeneratorTableTest
    {
        private static readonly string SCHEMA_NAME = "SN";
        TextGenerator generator;
        TextGenerator generatorWithSchema;

        [SetUp]
        public void Setup()
        {
            generator = new TextGenerator(null);
            generatorWithSchema = new TextGenerator(SCHEMA_NAME);
        }

        [Test]
        public void TestEmpty()
        {
            var table = new Table("EMPTY", null, null, false);
            FillDefault(table);
            generator.Visit(table);
            generatorWithSchema.Visit(table);
            Assert.AreEqual("create table EMPTY" + Environment.NewLine +"();", generator.Script);
            Assert.AreEqual("create table SN.EMPTY" + Environment.NewLine +"();", generatorWithSchema.Script);
        }

        [Test]
        public void TestOneColumn()
        {
            var table = new Table("ONE_COLUMN", null, null, false);
            FillDefault(table);
            var column = new TableColumn("COL", "NUMBER", null, "ONE_COLUMN", 1, 22, null, null, default(char), null, false);
            table.FindColumns(new List<TableColumn>{column});
            generator.Visit(table);
            generatorWithSchema.Visit(table);
            Assert.AreEqual("create table ONE_COLUMN" + Environment.NewLine + "(COL number" + Environment.NewLine + ");", generator.Script);
            Assert.AreEqual("create table SN.ONE_COLUMN" + Environment.NewLine + "(COL number" + Environment.NewLine + ");", generatorWithSchema.Script);
        }

        [Test]
        public void TestMultipleColumns()
        {
            var table = new Table("MANY_COLUMNS",  null, null, false);
            FillDefault(table);
            var column1 = new TableColumn("COL1", "CHAR", null, "MANY_COLUMNS", 1, 1, null, null, 'B', null, false);
            var column2 = new TableColumn("COL2", "CHAR", null, "MANY_COLUMNS", 2, 4000, null, null, 'C', null, false);
            table.FindColumns(new List<TableColumn>{column1, column2});
            generator.Visit(table);
            generatorWithSchema.Visit(table);
            string expected =
                "MANY_COLUMNS" + Environment.NewLine +
                "(COL1 char(1 byte)" + Environment.NewLine +
                ",COL2 char(4000 char)" + Environment.NewLine +
                ");";
            Assert.AreEqual("create table " + expected, generator.Script);
            Assert.AreEqual("create table SN." + expected, generatorWithSchema.Script);
        }

        [Test]
        public void TestTwoTables()
        {
            var table1 = new Table("FIRST_TABLE", null, null, false);
            FillDefault(table1);
            var column1 = new TableColumn("COL", "DATE", null, "FIRST_TABLE", 1, 7, null, null, default(char), null, false);
            table1.FindColumns(new List<TableColumn>{column1});
            generator.Visit(table1);

            var table2 = new Table("SECOND_TABLE", null, null, false);
            FillDefault(table2);
            var column2 = new TableColumn("COL", "DATE", null, "SECOND_TABLE", 1, 8, null, null, default(char), null, false);
            table2.FindColumns(new List<TableColumn>{column2});

            var schema = new OracleSchema();
            schema.AddDependency(table1);
            schema.AddDependency(table2);
            generator.Visit(schema);
            generatorWithSchema.Visit(schema);

            Assert.AreEqual(
                "set define off" + Environment.NewLine +
                "set sqlblanklines on" + Environment.NewLine + Environment.NewLine +
                "create table FIRST_TABLE" + Environment.NewLine +
                "(COL date" + Environment.NewLine +
                ");" + Environment.NewLine + Environment.NewLine +
                "create table SECOND_TABLE" + Environment.NewLine +
                "(COL date" + Environment.NewLine +
                ");"
               , generator.Script
            );
            Assert.AreEqual(
                "set define off" + Environment.NewLine +
                "set sqlblanklines on" + Environment.NewLine + Environment.NewLine +
                "create table SN.FIRST_TABLE" + Environment.NewLine +
                "(COL date" + Environment.NewLine +
                ");" + Environment.NewLine + Environment.NewLine +
                "create table SN.SECOND_TABLE" + Environment.NewLine +
                "(COL date" + Environment.NewLine +
                ");"
               , generatorWithSchema.Script
            );
        }

        private void FillDefault(Table table)
        {
            table.FindColumns(new List<TableColumn>(0));
            table.FindConstraints(new List<TableConstraint>(0));
            table.FindIndexes(new List<TableIndex>(0), new List<Lob>(0));
            table.FindSubpartitionTemplates(new List<SubpartitionTemplate>(0));
        }

        [Test]
        public void TestMaterializedView()
        {
            var mview = new MaterializedView("MY_MVIEW", "MY_MVIEW", "select DUMMY from DUAL", true, "DEMAND", "FAST", "IMMEDIATE");
            mview.Container = new Table("MY_MVIEW", null, null, false);
            FillDefault(mview.Container);
            var col = new TableColumn("COL", "VARCHAR2", null, "MY_MVIEW", 1, 1, null, null, default(char), null, false);
            mview.Container.FindColumns(new List<TableColumn>{col});

            generator.Visit(mview);
            generatorWithSchema.Visit(mview);
            Assert.AreEqual(
                "create materialized view MY_MVIEW" + Environment.NewLine +
                "(COL" + Environment.NewLine +
                ")" + Environment.NewLine +
                "build immediate" + Environment.NewLine +
                "refresh fast on demand" + Environment.NewLine +
                "enable query rewrite" + Environment.NewLine +
                "as select DUMMY from DUAL;",
                generator.Script
            );
            Assert.AreEqual(
                "create materialized view SN.MY_MVIEW" + Environment.NewLine +
                "(COL" + Environment.NewLine +
                ")" + Environment.NewLine +
                "build immediate" + Environment.NewLine +
                "refresh fast on demand" + Environment.NewLine +
                "enable query rewrite" + Environment.NewLine +
                "as select DUMMY from DUAL;",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void TestMaterializedViewAndTable()
        {
            var mview = new MaterializedView("MY_MVIEW", "MY_MVIEW", "select DUMMY from DUAL", true, "DEMAND", "FAST", "IMMEDIATE");
            mview.Container = new Table("MY_MVIEW", null, null, false) {ParentMaterializedView = mview};
            FillDefault(mview.Container);
            var col = new TableColumn("COL", "VARCHAR2", null, "MY_MVIEW", 1, 1, null, null, default(char), null, false);
            mview.Container.FindColumns(new List<TableColumn>{col});

            var table = new Table("MY_TABLE", null, null, false);
            FillDefault(table);
            var tcol = new TableColumn("ALL", "NCHAR", null, "MY_TABLE", 1, 20, null, null, 'C', null, false);
            table.FindColumns(new List<TableColumn>{tcol});
            
            var schema = new OracleSchema();
            schema.AddDependency(mview);
            schema.AddDependency(table);

            generator.Visit(schema);
            generatorWithSchema.Visit(schema);
            Assert.AreEqual(
                "set define off" + Environment.NewLine +
                "set sqlblanklines on" + Environment.NewLine + Environment.NewLine +
                "create materialized view MY_MVIEW" + Environment.NewLine +
                "(COL" + Environment.NewLine +
                ")" + Environment.NewLine +
                "build immediate" + Environment.NewLine +
                "refresh fast on demand" + Environment.NewLine +
                "enable query rewrite" + Environment.NewLine +
                "as select DUMMY from DUAL;" + Environment.NewLine + Environment.NewLine +
                "create table MY_TABLE" + Environment.NewLine +
                "(\"ALL\" nchar(20)" + Environment.NewLine +
                ");",
                generator.Script
            );
            Assert.AreEqual(
                "set define off" + Environment.NewLine +
                "set sqlblanklines on" + Environment.NewLine + Environment.NewLine +
                "create materialized view SN.MY_MVIEW" + Environment.NewLine +
                "(COL" + Environment.NewLine +
                ")" + Environment.NewLine +
                "build immediate" + Environment.NewLine +
                "refresh fast on demand" + Environment.NewLine +
                "enable query rewrite" + Environment.NewLine +
                "as select DUMMY from DUAL;" + Environment.NewLine + Environment.NewLine +
                "create table SN.MY_TABLE" + Environment.NewLine +
                "(\"ALL\" nchar(20)" + Environment.NewLine +
                ");",
                generatorWithSchema.Script
            );
        }

        public TableConstraint CreateCheckConstraint(string name, string tableName, string checkText, string indexName = null, bool enabled = true)
        {
            var row = new Mock<IDataRecord>();
            row.Setup(r => r.GetOrdinal("CONSTRAINT_TYPE")).Returns(0);
            row.SetupGet(r => r[0]).Returns('C');
            row.Setup(r => r.GetOrdinal("CONSTRAINT_NAME")).Returns(1);
            row.SetupGet(r => r[1]).Returns(name);
            row.Setup(r => r.GetOrdinal("TABLE_NAME")).Returns(2);
            row.SetupGet(r => r[2]).Returns(tableName);
            row.Setup(r => r.GetOrdinal("SEARCH_CONDITION")).Returns(3);
            row.SetupGet(r => r[3]).Returns(checkText);
            row.Setup(r => r.GetOrdinal("INDEX_NAME")).Returns(4);
            row.SetupGet(r => r[4]).Returns(indexName);
            row.Setup(r => r.GetOrdinal("STATUS")).Returns(5);
            row.SetupGet(r => r[5]).Returns(enabled ? "ENABLED" : "DISABLED");
            return TableConstraint.Create(row.Object);
        }

        public TableConstraint CreateUniqueConstraint(string name, string tableName, string indexName = null, bool enabled = true)
        {
            var row = new Mock<IDataRecord>();
            row.Setup(r => r.GetOrdinal("CONSTRAINT_TYPE")).Returns(0);
            row.SetupGet(r => r[0]).Returns('U');
            row.Setup(r => r.GetOrdinal("CONSTRAINT_NAME")).Returns(1);
            row.SetupGet(r => r[1]).Returns(name);
            row.Setup(r => r.GetOrdinal("TABLE_NAME")).Returns(2);
            row.SetupGet(r => r[2]).Returns(tableName);
            row.Setup(r => r.GetOrdinal("INDEX_NAME")).Returns(4);
            row.SetupGet(r => r[4]).Returns(indexName);
            row.Setup(r => r.GetOrdinal("STATUS")).Returns(5);
            row.SetupGet(r => r[5]).Returns(enabled ? "ENABLED" : "DISABLED");
            return TableConstraint.Create(row.Object);
        }

        public TableConstraint CreatePrimaryKey(string name, string tableName, string indexName = null, bool enabled = true)
        {
            var row = new Mock<IDataRecord>();
            row.Setup(r => r.GetOrdinal("CONSTRAINT_TYPE")).Returns(0);
            row.SetupGet(r => r[0]).Returns('P');
            row.Setup(r => r.GetOrdinal("CONSTRAINT_NAME")).Returns(1);
            row.SetupGet(r => r[1]).Returns(name);
            row.Setup(r => r.GetOrdinal("TABLE_NAME")).Returns(2);
            row.SetupGet(r => r[2]).Returns(tableName);
            row.Setup(r => r.GetOrdinal("INDEX_NAME")).Returns(4);
            row.SetupGet(r => r[4]).Returns(indexName);
            row.Setup(r => r.GetOrdinal("STATUS")).Returns(5);
            row.SetupGet(r => r[5]).Returns(enabled ? "ENABLED" : "DISABLED");
            return TableConstraint.Create(row.Object);
        }

        public ReferentialIntegrityConstraint CreateForeignKey(string name, string tableName, string refConstraintName, string deleteRule = "NO ACTION", bool enabled = true)
        {
            var row = new Mock<IDataRecord>();
            row.Setup(r => r.GetOrdinal("CONSTRAINT_TYPE")).Returns(0);
            row.SetupGet(r => r[0]).Returns('R');
            row.Setup(r => r.GetOrdinal("CONSTRAINT_NAME")).Returns(1);
            row.SetupGet(r => r[1]).Returns(name);
            row.Setup(r => r.GetOrdinal("TABLE_NAME")).Returns(2);
            row.SetupGet(r => r[2]).Returns(tableName);
            row.Setup(r => r.GetOrdinal("R_CONSTRAINT_NAME")).Returns(3);
            row.SetupGet(r => r[3]).Returns(refConstraintName);
            row.Setup(r => r.GetOrdinal("DELETE_RULE")).Returns(4);
            row.SetupGet(r => r[4]).Returns(deleteRule);
            row.Setup(r => r.GetOrdinal("STATUS")).Returns(5);
            row.SetupGet(r => r[5]).Returns(enabled ? "ENABLED" : "DISABLED");
            row.Setup(r => r.GetOrdinal("INDEX_NAME")).Returns(6);
            row.SetupGet(r => r[6]).Returns(null);
            return (ReferentialIntegrityConstraint)TableConstraint.Create(row.Object);
        }

        [Test]
        public void TestInlineNotNullConstraint()
        {
            var constraint = CreateCheckConstraint("SYS_C025000", "MY_TABLE", "\"COL\" IS NOT NULL");
            var table = new Table("MY_TABLE", null, null, false);
            FillDefault(table);
            var column = new TableColumn("COL", "TIMESTAMP(9)", null, "MY_TABLE", 1, 11, null, 9, 'C', null, false);
            table.FindColumns(new List<TableColumn>{column});
            table.FindConstraints(new List<TableConstraint>{constraint});
            var ccolumn = new ConstraintColumn("COL", "MY_TABLE", "SYS_C025000", 1);
            constraint.FindColumns(new List<ConstraintColumn>{ccolumn}, new List<TableColumn>{column});

            generator.Visit(table);
            generatorWithSchema.Visit(table);
            Assert.AreEqual(
                "create table MY_TABLE" + Environment.NewLine +
                "(COL timestamp(9) not null" + Environment.NewLine +
                ");",
                generator.Script
            );
            Assert.AreEqual(
                "create table SN.MY_TABLE" + Environment.NewLine +
                "(COL timestamp(9) not null" + Environment.NewLine +
                ");",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void TestInlineDisabledConstraint()
        {
            var constraint = CreateCheckConstraint("SYS_C025000", "MY_TABLE", "\"COL\" IS NOT NULL", null, false);
            var table = new Table("MY_TABLE", null, null, false);
            FillDefault(table);
            var column = new TableColumn("COL", "TIMESTAMP(9)", null, "MY_TABLE", 1, 11, null, 9, 'C', null, false);
            table.FindColumns(new List<TableColumn>{column});
            table.FindConstraints(new List<TableConstraint>{constraint});
            var ccolumn = new ConstraintColumn("COL", "MY_TABLE", "SYS_C025000", 1);
            constraint.FindColumns(new List<ConstraintColumn>{ccolumn}, new List<TableColumn>{column});

            generator.Visit(table);
            generatorWithSchema.Visit(table);
            Assert.AreEqual(
                "create table MY_TABLE" + Environment.NewLine +
                "(COL timestamp(9) not null disable" + Environment.NewLine +
                ");",
                generator.Script
            );
            Assert.AreEqual(
                "create table SN.MY_TABLE" + Environment.NewLine +
                "(COL timestamp(9) not null disable" + Environment.NewLine +
                ");",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void TestNoInlineNotNullConstraint()
        {
            var constraint = CreateCheckConstraint("MY_CNSRT", "MY_TABLE", "\"COL\" IS NOT NULL");
            var table = new Table("MY_TABLE", null, null, false);
            FillDefault(table);
            var column = new TableColumn("COL", "NVARCHAR2", null, "MY_TABLE", 1, 5, null, null, 'C', null, false);
            table.FindColumns(new List<TableColumn>{column});
            table.FindConstraints(new List<TableConstraint>{constraint});
            var ccolumn = new ConstraintColumn("COL", "MY_TABLE", "MY_CNSRT", 1);
            constraint.FindColumns(new List<ConstraintColumn>{ccolumn}, new List<TableColumn>{column});

            generator.Visit(table);
            generatorWithSchema.Visit(table);
            Assert.AreEqual(
                "create table MY_TABLE" + Environment.NewLine +
                "(COL nvarchar2(5)" + Environment.NewLine +
                ",constraint MY_CNSRT check (COL is not null)" + Environment.NewLine +
                ");",
                generator.Script
            );
            Assert.AreEqual(
                "create table SN.MY_TABLE" + Environment.NewLine +
                "(COL nvarchar2(5)" + Environment.NewLine +
                ",constraint MY_CNSRT check (COL is not null)" + Environment.NewLine +
                ");",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void TestInlineUniqueConstraint()
        {
            var table = new Table("CLMN", null, null, false);
            FillDefault(table);
            var column = new TableColumn("L", "NUMBER", null, "CLMN", 1, 22, 22, 0, 'C', null, false);
            var columns = new List<TableColumn>{column};
            table.FindColumns(columns);
            var index = new TableIndex("SYS_C000001", "CLMN", "NORMAL", true);
            table.FindIndexes(new List<TableIndex>{index}, new List<Lob>(0));
            var indexCol = new IndexColumn("SYS_C000001", "L", 1);
            index.FindColumns(new List<IndexColumn>{indexCol}, columns, new List<IndexExpression>(0));
            TableConstraint constraint = CreateUniqueConstraint("SYS_C000001", "CLMN", "SYS_C000001");
            table.FindConstraints(new List<TableConstraint>{constraint});
            var constraintColumn = new ConstraintColumn("L", "CLMN", "SYS_C000001", 1);
            constraint.FindColumns(new List<ConstraintColumn>{constraintColumn}, columns);
            constraint.FindIndex(new List<TableIndex>{index});

            generator.Visit(table);
            generatorWithSchema.Visit(table);
            Assert.AreEqual(
                "create table CLMN" + Environment.NewLine +
                "(L number(22) unique" + Environment.NewLine +
                ");",
                generator.Script
            );
            Assert.AreEqual(
                "create table SN.CLMN" + Environment.NewLine +
                "(L number(22) unique" + Environment.NewLine +
                ");",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void TestNotInlineUniqueConstraint()
        {
            var table = new Table("TRANSACTION", null, null, false);
            FillDefault(table);
            var column = new TableColumn("L", "NUMBER", null, "TRANSACTION", 1, 22, 22, 0, 'C', null, false);
            var columns = new List<TableColumn>{column};
            table.FindColumns(columns);
            var index = new TableIndex("U_TRANSACTION_L", "TRANSACTION", "NORMAL", true);
            table.FindIndexes(new List<TableIndex>{index}, new List<Lob>(0));
            var indexCol = new IndexColumn("U_TRANSACTION_L", "L", 1);
            index.FindColumns(new List<IndexColumn>{indexCol}, columns, new List<IndexExpression>(0));
            TableConstraint constraint = CreateUniqueConstraint("U_TRANSACTION_L", "TRANSACTION", "U_TRANSACTION_L");
            table.FindConstraints(new List<TableConstraint>{constraint});
            var constraintColumn = new ConstraintColumn("L", "TRANSACTION", "U_TRANSACTION_L", 1);
            constraint.FindColumns(new List<ConstraintColumn>{constraintColumn}, columns);
            constraint.FindIndex(new List<TableIndex>{index});

            generator.Visit(table);
            generatorWithSchema.Visit(table);
            Assert.AreEqual(
                "create table TRANSACTION" + Environment.NewLine +
                "(L number(22)" + Environment.NewLine +
                ",constraint U_TRANSACTION_L unique (L)" + Environment.NewLine +
                ");",
                generator.Script
            );
            Assert.AreEqual(
                "create table SN.TRANSACTION" + Environment.NewLine +
                "(L number(22)" + Environment.NewLine +
                ",constraint U_TRANSACTION_L unique (L)" + Environment.NewLine +
                ");",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void TestManyColumnsPrimaryKey()
        {
            var table = new Table("WEB_USER", null, null, false);
            FillDefault(table);
            var id = new TableColumn("ID", "NUMBER", null, "WEB_USER", 1, 22, 22, 0, 'C', null, false);
            var regDate = new TableColumn("REG_DATE", "TIMESTAMP(6) WITH TIME ZONE", null, "WEB_USER", 2, 20, null, 6, 'B', null, false);
            var columns = new List<TableColumn>{id, regDate};
            table.FindColumns(columns);

            var index = new TableIndex("SYS_C1030416", "WEB_USER", "NORMAL", true);
            table.FindIndexes(new List<TableIndex>{index}, new List<Lob>(0));
            var indexColId = new IndexColumn("SYS_C1030416", "ID", 1);
            var indexColRegDate = new IndexColumn("SYS_C1030416", "REG_DATE", 2);
            index.FindColumns(new List<IndexColumn>{indexColId, indexColRegDate}, columns, new List<IndexExpression>(0));

            TableConstraint constraint = CreatePrimaryKey("SYS_C1030416", "WEB_USER", "SYS_C1030416");
            table.FindConstraints(new List<TableConstraint>{constraint});
            var constrColId = new ConstraintColumn("ID", "WEB_USER", "SYS_C1030416", 1);
            var constrColRegDate = new ConstraintColumn("REG_DATE", "WEB_USER", "SYS_C1030416", 2);
            constraint.FindColumns(new List<ConstraintColumn>{constrColId, constrColRegDate}, columns);
            constraint.FindIndex(new List<TableIndex>{index});

            generator.Visit(table);
            generatorWithSchema.Visit(table);
            Assert.AreEqual(
                "create table WEB_USER" + Environment.NewLine +
                "(ID number(22)" + Environment.NewLine +
                ",REG_DATE timestamp with time zone" + Environment.NewLine +
                ",primary key (ID,REG_DATE)" + Environment.NewLine +
                ");",
                generator.Script
            );
            Assert.AreEqual(
                "create table SN.WEB_USER" + Environment.NewLine +
                "(ID number(22)" + Environment.NewLine +
                ",REG_DATE timestamp with time zone" + Environment.NewLine +
                ",primary key (ID,REG_DATE)" + Environment.NewLine +
                ");",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void TestInlineUniqueConstraintEvenWithDifferentAutoName()
        {
            var table = new Table("CLMN", null, null, false);
            FillDefault(table);
            var column = new TableColumn("L", "NUMBER", null, "CLMN", 1, 18, 17, 2, default(char), null, false);
            var columns = new List<TableColumn>{column};
            table.FindColumns(columns);
            var index = new TableIndex("SYS_C007401", "CLMN", "NORMAL", true);
            table.FindIndexes(new List<TableIndex>{index}, new List<Lob>(0));
            var indexCol = new IndexColumn("SYS_C007401", "L", 1);
            index.FindColumns(new List<IndexColumn>{indexCol}, columns, new List<IndexExpression>(0));
            TableConstraint constraint = CreateUniqueConstraint("SYS_C000001", "CLMN", "SYS_C007401");
            table.FindConstraints(new List<TableConstraint>{constraint});
            var constraintColumn = new ConstraintColumn("L", "CLMN", "SYS_C000001", 1);
            constraint.FindColumns(new List<ConstraintColumn>{constraintColumn}, columns);
            constraint.FindIndex(new List<TableIndex>{index});

            generator.Visit(table);
            generatorWithSchema.Visit(table);
            Assert.AreEqual(
                "create table CLMN" + Environment.NewLine +
                "(L number(17,2) unique" + Environment.NewLine +
                ");",
                generator.Script
            );
            Assert.AreEqual(
                "create table SN.CLMN" + Environment.NewLine +
                "(L number(17,2) unique" + Environment.NewLine +
                ");",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void TestAfterNormalIndexUniqueConstraint()
        {
            var table = new Table("CLMN", null, null, false);
            FillDefault(table);
            var column = new TableColumn("L", "NUMBER", null, "CLMN", 1, 18, 17, 2, default(char), null, false);
            var columns = new List<TableColumn>{column};
            table.FindColumns(columns);
            var index = new TableIndex("U_CLMN", "CLMN", "NORMAL", false);
            table.FindIndexes(new List<TableIndex>{index}, new List<Lob>(0));
            var indexCol = new IndexColumn("U_CLMN", "L", 1);
            index.FindColumns(new List<IndexColumn>{indexCol}, columns, new List<IndexExpression>(0));
            TableConstraint constraint = CreateUniqueConstraint("U_CLMN", "CLMN", "U_CLMN");
            table.FindConstraints(new List<TableConstraint>{constraint});
            var constraintColumn = new ConstraintColumn("L", "CLMN", "U_CLMN", 1);
            constraint.FindColumns(new List<ConstraintColumn>{constraintColumn}, columns);
            constraint.FindIndex(new List<TableIndex>{index});

            generator.Visit(table);
            generatorWithSchema.Visit(table);
            Assert.AreEqual(
                "create table CLMN" + Environment.NewLine +
                "(L number(17,2)" + Environment.NewLine +
                ");" + Environment.NewLine +
                "create index U_CLMN on CLMN(L);" + Environment.NewLine +
                "alter table CLMN add constraint U_CLMN unique (L);",
                generator.Script
            );
            Assert.AreEqual(
                "create table SN.CLMN" + Environment.NewLine +
                "(L number(17,2)" + Environment.NewLine +
                ");" + Environment.NewLine +
                "create index SN.U_CLMN on SN.CLMN(L);" + Environment.NewLine +
                "alter table SN.CLMN add constraint U_CLMN unique (L);",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void TestNotNullWithTwoColumns()
        {
            var constraint = CreateCheckConstraint("MY_CNSRT", "MY_TABLE", "COL2 is not null and COL1 IS NOT NULL");
            var table = new Table("MY_TABLE", null, null, false);
            FillDefault(table);
            var column1 = new TableColumn("COL1", "BINARY_FLOAT", null, "MY_TABLE", 1, 8, null, null, 'C', null, false);
            var column2 = new TableColumn("COL2", "BINARY_DOUBLE", null, "MY_TABLE", 2, 8, null, null, 'C', null, false);
            table.FindColumns(new List<TableColumn>{column1, column2});
            table.FindConstraints(new List<TableConstraint>{constraint});
            var ccolumn1 = new ConstraintColumn("COL1", "MY_TABLE", "MY_CNSRT", 1);
            var ccolumn2 = new ConstraintColumn("COL2", "MY_TABLE", "MY_CNSRT", 1);
            constraint.FindColumns(new List<ConstraintColumn>{ccolumn1, ccolumn2}, new List<TableColumn>{column1, column2});

            generator.Visit(table);
            generatorWithSchema.Visit(table);
            Assert.AreEqual(
                "create table MY_TABLE" + Environment.NewLine +
                "(COL1 binary_float" + Environment.NewLine +
                ",COL2 binary_double" + Environment.NewLine +
                ",constraint MY_CNSRT check (COL2 is not null and COL1 IS NOT NULL)" + Environment.NewLine +
                ");",
                generator.Script
            );
            Assert.AreEqual(
                "create table SN.MY_TABLE" + Environment.NewLine +
                "(COL1 binary_float" + Environment.NewLine +
                ",COL2 binary_double" + Environment.NewLine +
                ",constraint MY_CNSRT check (COL2 is not null and COL1 IS NOT NULL)" + Environment.NewLine +
                ");",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void TestColumnWithDefaultValue()
        {
            var table = new Table("WITH_COLUMN_DEFAULT", null, null, false);
            FillDefault(table);
            var column = new TableColumn("COL", "NUMBER", null, "WITH_COLUMN_DEFAULT", 1, 22, null, 0, 'C', "0", false);
            table.FindColumns(new List<TableColumn>{column});
            generator.Visit(table);
            generatorWithSchema.Visit(table);
            Assert.AreEqual("create table WITH_COLUMN_DEFAULT" + Environment.NewLine + "(COL integer default 0" + Environment.NewLine + ");", generator.Script);
            Assert.AreEqual("create table SN.WITH_COLUMN_DEFAULT" + Environment.NewLine + "(COL integer default 0" + Environment.NewLine + ");", generatorWithSchema.Script);
        }

        [Test]
        public void TestColumnWitKnownDefault()
        {
            var table = new Table("KNOWN_DEFAULT", null, null, false);
            FillDefault(table);
            var column = new TableColumn("T", "TIMESTAMP(6)", null, "KNOWN_DEFAULT", 1, 11, null, 6, default(char), "SYSTIMESTAMP ", false);
            table.FindColumns(new List<TableColumn>{column});
            generator.Visit(table);
            generatorWithSchema.Visit(table);
            Assert.AreEqual("create table KNOWN_DEFAULT" + Environment.NewLine + "(T timestamp default systimestamp" + Environment.NewLine + ");", generator.Script);
            Assert.AreEqual("create table SN.KNOWN_DEFAULT" + Environment.NewLine + "(T timestamp default systimestamp" + Environment.NewLine + ");", generatorWithSchema.Script);
        }

        [Test]
        public void TestVirtualColumn()
        {
            var table = new Table("WITH_VIRTUAL_COLUMN", null, null, false);
            FillDefault(table);
            var column1 = new TableColumn("SRC", "FLOAT", null, "WITH_VIRTUAL_COLUMN", 1, 22, 126, null, 'B', null, true);
            var column2 = new TableColumn("DST", "NUMBER", null, "WITH_VIRTUAL_COLUMN", 1, 22, null, null, 'C', "\"SRC\"/2", true);
            table.FindColumns(new List<TableColumn>{column1, column2});
            generator.Visit(table);
            generatorWithSchema.Visit(table);
            Assert.AreEqual(
                "create table WITH_VIRTUAL_COLUMN" + Environment.NewLine +
                "(SRC float" + Environment.NewLine +
                ",DST number as (\"SRC\"/2)" + Environment.NewLine +
                ");", generator.Script
            );
            Assert.AreEqual(
                "create table SN.WITH_VIRTUAL_COLUMN" + Environment.NewLine +
                "(SRC float" + Environment.NewLine +
                ",DST number as (\"SRC\"/2)" + Environment.NewLine +
                ");", generatorWithSchema.Script
            );
        }

        [Test]
        public void TestTemporaryOnCommit()
        {
            var table = new Table("TEMP_TABLE", "SYS$TRANSACTION", null, false);
            FillDefault(table);
            var column = new TableColumn("SRC", "FLOAT", null, "TEMP_TABLE", 1, 22, 120, null, 'B', null, false);
            table.FindColumns(new List<TableColumn>{column});
            generator.Visit(table);
            generatorWithSchema.Visit(table);
            Assert.AreEqual(
                "create global temporary table TEMP_TABLE" + Environment.NewLine +
                "(SRC float(120)" + Environment.NewLine +
                ")" + Environment.NewLine +
                "on commit delete rows;",
                generator.Script
            );
            Assert.AreEqual(
                "create global temporary table SN.TEMP_TABLE" + Environment.NewLine +
                "(SRC float(120)" + Environment.NewLine +
                ")" + Environment.NewLine +
                "on commit delete rows;",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void TestTemporaryOnDisconnect()
        {
            var table = new Table("TEMP_SESSION", "SYS$SESSION", null, false);
            FillDefault(table);
            var column = new TableColumn("R", "LONG RAW", null, "TEMP_SESSION", 1, 0, null, null, default(char), null, false);
            table.FindColumns(new List<TableColumn>{column});
            generator.Visit(table);
            generatorWithSchema.Visit(table);
            Assert.AreEqual(
                "create global temporary table TEMP_SESSION" + Environment.NewLine +
                "(R long raw" + Environment.NewLine +
                ")" + Environment.NewLine +
                "on commit preserve rows;",
                generator.Script
            );
            Assert.AreEqual(
                "create global temporary table SN.TEMP_SESSION" + Environment.NewLine +
                "(R long raw" + Environment.NewLine +
                ")" + Environment.NewLine +
                "on commit preserve rows;",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void TestIndexOrganization()
        {
            var table = new Table("ON_INDEX_TABLE", null, "IOT", false);
            FillDefault(table);
            var column = new TableColumn("R", "NUMBER", null, "ON_INDEX_TABLE", 1, 20, 20, 0, default(char), null, false);
            table.FindColumns(new List<TableColumn>{column});
            generator.Visit(table);
            generatorWithSchema.Visit(table);
            Assert.AreEqual(
                "create table ON_INDEX_TABLE" + Environment.NewLine +
                "(R number(20)" + Environment.NewLine +
                ")" + Environment.NewLine +
                "organization index;",
                generator.Script
            );
            Assert.AreEqual(
                "create table SN.ON_INDEX_TABLE" + Environment.NewLine +
                "(R number(20)" + Environment.NewLine +
                ")" + Environment.NewLine +
                "organization index;",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void TestListPartitioning()
        {
            var table = new Table("LIST_T", null, null, true);
            FillDefault(table);
            var column = new TableColumn("IS_V", "NUMBER", null, "LIST_T", 1, 1, 1, 0, 'B', null, false);
            table.FindColumns(new List<TableColumn>{column});
            var part = new PartTable("LIST_T", "LIST", "NONE", null, null);
            table.FindPart(new List<PartTable>{part});
            var partColumn = new PartKeyColumn("LIST_T", "TABLE", "IS_V", 1);
            table.FindPartKeyColumns(new List<PartKeyColumn>{partColumn});
            var part1 = new TablePartition("LIST_T", "PART_ZERO", 0, "0", 1);
            var part2 = new TablePartition("LIST_T", "PART_DEF", 0, "DEFAULT", 2);
            table.FindPartitions(new List<TablePartition>{part1, part2});
            generator.Visit(table);
            generatorWithSchema.Visit(table);
            Assert.AreEqual(
                "create table LIST_T" + Environment.NewLine +
                "(IS_V number(1)" + Environment.NewLine +
                ")" + Environment.NewLine +
                "partition by list (IS_V)" + Environment.NewLine +
                "(partition PART_ZERO values (0)" + Environment.NewLine +
                ",partition PART_DEF values (default)" + Environment.NewLine +
                ");",
                generator.Script
            );
            Assert.AreEqual(
                "create table SN.LIST_T" + Environment.NewLine +
                "(IS_V number(1)" + Environment.NewLine +
                ")" + Environment.NewLine +
                "partition by list (IS_V)" + Environment.NewLine +
                "(partition PART_ZERO values (0)" + Environment.NewLine +
                ",partition PART_DEF values (default)" + Environment.NewLine +
                ");",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void TestRangePartition()
        {
            var table = new Table("RANGE_T", null, null, true);
            FillDefault(table);
            var column = new TableColumn("Q", "NUMBER", null, "RANGE_T", 1, 1, 1, 0, 'B', null, false);
            table.FindColumns(new List<TableColumn>{column});
            var part = new PartTable("RANGE_T", "RANGE", "NONE", null, null);
            table.FindPart(new List<PartTable>{part});
            var partColumn = new PartKeyColumn("RANGE_T", "TABLE", "Q", 1);
            table.FindPartKeyColumns(new List<PartKeyColumn>{partColumn});
            var part1 = new TablePartition("RANGE_T", "PART_NEG", 0, "0", 1);
            var part2 = new TablePartition("RANGE_T", "PART_POS", 0, "MAXVALUE", 2);
            table.FindPartitions(new List<TablePartition>{part1, part2});
            generator.Visit(table);
            generatorWithSchema.Visit(table);
            Assert.AreEqual(
                "create table RANGE_T" + Environment.NewLine +
                "(Q number(1)" + Environment.NewLine +
                ")" + Environment.NewLine +
                "partition by range (Q)" + Environment.NewLine +
                "(partition PART_NEG values less than (0)" + Environment.NewLine +
                ",partition PART_POS values less than (maxvalue)" + Environment.NewLine +
                ");",
                generator.Script
            );
            Assert.AreEqual(
                "create table SN.RANGE_T" + Environment.NewLine +
                "(Q number(1)" + Environment.NewLine +
                ")" + Environment.NewLine +
                "partition by range (Q)" + Environment.NewLine +
                "(partition PART_NEG values less than (0)" + Environment.NewLine +
                ",partition PART_POS values less than (maxvalue)" + Environment.NewLine +
                ");",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void TestHashPartition()
        {
            var table = new Table("HASH_T", null, null, true);
            FillDefault(table);
            var column = new TableColumn("E", "NUMBER", null, "HASH_T", 1, 1, 1, 0, 'B', null, false);
            table.FindColumns(new List<TableColumn>{column});
            var part = new PartTable("HASH_T", "HASH", "NONE", null, null);
            table.FindPart(new List<PartTable>{part});
            var partColumn = new PartKeyColumn("HASH_T", "TABLE", "E", 1);
            table.FindPartKeyColumns(new List<PartKeyColumn>{partColumn});
            var part1 = new TablePartition("HASH_T", "PART_1", 0, null, 1);
            var part2 = new TablePartition("HASH_T", "PART_2", 0, null, 2);
            table.FindPartitions(new List<TablePartition>{part1, part2});
            generator.Visit(table);
            generatorWithSchema.Visit(table);
            Assert.AreEqual(
                "create table HASH_T" + Environment.NewLine +
                "(E number(1)" + Environment.NewLine +
                ")" + Environment.NewLine +
                "partition by hash (E)" + Environment.NewLine +
                "(partition PART_1" + Environment.NewLine +
                ",partition PART_2" + Environment.NewLine +
                ");",
                generator.Script
            );
            Assert.AreEqual(
                "create table SN.HASH_T" + Environment.NewLine +
                "(E number(1)" + Environment.NewLine +
                ")" + Environment.NewLine +
                "partition by hash (E)" + Environment.NewLine +
                "(partition PART_1" + Environment.NewLine +
                ",partition PART_2" + Environment.NewLine +
                ");",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void TestHashPartitionWithAutoName()
        {
            var table = new Table("HASH_A", null, null, true);
            FillDefault(table);
            var column = new TableColumn("A", "NUMBER", null, "HASH_A", 1, 1, 1, 0, 'B', null, false);
            table.FindColumns(new List<TableColumn>{column});
            var part = new PartTable("HASH_A", "HASH", "NONE", null, null);
            table.FindPart(new List<PartTable>{part});
            var partColumn = new PartKeyColumn("HASH_A", "TABLE", "A", 1);
            table.FindPartKeyColumns(new List<PartKeyColumn>{partColumn});
            var part1 = new TablePartition("HASH_A", "SYS_P01234", 0, null, 1);
            var part2 = new TablePartition("HASH_A", "SYS_P01235", 0, null, 2);
            table.FindPartitions(new List<TablePartition>{part1, part2});
            generator.Visit(table);
            generatorWithSchema.Visit(table);
            Assert.AreEqual(
                "create table HASH_A" + Environment.NewLine +
                "(A number(1)" + Environment.NewLine +
                ")" + Environment.NewLine +
                "partition by hash (A) partitions 2;",
                generator.Script
            );
            Assert.AreEqual(
                "create table SN.HASH_A" + Environment.NewLine +
                "(A number(1)" + Environment.NewLine +
                ")" + Environment.NewLine +
                "partition by hash (A) partitions 2;",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void TestSystemPartition()
        {
            var table = new Table("APPS", null, null, true);
            FillDefault(table);
            var column1 = new TableColumn("APP_ID", "NUMBER", null, "APPS", 1, 22, null, null, 'C', null, false);
            var column2 = new TableColumn("APP_DATA", "RAW", null, "APPS", 2, 16, null, null, 'B', null, false);
            table.FindColumns(new List<TableColumn>{column1, column2});
            var part = new PartTable("APPS", "SYSTEM", "NONE", null, null);
            table.FindPart(new List<PartTable>{part});
            var part1 = new TablePartition("APPS", "P1", 0, null, 1);
            var part2 = new TablePartition("APPS", "P2", 0, null, 2);
            var part3 = new TablePartition("APPS", "P3", 0, null, 3);
            table.FindPartitions(new List<TablePartition>{part1, part2, part3});
            generator.Visit(table);
            generatorWithSchema.Visit(table);
            Assert.AreEqual(
                "create table APPS" + Environment.NewLine +
                "(APP_ID number" + Environment.NewLine +
                ",APP_DATA raw(16)" + Environment.NewLine +
                ")" + Environment.NewLine +
                "partition by system" + Environment.NewLine +
                "(partition P1" + Environment.NewLine +
                ",partition P2" + Environment.NewLine +
                ",partition P3" + Environment.NewLine +
                ");",
                generator.Script
            );
            Assert.AreEqual(
                "create table SN.APPS" + Environment.NewLine +
                "(APP_ID number" + Environment.NewLine +
                ",APP_DATA raw(16)" + Environment.NewLine +
                ")" + Environment.NewLine +
                "partition by system" + Environment.NewLine +
                "(partition P1" + Environment.NewLine +
                ",partition P2" + Environment.NewLine +
                ",partition P3" + Environment.NewLine +
                ");",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void TestListHashPartitioning()
        {
            var table = new Table("WITH_SUBPART", null, null, true);
            FillDefault(table);
            var col1 = new TableColumn("COL1", "NUMBER", null, "WITH_SUBPART", 1, 22, 15, 0, default(char), null, false);
            var col2 = new TableColumn("COL2", "NUMBER", null, "WITH_SUBPART", 2, 22, 15, 0, default(char), null, false);
            table.FindColumns(new List<TableColumn>{col1, col2});

            var subpartition11 = new TableSubpartition("WITH_SUBPART", "PART_10_5", "PART_10_5__1", null, 1);
            var subpartition12 = new TableSubpartition("WITH_SUBPART", "PART_10_5", "PART_10_5__2", null, 2);
            var subpartition13 = new TableSubpartition("WITH_SUBPART", "PART_10_5", "PART_10_5__3", null, 3);
            var subpartition21 = new TableSubpartition("WITH_SUBPART", "PART_D", "PART_D__1", null, 1);
            var subpartition22 = new TableSubpartition("WITH_SUBPART", "PART_D", "PART_D__2", null, 2);
            var subpartition23 = new TableSubpartition("WITH_SUBPART", "PART_D", "PART_D__3", null, 3);
            var partition1 = new TablePartition("WITH_SUBPART", "PART_10_5", 3, "10, 5", 1);
            var partition2 = new TablePartition("WITH_SUBPART", "PART_D", 3, "default", 2);
            partition1.FindSubpartitions(new List<TableSubpartition>{subpartition11, subpartition12, subpartition13});
            partition2.FindSubpartitions(new List<TableSubpartition>{subpartition21, subpartition22, subpartition23});

            table.FindPartitions(new List<TablePartition>{partition1, partition2});
            var partTable = new PartTable("WITH_SUBPART", "LIST", "HASH", null, null);
            table.FindPart(new List<PartTable>{partTable});
            var partKey = new PartKeyColumn("WITH_SUBPART", "TABLE", "COL1", 1);
            table.FindPartKeyColumns(new List<PartKeyColumn>{partKey});
            var subpartKey = new SubpartitionKeyColumn("WITH_SUBPART", "TABLE", "COL2", 1);
            table.FindSubpartKeyColumns(new List<SubpartitionKeyColumn>{subpartKey});

            generator.Visit(table);
            generatorWithSchema.Visit(table);
            Assert.AreEqual(
                "create table WITH_SUBPART" + Environment.NewLine +
                "(COL1 number(15)" + Environment.NewLine +
                ",COL2 number(15)" + Environment.NewLine +
                ")" + Environment.NewLine +
                "partition by list (COL1)" + Environment.NewLine +
                "subpartition by hash (COL2)" + Environment.NewLine +
                "(partition PART_10_5 values (10, 5)" + Environment.NewLine +
                "  (subpartition PART_10_5__1" + Environment.NewLine +
                "  ,subpartition PART_10_5__2" + Environment.NewLine +
                "  ,subpartition PART_10_5__3" + Environment.NewLine +
                "  )" + Environment.NewLine +
                ",partition PART_D values (default)" + Environment.NewLine +
                "  (subpartition PART_D__1" + Environment.NewLine +
                "  ,subpartition PART_D__2" + Environment.NewLine +
                "  ,subpartition PART_D__3" + Environment.NewLine +
                "  )" + Environment.NewLine +
                ");"
                ,
                generator.Script
            );
            Assert.AreEqual(
                "create table SN.WITH_SUBPART" + Environment.NewLine +
                "(COL1 number(15)" + Environment.NewLine +
                ",COL2 number(15)" + Environment.NewLine +
                ")" + Environment.NewLine +
                "partition by list (COL1)" + Environment.NewLine +
                "subpartition by hash (COL2)" + Environment.NewLine +
                "(partition PART_10_5 values (10, 5)" + Environment.NewLine +
                "  (subpartition PART_10_5__1" + Environment.NewLine +
                "  ,subpartition PART_10_5__2" + Environment.NewLine +
                "  ,subpartition PART_10_5__3" + Environment.NewLine +
                "  )" + Environment.NewLine +
                ",partition PART_D values (default)" + Environment.NewLine +
                "  (subpartition PART_D__1" + Environment.NewLine +
                "  ,subpartition PART_D__2" + Environment.NewLine +
                "  ,subpartition PART_D__3" + Environment.NewLine +
                "  )" + Environment.NewLine +
                ");"
                ,
                generatorWithSchema.Script
            );
        }

        [Test]
        public void TestListHashAutoNamePartitioning()
        {
            var table = new Table("WITH_SUBPART", null, null, true);
            FillDefault(table);
            var col1 = new TableColumn("COL1", "NUMBER", null, "WITH_SUBPART", 1, 22, 15, 0, default(char), null, false);
            var col2 = new TableColumn("COL2", "NUMBER", null, "WITH_SUBPART", 2, 22, 15, 0, default(char), null, false);
            table.FindColumns(new List<TableColumn>{col1, col2});

            var s1 = new TableSubpartition("WITH_SUBPART", "PART_10_5", "SYS_SUBP001", null, 1);
            var s2 = new TableSubpartition("WITH_SUBPART", "PART_10_5", "SYS_SUBP002", null, 2);
            var s3 = new TableSubpartition("WITH_SUBPART", "PART_D", "SYS_SUBP003", null, 1);
            var s4 = new TableSubpartition("WITH_SUBPART", "PART_D", "SYS_SUBP004", null, 2);
            var partition1 = new TablePartition("WITH_SUBPART", "PART_10_5", 2, "10, 5", 1);
            var partition2 = new TablePartition("WITH_SUBPART", "PART_D", 2, "default", 2);
            var subpartitions = new List<TableSubpartition>{s1, s2, s3, s4};
            partition1.FindSubpartitions(subpartitions);
            partition2.FindSubpartitions(subpartitions);

            table.FindPartitions(new List<TablePartition>{partition1, partition2});
            var partTable = new PartTable("WITH_SUBPART", "LIST", "HASH", null, null);
            table.FindPart(new List<PartTable>{partTable});
            var partKey = new PartKeyColumn("WITH_SUBPART", "TABLE", "COL1", 1);
            table.FindPartKeyColumns(new List<PartKeyColumn>{partKey});
            var subpartKey = new SubpartitionKeyColumn("WITH_SUBPART", "TABLE", "COL2", 1);
            table.FindSubpartKeyColumns(new List<SubpartitionKeyColumn>{subpartKey});

            generator.Visit(table);
            generatorWithSchema.Visit(table);
            Assert.AreEqual(
                "create table WITH_SUBPART" + Environment.NewLine +
                "(COL1 number(15)" + Environment.NewLine +
                ",COL2 number(15)" + Environment.NewLine +
                ")" + Environment.NewLine +
                "partition by list (COL1)" + Environment.NewLine +
                "subpartition by hash (COL2) subpartitions 2" + Environment.NewLine +
                "(partition PART_10_5 values (10, 5)" + Environment.NewLine +
                ",partition PART_D values (default)" + Environment.NewLine +
                ");"
                ,
                generator.Script
            );
            Assert.AreEqual(
                "create table SN.WITH_SUBPART" + Environment.NewLine +
                "(COL1 number(15)" + Environment.NewLine +
                ",COL2 number(15)" + Environment.NewLine +
                ")" + Environment.NewLine +
                "partition by list (COL1)" + Environment.NewLine +
                "subpartition by hash (COL2) subpartitions 2" + Environment.NewLine +
                "(partition PART_10_5 values (10, 5)" + Environment.NewLine +
                ",partition PART_D values (default)" + Environment.NewLine +
                ");"
                ,
                generatorWithSchema.Script
            );
        }

        [Test]
        public void TestRangeListPartitioning()
        {
            var table = new Table("RANGE_LIST", null, null, true);
            FillDefault(table);
            var col1 = new TableColumn("SALES_AMNT", "NUMBER", null, "RANGE_LIST", 1, 22, null, null, default(char), "null", false);
            var col2 = new TableColumn("REG_CODE", "VARCHAR2", null, "RANGE_LIST", 2, 3, null, null, 'B', null, false);
            var col3 = new TableColumn("D_DATE_ID", "NUMBER", null, "RANGE_LIST", 3, 22, null, null, default(char), null, false);
            table.FindColumns(new List<TableColumn>{col1, col2, col3});

            var subpartition11 = new TableSubpartition("RANGE_LIST", "P2010", "P1_NORTH", "'ID', 'OR'", 1);
            var subpartition12 = new TableSubpartition("RANGE_LIST", "P2010", "P1_SOUTH", "'AZ', 'NM'", 2);
            var subpartition21 = new TableSubpartition("RANGE_LIST", "P2011", "P2_NORTH", "'ID', 'OR'", 1);
            var subpartition22 = new TableSubpartition("RANGE_LIST", "P2011", "P2_SOUTH", "'AZ', 'NM'", 2);
            var partition1 = new TablePartition("RANGE_LIST", "P2010", 2, "20100101", 1);
            var partition2 = new TablePartition("RANGE_LIST", "P2011", 2, "20110101", 2);
            partition1.FindSubpartitions(new List<TableSubpartition>{subpartition11, subpartition12});
            partition2.FindSubpartitions(new List<TableSubpartition>{subpartition21, subpartition22});

            table.FindPartitions(new List<TablePartition>{partition1, partition2});
            var partTable = new PartTable("RANGE_LIST", "RANGE", "LIST", null, null);
            table.FindPart(new List<PartTable>{partTable});
            var partKey = new PartKeyColumn("RANGE_LIST", "TABLE", "D_DATE_ID", 1);
            table.FindPartKeyColumns(new List<PartKeyColumn>{partKey});
            var subpartKey = new SubpartitionKeyColumn("RANGE_LIST", "TABLE", "REG_CODE", 1);
            table.FindSubpartKeyColumns(new List<SubpartitionKeyColumn>{subpartKey});

            generator.Visit(table);
            generatorWithSchema.Visit(table);
            Assert.AreEqual(
                "create table RANGE_LIST" + Environment.NewLine +
                "(SALES_AMNT number" + Environment.NewLine +
                ",REG_CODE varchar2(3 byte)" + Environment.NewLine +
                ",D_DATE_ID number" + Environment.NewLine +
                ")" + Environment.NewLine +
                "partition by range (D_DATE_ID)" + Environment.NewLine +
                "subpartition by list (REG_CODE)" + Environment.NewLine +
                "(partition P2010 values less than (20100101)" + Environment.NewLine +
                "  (subpartition P1_NORTH values ('ID', 'OR')" + Environment.NewLine +
                "  ,subpartition P1_SOUTH values ('AZ', 'NM')" + Environment.NewLine +
                "  )" + Environment.NewLine +
                ",partition P2011 values less than (20110101)" + Environment.NewLine +
                "  (subpartition P2_NORTH values ('ID', 'OR')" + Environment.NewLine +
                "  ,subpartition P2_SOUTH values ('AZ', 'NM')" + Environment.NewLine +
                "  )" + Environment.NewLine +
                ");"
                ,
                generator.Script
            );
            Assert.AreEqual(
                "create table SN.RANGE_LIST" + Environment.NewLine +
                "(SALES_AMNT number" + Environment.NewLine +
                ",REG_CODE varchar2(3 byte)" + Environment.NewLine +
                ",D_DATE_ID number" + Environment.NewLine +
                ")" + Environment.NewLine +
                "partition by range (D_DATE_ID)" + Environment.NewLine +
                "subpartition by list (REG_CODE)" + Environment.NewLine +
                "(partition P2010 values less than (20100101)" + Environment.NewLine +
                "  (subpartition P1_NORTH values ('ID', 'OR')" + Environment.NewLine +
                "  ,subpartition P1_SOUTH values ('AZ', 'NM')" + Environment.NewLine +
                "  )" + Environment.NewLine +
                ",partition P2011 values less than (20110101)" + Environment.NewLine +
                "  (subpartition P2_NORTH values ('ID', 'OR')" + Environment.NewLine +
                "  ,subpartition P2_SOUTH values ('AZ', 'NM')" + Environment.NewLine +
                "  )" + Environment.NewLine +
                ");"
                ,
                generatorWithSchema.Script
            );
        }

        [Test]
        public void TestSubpartitionTemplate()
        {
            var table = new Table("COMPOSITE_RNG_HASH", null, null, true);
            FillDefault(table);
            var col1 = new TableColumn("CUST_ID", "NUMBER", null, "COMPOSITE_RNG_HASH", 1, 22, 10, 0, 'C', null, false);
            var col2 = new TableColumn("TIME_ID", "DATE", null, "COMPOSITE_RNG_HASH", 2, 7, null, null, 'C', null, false);
            table.FindColumns(new List<TableColumn>{col1, col2});

            var s1 = new TableSubpartition("COMPOSITE_RNG_HASH", "SALES_PRE05", "SALES_PRE05_SP1", null, 1);
            var s2 = new TableSubpartition("COMPOSITE_RNG_HASH", "SALES_PRE05", "SALES_PRE05_SP2", null, 2);
            var s3 = new TableSubpartition("COMPOSITE_RNG_HASH", "SALES_2005", "SALES_2005_SP1", null, 1);
            var s4 = new TableSubpartition("COMPOSITE_RNG_HASH", "SALES_2005", "SALES_2005_SP2", null, 2);
            var s5 = new TableSubpartition("COMPOSITE_RNG_HASH", "SALES_FUTURE", "SALES_FUTURE_SP1", null, 1);
            var s6 = new TableSubpartition("COMPOSITE_RNG_HASH", "SALES_FUTURE", "SALES_FUTURE_SP2", null, 2);
            var subparts = new List<TableSubpartition>{s1, s2, s3, s4, s5, s6};

            var p1 = new TablePartition("COMPOSITE_RNG_HASH", "SALES_PRE05", 2, "TO_DATE(' 2005-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN')", 1);
            var p2 = new TablePartition("COMPOSITE_RNG_HASH", "SALES_2005", 2, "TO_DATE(' 2006-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN')", 2);
            var p3 = new TablePartition("COMPOSITE_RNG_HASH", "SALES_FUTURE", 2, "MAXVALUE", 3);
            p1.FindSubpartitions(subparts);
            p2.FindSubpartitions(subparts);
            p3.FindSubpartitions(subparts);
            table.FindPartitions(new List<TablePartition>{p1, p2, p3});

            var st1 = new SubpartitionTemplate("COMPOSITE_RNG_HASH", "SP1", 1, null);
            var st2 = new SubpartitionTemplate("COMPOSITE_RNG_HASH", "SP2", 2, null);
            table.FindSubpartitionTemplates(new List<SubpartitionTemplate>{st1, st2});

            var partTable = new PartTable("COMPOSITE_RNG_HASH", "RANGE", "HASH", null, null);
            table.FindPart(new List<PartTable>{partTable});

            var partKey = new PartKeyColumn("COMPOSITE_RNG_HASH", "TABLE", "TIME_ID", 1);
            table.FindPartKeyColumns(new List<PartKeyColumn>{partKey});
            var subpartKey = new SubpartitionKeyColumn("COMPOSITE_RNG_HASH", "TABLE", "CUST_ID", 1);
            table.FindSubpartKeyColumns(new List<SubpartitionKeyColumn>{subpartKey});

            generator.Visit(table);
            generatorWithSchema.Visit(table);
            Assert.AreEqual(
                "create table COMPOSITE_RNG_HASH" + Environment.NewLine +
                "(CUST_ID number(10)" + Environment.NewLine +
                ",TIME_ID date" + Environment.NewLine +
                ")" + Environment.NewLine +
                "partition by range (TIME_ID)" + Environment.NewLine +
                "subpartition by hash (CUST_ID)" + Environment.NewLine +
                "subpartition template" + Environment.NewLine +
                "(subpartition SP1" + Environment.NewLine +
                ",subpartition SP2" + Environment.NewLine +
                ")" + Environment.NewLine +
                "(partition SALES_PRE05 values less than (TO_DATE(' 2005-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))" + Environment.NewLine +
                ",partition SALES_2005 values less than (TO_DATE(' 2006-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))" + Environment.NewLine +
                ",partition SALES_FUTURE values less than (maxvalue)" + Environment.NewLine +
                ");"
                ,
                generator.Script
            );
            Assert.AreEqual(
                "create table SN.COMPOSITE_RNG_HASH" + Environment.NewLine +
                "(CUST_ID number(10)" + Environment.NewLine +
                ",TIME_ID date" + Environment.NewLine +
                ")" + Environment.NewLine +
                "partition by range (TIME_ID)" + Environment.NewLine +
                "subpartition by hash (CUST_ID)" + Environment.NewLine +
                "subpartition template" + Environment.NewLine +
                "(subpartition SP1" + Environment.NewLine +
                ",subpartition SP2" + Environment.NewLine +
                ")" + Environment.NewLine +
                "(partition SALES_PRE05 values less than (TO_DATE(' 2005-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))" + Environment.NewLine +
                ",partition SALES_2005 values less than (TO_DATE(' 2006-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))" + Environment.NewLine +
                ",partition SALES_FUTURE values less than (maxvalue)" + Environment.NewLine +
                ");"
                ,
                generatorWithSchema.Script
            );
        }

        [Test]
        public void TestReferencePartitioning()
        {
            var main = new Table("HASH_PRODUCTS", null, null, true);
            FillDefault(main);
            var col = new TableColumn("PRODUCT_ID", "NUMBER", null, "HASH_PRODUCTS", 1, 22, 10, 0, 'C', null, false);
            main.FindColumns(new List<TableColumn>{col});
            var index = new TableIndex("SYS_C001227305", "HASH_PRODUCTS", "NORMAL", true);
            main.FindIndexes(new List<TableIndex>{index}, new List<Lob>(0));
            var indCol = new IndexColumn("SYS_C001227305", "PRODUCT_ID", 1);
            index.FindColumns(new List<IndexColumn>{indCol}, new List<TableColumn>{col}, new List<IndexExpression>(0));
            TableConstraint pk = CreatePrimaryKey("SYS_C001227305", "HASH_PRODUCTS", "SYS_C001227305");
            main.FindConstraints(new List<TableConstraint>{pk});
            var pkCol = new ConstraintColumn("PRODUCT_ID", "HASH_PRODUCTS", "SYS_C001227305", 1);
            pk.FindColumns(new List<ConstraintColumn>{pkCol}, new List<TableColumn>{col});
            pk.FindIndex(new List<TableIndex>{index});

            var mainPart = new PartTable("HASH_PRODUCTS", "HASH", null, null, null);
            main.FindPart(new List<PartTable>{mainPart});
            var mp1 = new TablePartition("HASH_PRODUCTS", "SYS_P4182", 0, null, 1);
            var mp2 = new TablePartition("HASH_PRODUCTS", "SYS_P4183", 0, null, 2);
            var mp3 = new TablePartition("HASH_PRODUCTS", "SYS_P4184", 0, null, 3);
            main.FindPartitions(new List<TablePartition>{mp1, mp2, mp3});
            var partKey = new PartKeyColumn("HASH_PRODUCTS", "TABLE", "PRODUCT_ID", 1);
            main.FindPartKeyColumns(new List<PartKeyColumn>{partKey});

            var table = new Table("PART_ORDER_ITEMS", null, null, true);
            FillDefault(table);
            var col1 = new TableColumn("PRODUCT_ID", "NUMBER", null, "PART_ORDER_ITEMS", 1, 22, 10, 0, 'B', null, false);
            var col2 = new TableColumn("REG_DATE", "TIMESTAMP(6) WITH LOCAL TIME ZONE", null, "PART_ORDER_ITEMS", 2, 11, null, 6, 'C', null, false);
            table.FindColumns(new List<TableColumn>{col1, col2});
            var partTable = new PartTable("PART_ORDER_ITEMS", "REFERENCE", null, "PRODUCT_ID_FK", null);
            table.FindPart(new List<PartTable>{partTable});
            var p1 = new TablePartition("PART_ORDER_ITEMS", "SYS_P4185", 0, null, 1);
            var p2 = new TablePartition("PART_ORDER_ITEMS", "SYS_P4186", 0, null, 2);
            var p3 = new TablePartition("PART_ORDER_ITEMS", "SYS_P4187", 0, null, 3);
            table.FindPartitions(new List<TablePartition>{p1, p2, p3});
            ReferentialIntegrityConstraint fk = CreateForeignKey("PRODUCT_ID_FK", "PART_ORDER_ITEMS", "SYS_C001227305");
            var fkCol = new ConstraintColumn("PRODUCT_ID", "PART_ORDER_ITEMS", "PRODUCT_ID_FK", 1);
            fk.FindColumns(new List<ConstraintColumn>{fkCol}, new List<TableColumn>{col1, col2});
            fk.FindRefConstraint(new List<TableConstraint>{pk});
            table.FindConstraints(new List<TableConstraint>{fk});

            var schema = new OracleSchema();
            schema.AddDependency(main);
            schema.AddDependency(table);
            generator.Visit(schema);
            generatorWithSchema.Visit(schema);
            Assert.AreEqual(
                "set define off" + Environment.NewLine +
                "set sqlblanklines on" + Environment.NewLine + Environment.NewLine +
                "create table HASH_PRODUCTS" + Environment.NewLine +
                "(PRODUCT_ID number(10) primary key" + Environment.NewLine +
                ")" + Environment.NewLine +
                "partition by hash (PRODUCT_ID) partitions 3;" + Environment.NewLine + Environment.NewLine +
                "create table PART_ORDER_ITEMS" + Environment.NewLine +
                "(PRODUCT_ID number(10)" + Environment.NewLine +
                ",REG_DATE timestamp with local time zone" + Environment.NewLine +
                ",constraint PRODUCT_ID_FK foreign key (PRODUCT_ID) references HASH_PRODUCTS(PRODUCT_ID)" + Environment.NewLine +
                ")" + Environment.NewLine +
                "partition by reference (PRODUCT_ID_FK);"
                ,
                generator.Script
            );
        }

        [Test]
        public void TestIndex()
        {
            var table = new Table("SOME_I", null, null, false);
            FillDefault(table);
            var column = new TableColumn("L", "NUMBER", null, "SOME_I", 1, 1, 1, 0, 'C', null, false);
            var columns = new List<TableColumn>{column};
            table.FindColumns(columns);
            var index = new TableIndex("L_INDEX", "SOME_I", "NORMAL", false);
            table.FindIndexes(new List<TableIndex>{index}, new List<Lob>(0));
            var indexCol = new IndexColumn("L_INDEX", "L", 1);
            index.FindColumns(new List<IndexColumn>{indexCol}, columns, new List<IndexExpression>(0));
            generator.Visit(table);
            generatorWithSchema.Visit(table);
            Assert.AreEqual(
                "create table SOME_I" + Environment.NewLine +
                "(L number(1)" + Environment.NewLine +
                ");" + Environment.NewLine +
                "create index L_INDEX on SOME_I(L);",
                generator.Script
            );
            Assert.AreEqual(
                "create table SN.SOME_I" + Environment.NewLine +
                "(L number(1)" + Environment.NewLine +
                ");" + Environment.NewLine +
                "create index SN.L_INDEX on SN.SOME_I(L);",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void TestBitmapIndex()
        {
            var table = new Table("SOME_I", null, null, false);
            FillDefault(table);
            var column = new TableColumn("L", "NUMBER", null, "SOME_I", 1, 1, 1, 0, 'C', null, false);
            var columns = new List<TableColumn>{column};
            table.FindColumns(columns);
            var index = new TableIndex("L_INDEX", "SOME_I", "BITMAP", false);
            table.FindIndexes(new List<TableIndex>{index}, new List<Lob>(0));
            var indexCol = new IndexColumn("L_INDEX", "L", 1);
            index.FindColumns(new List<IndexColumn>{indexCol}, columns, new List<IndexExpression>(0));
            generator.Visit(table);
            generatorWithSchema.Visit(table);
            Assert.AreEqual(
                "create table SOME_I" + Environment.NewLine +
                "(L number(1)" + Environment.NewLine +
                ");" + Environment.NewLine +
                "create bitmap index L_INDEX on SOME_I(L);",
                generator.Script
            );
            Assert.AreEqual(
                "create table SN.SOME_I" + Environment.NewLine +
                "(L number(1)" + Environment.NewLine +
                ");" + Environment.NewLine +
                "create bitmap index SN.L_INDEX on SN.SOME_I(L);",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void TestUniqueIndex()
        {
            var table = new Table("SOME_I", null, null, false);
            FillDefault(table);
            var column = new TableColumn("L", "NUMBER", null, "SOME_I", 1, 1, 1, 0, 'C', null, false);
            var columns = new List<TableColumn>{column};
            table.FindColumns(columns);
            var index = new TableIndex("L_INDEX", "SOME_I", "NORMAL", true);
            table.FindIndexes(new List<TableIndex>{index}, new List<Lob>(0));
            var indexCol = new IndexColumn("L_INDEX", "L", 1);
            index.FindColumns(new List<IndexColumn>{indexCol}, columns, new List<IndexExpression>(0));
            generator.Visit(table);
            generatorWithSchema.Visit(table);
            Assert.AreEqual(
                "create table SOME_I" + Environment.NewLine +
                "(L number(1)" + Environment.NewLine +
                ");" + Environment.NewLine +
                "create unique index L_INDEX on SOME_I(L);",
                generator.Script
            );
            Assert.AreEqual(
                "create table SN.SOME_I" + Environment.NewLine +
                "(L number(1)" + Environment.NewLine +
                ");" + Environment.NewLine +
                "create unique index SN.L_INDEX on SN.SOME_I(L);",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void TestConstraintMismatchNameWithIndexIndex()
        {
            var table = new Table("CLMN", null, null, false);
            FillDefault(table);
            var column = new TableColumn("L", "NUMBER", null, "CLMN", 1, 22, 22, 0, 'C', null, false);
            var columns = new List<TableColumn>{column};
            table.FindColumns(columns);
            var index = new TableIndex("U$CLMN_L", "CLMN", "NORMAL", true);
            table.FindIndexes(new List<TableIndex>{index}, new List<Lob>(0));
            var indexCol = new IndexColumn("U$CLMN_L", "L", 1);
            index.FindColumns(new List<IndexColumn>{indexCol}, columns, new List<IndexExpression>(0));
            TableConstraint constraint = CreateUniqueConstraint("SYS_C000001", "CLMN", "U$CLMN_L");
            table.FindConstraints(new List<TableConstraint>{constraint});
            var constraintColumn = new ConstraintColumn("L", "CLMN", "SYS_C000001", 1);
            constraint.FindColumns(new List<ConstraintColumn>{constraintColumn}, columns);
            constraint.FindIndex(new List<TableIndex>{index});

            generator.Visit(table);
            generatorWithSchema.Visit(table);
            Assert.AreEqual(
                "create table CLMN" + Environment.NewLine +
                "(L number(22)" + Environment.NewLine +
                ");" + Environment.NewLine +
                "create unique index U$CLMN_L on CLMN(L);"  + Environment.NewLine +
                "alter table CLMN add unique (L);",
                generator.Script
            );
            Assert.AreEqual(
                "create table SN.CLMN" + Environment.NewLine +
                "(L number(22)" + Environment.NewLine +
                ");" + Environment.NewLine +
                "create unique index SN.U$CLMN_L on SN.CLMN(L);"  + Environment.NewLine +
                "alter table SN.CLMN add unique (L);",
                generatorWithSchema.Script
            );
        }
    }
}
