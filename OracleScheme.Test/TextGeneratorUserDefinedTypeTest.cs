﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using OracleScheme.DBObjects;

namespace OracleScheme.Test
{
    [TestFixture]
    public class TextGeneratorUserDefinedTypeTest
    {
        private static readonly string SCHEMA_NAME = "SN";
        TextGenerator generator;
        TextGenerator generatorWithSchema;

        [SetUp]
        public void Setup()
        {
            generator = new TextGenerator(null);
            generatorWithSchema = new TextGenerator(SCHEMA_NAME);
        }

        [Test]
        public void EmptyObject()
        {
            var type = new UserDefinedType("EMPTY_TYPE", "OBJECT", 0, 0);
            generator.Visit(type);
            generatorWithSchema.Visit(type);
            Assert.AreEqual("create or replace type EMPTY_TYPE as object;" + Environment.NewLine + "/", generator.Script);
            Assert.AreEqual("create or replace type SN.EMPTY_TYPE as object;" + Environment.NewLine + "/", generatorWithSchema.Script);
        }

        [Test]
        public void AttirbutesOnly()
        {
            var type = new UserDefinedType("MY_TYPE", "OBJECT", 5, 0);
            var fld1 = new ObjectTypeAttribute("MY_TYPE", "FLD1", "VARCHAR2", 60, null, null, 1, 'C');
            var fld2 = new ObjectTypeAttribute("MY_TYPE", "FLD2", "INTERVAL DAY(2) TO SECOND(6)", null, 2, 6, 1, default(char));
            var fld3 = new ObjectTypeAttribute("MY_TYPE", "FLD3", "INTERVAL DAY(2) TO SECOND(9)", null, 2, 9, 2, default(char));
            var fld4 = new ObjectTypeAttribute("MY_TYPE", "FLD4", "INTERVAL DAY(0) TO SECOND(6)", null, 0, 6, 4, default(char));
            var fld5 = new ObjectTypeAttribute("MY_TYPE", "FLD5", "INTERVAL DAY(9) TO SECOND(0)", null, 9, 0, 5, default(char));
            type.FindAttributes(new List<ObjectTypeAttribute>{fld1, fld2, fld3, fld4, fld5}, new List<UserDefinedType>(0));
            generator.Visit(type);
            generatorWithSchema.Visit(type);
            Assert.AreEqual(
                "create or replace type MY_TYPE as object" + Environment.NewLine +
                "(FLD1 varchar2(60 char)" + Environment.NewLine +
                ",FLD2 interval day to second" + Environment.NewLine +
                ",FLD3 interval day to second(9)" + Environment.NewLine +
                ",FLD4 interval day(0) to second" + Environment.NewLine +
                ",FLD5 interval day(9) to second(0)" + Environment.NewLine +
                ");" + Environment.NewLine +
                "/",
                generator.Script
            );
            Assert.AreEqual(
                "create or replace type SN.MY_TYPE as object" + Environment.NewLine +
                "(FLD1 varchar2(60 char)" + Environment.NewLine +
                ",FLD2 interval day to second" + Environment.NewLine +
                ",FLD3 interval day to second(9)" + Environment.NewLine +
                ",FLD4 interval day(0) to second" + Environment.NewLine +
                ",FLD5 interval day(9) to second(0)" + Environment.NewLine +
                ");" + Environment.NewLine +
                "/",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void WithBody()
        {
            var type = new UserDefinedType("MY_TYPE", "OBJECT", 0, 1);
            var method = new ObjectTypeMethod("MY_TYPE", "MY_MEMBER", 1, "PUBLIC", false, 0);
            method.FindParams(new List<ObjectTypeMethodParam>(0));
            var sources = new Dictionary<SourceObjectKey, string>
            {
                {new SourceObjectKey("TYPE BODY", "MY_TYPE"), "TYPE BODY MY_TYPE as static MY_MEMBER procesure is begin null; end; end;"}
            };
            UserDefinedTypeBody body = type.FindBodyCode(sources);
            type.FindMethods(new List<ObjectTypeMethod>{method});

            var schema = new OracleSchema();
            schema.AddDependency(type);
            schema.AddDependency(body);

            generator.Visit(schema);
            generatorWithSchema.Visit(schema);
            Assert.AreEqual(
                "set define off" + Environment.NewLine +
                "set sqlblanklines on" + Environment.NewLine + Environment.NewLine +
                "create or replace type MY_TYPE as object" + Environment.NewLine +
                "(static procedure MY_MEMBER" + Environment.NewLine +
                ");" + Environment.NewLine + 
                "/" + Environment.NewLine + Environment.NewLine +
                "create or replace type body MY_TYPE as" + Environment.NewLine +
                "static MY_MEMBER procesure is begin null; end; end;" + Environment.NewLine +
                "/",
                generator.Script
            );
            Assert.AreEqual(
                "set define off" + Environment.NewLine +
                "set sqlblanklines on" + Environment.NewLine + Environment.NewLine +
                "create or replace type SN.MY_TYPE as object" + Environment.NewLine +
                "(static procedure MY_MEMBER" + Environment.NewLine +
                ");" + Environment.NewLine + 
                "/" + Environment.NewLine + Environment.NewLine +
                "create or replace type body SN.MY_TYPE as" + Environment.NewLine +
                "static MY_MEMBER procesure is begin null; end; end;" + Environment.NewLine +
                "/",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void BodyWithComments()
        {
            var type = new UserDefinedType("MY_TYPE", "OBJECT", 0, 1);
            var method = new ObjectTypeMethod("MY_TYPE", "MY_MEMBER", 1, "PUBLIC", false, 0);
            method.FindParams(new List<ObjectTypeMethodParam>(0));
            var sources = new Dictionary<SourceObjectKey, string>
            {
                {new SourceObjectKey("TYPE BODY", "MY_TYPE"), "type body/*comment*/MY_TYPE as static MY_MEMBER procesure is begin null; end; end;"}
            };
            UserDefinedTypeBody body = type.FindBodyCode(sources);
            type.FindMethods(new List<ObjectTypeMethod>{method});

            var schema = new OracleSchema();
            schema.AddDependency(type);
            schema.AddDependency(body);

            generator.Visit(schema);
            generatorWithSchema.Visit(schema);
            Assert.AreEqual(
                "set define off" + Environment.NewLine +
                "set sqlblanklines on" + Environment.NewLine + Environment.NewLine +
                "create or replace type MY_TYPE as object" + Environment.NewLine +
                "(static procedure MY_MEMBER" + Environment.NewLine +
                ");" + Environment.NewLine + 
                "/" + Environment.NewLine + Environment.NewLine +
                "create or replace type body/*comment*/MY_TYPE as static MY_MEMBER procesure is begin null; end; end;" + Environment.NewLine +
                "/",
                generator.Script
            );
            Assert.AreEqual(
                "set define off" + Environment.NewLine +
                "set sqlblanklines on" + Environment.NewLine + Environment.NewLine +
                "create or replace type SN.MY_TYPE as object" + Environment.NewLine +
                "(static procedure MY_MEMBER" + Environment.NewLine +
                ");" + Environment.NewLine + 
                "/" + Environment.NewLine + Environment.NewLine +
                "create or replace type body/*comment*/MY_TYPE as static MY_MEMBER procesure is begin null; end; end;" + Environment.NewLine +
                "/",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void BodyWithQuotas()
        {
            var type = new UserDefinedType("MY_TYPE", "OBJECT", 0, 1);
            var method = new ObjectTypeMethod("MY_TYPE", "MY_MEMBER", 1, "PUBLIC", false, 0);
            method.FindParams(new List<ObjectTypeMethodParam>(0));
            var sources = new Dictionary<SourceObjectKey, string>
            {
                {new SourceObjectKey("TYPE BODY", "MY_TYPE"), "TYPE BODY\"MY_TYPE\"as static MY_MEMBER procesure is begin null; end; end;"}
            };
            UserDefinedTypeBody body = type.FindBodyCode(sources);
            type.FindMethods(new List<ObjectTypeMethod>{method});

            var schema = new OracleSchema();
            schema.AddDependency(type);
            schema.AddDependency(body);

            generator.Visit(schema);
            generatorWithSchema.Visit(schema);
            Assert.AreEqual(
                "set define off" + Environment.NewLine +
                "set sqlblanklines on" + Environment.NewLine + Environment.NewLine +
                "create or replace type MY_TYPE as object" + Environment.NewLine +
                "(static procedure MY_MEMBER" + Environment.NewLine +
                ");" + Environment.NewLine + 
                "/" + Environment.NewLine + Environment.NewLine +
                "create or replace type body MY_TYPE as" + Environment.NewLine +
                "static MY_MEMBER procesure is begin null; end; end;" + Environment.NewLine +
                "/",
                generator.Script
            );
            Assert.AreEqual(
                "set define off" + Environment.NewLine +
                "set sqlblanklines on" + Environment.NewLine + Environment.NewLine +
                "create or replace type SN.MY_TYPE as object" + Environment.NewLine +
                "(static procedure MY_MEMBER" + Environment.NewLine +
                ");" + Environment.NewLine + 
                "/" + Environment.NewLine + Environment.NewLine +
                "create or replace type body SN.MY_TYPE as" + Environment.NewLine +
                "static MY_MEMBER procesure is begin null; end; end;" + Environment.NewLine +
                "/",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void WithBodyAndSequence()
        {
            var type = new UserDefinedType("MY_TYPE", "OBJECT", 0, 1);
            var method = new ObjectTypeMethod("MY_TYPE", "MY_MEMBER", 1, "PUBLIC", false, 1);
            var parameter = new ObjectTypeMethodParam("MY_TYPE", "MY_MEMBER", 1, "SELF", 1, "IN OUT", "MY_TYPE");
            method.FindParams(new List<ObjectTypeMethodParam>{parameter});
            var sources = new Dictionary<SourceObjectKey, string>
            {
                {new SourceObjectKey("TYPE BODY", "MY_TYPE"), "TYPE BODY MY_TYPE as member MY_MEMBER procesure is begin null; end; end;"}
            };
            UserDefinedTypeBody body = type.FindBodyCode(sources);
            type.FindMethods(new List<ObjectTypeMethod>{method});

            var seq = new Sequence("MY_SEQ", 1, 3, 1, false, true, 20, 2);

            var schema = new OracleSchema();
            schema.AddDependency(type);
            schema.AddDependency(body);
            schema.AddDependency(seq);

            generator.Visit(schema);
            generatorWithSchema.Visit(schema);
            Assert.AreEqual(
                "set define off" + Environment.NewLine +
                "set sqlblanklines on" + Environment.NewLine + Environment.NewLine +
                "create or replace type MY_TYPE as object" + Environment.NewLine +
                "(member procedure MY_MEMBER" + Environment.NewLine +
                ");" + Environment.NewLine + 
                "/" + Environment.NewLine + Environment.NewLine +
                "create or replace type body MY_TYPE as" + Environment.NewLine +
                "member MY_MEMBER procesure is begin null; end; end;" + Environment.NewLine +
                "/" + Environment.NewLine + Environment.NewLine +
                "create sequence MY_SEQ start with 2 maxvalue 3 order;",
                generator.Script
            );
            Assert.AreEqual(
                "set define off" + Environment.NewLine +
                "set sqlblanklines on" + Environment.NewLine + Environment.NewLine +
                "create or replace type SN.MY_TYPE as object" + Environment.NewLine +
                "(member procedure MY_MEMBER" + Environment.NewLine +
                ");" + Environment.NewLine + 
                "/" + Environment.NewLine + Environment.NewLine +
                "create or replace type body SN.MY_TYPE as" + Environment.NewLine +
                "member MY_MEMBER procesure is begin null; end; end;" + Environment.NewLine +
                "/" + Environment.NewLine + Environment.NewLine +
                "create sequence SN.MY_SEQ start with 2 maxvalue 3 order;",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void TableOf()
        {
            var type = new UserDefinedType("NUMBER20ARRAY", "COLLECTION", 0, 0);
            var elementType = new CollectionElementType("NUMBER20ARRAY", "TABLE", null, "NUMBER", null, 20, 0, 'C');
            type.FindElementType(new List<CollectionElementType>{elementType}, new List<UserDefinedType>(0));
            generator.Visit(type);
            generatorWithSchema.Visit(type);
            Assert.AreEqual(
                "create or replace type NUMBER20ARRAY as table of number(20);" + Environment.NewLine +
                "/",
                generator.Script
            );
            Assert.AreEqual(
                "create or replace type SN.NUMBER20ARRAY as table of number(20);" + Environment.NewLine +
                "/",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void Varray()
        {
            var type = new UserDefinedType("VAR_INT", "COLLECTION", 0, 0);
            var elementType = new CollectionElementType("VAR_INT", "VARYING ARRAY", 10, "INTEGER", null, null, null, 'B');
            type.FindElementType(new List<CollectionElementType>{elementType}, new List<UserDefinedType>(0));
            generator.Visit(type);
            generatorWithSchema.Visit(type);
            Assert.AreEqual(
                "create or replace type VAR_INT as varray(10) of integer;" + Environment.NewLine +
                "/",
                generator.Script
            );
            Assert.AreEqual(
                "create or replace type SN.VAR_INT as varray(10) of integer;" + Environment.NewLine +
                "/",
                generatorWithSchema.Script
            );
        }

        [Test]
        public void WithConstructor()
        {
            var type = new UserDefinedType("WITH_CONSTRUCTOR", "OBJECT", 0, 1);
            var method = new ObjectTypeMethod("WITH_CONSTRUCTOR", "WITH_CONSTRUCTOR", 1, "PUBLIC", true, 1);
            type.FindMethods(new List<ObjectTypeMethod>{method});
            var param = new ObjectTypeMethodParam("WITH_CONSTRUCTOR", "WITH_CONSTRUCTOR", 1, "SELF", 1, "IN OUT", "WITH_CONSTRUCTOR");
            method.FindParams(new List<ObjectTypeMethodParam>{param});
            generator.Visit(type);
            generatorWithSchema.Visit(type);
            Assert.AreEqual(
                "create or replace type WITH_CONSTRUCTOR as object" + Environment.NewLine +
                "(constructor function WITH_CONSTRUCTOR return SELF as result" + Environment.NewLine +
                ");" + Environment.NewLine +
                "/",
                generator.Script
            );
        }
    }
}
