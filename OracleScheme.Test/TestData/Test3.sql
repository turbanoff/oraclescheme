﻿set define off
set sqlblanklines on

create table DETAIL
(DETAIL_ID number primary key
,MASTER_ID number
);

create table MASTER
(MASTER_ID number primary key
,DETAIL_ID number
);