﻿set define off
set sqlblanklines on

create table TQ84_INTERVAL_PARTITION
(ID number
,TXT varchar2(10 byte)
,DT date
)
partition by range (DT) interval (NUMTOYMINTERVAL(1, 'MONTH'))
(partition TQ84_PARTITION_1 values less than (TO_DATE(' 2010-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
);