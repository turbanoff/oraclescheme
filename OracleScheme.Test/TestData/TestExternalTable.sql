﻿set define off
set sqlblanklines on

create table EXTERNAL_TABLE
(LINE varchar2(4000 byte)
)
organization external (
  default directory DATA_PUMP_DIR
  access parameters (
    records delimited by newline
    badfile diag_trace:'external_table%a_%p.bad'
    logfile diag_trace:'external_table%a_%p.log'
    fields terminated by '~*~'
    missing field values are null
    ( line )
  )
  location (DATA_PUMP_DIR:'LNRE1U1_ora_076.trc')
);