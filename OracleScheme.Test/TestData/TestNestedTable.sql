﻿set define off
set sqlblanklines on

create or replace type XX_TYPE_CHAIR as table of varchar2(100 char);
/

create table CHAIRS
(ID number
,CHAIR_NAME XX_TYPE_CHAIR
)
nested table CHAIR_NAME store as NESTED_CHAIRS;