﻿set define off
set sqlblanklines on

create table T1
(A integer primary key
);
create materialized view log on T1;

create table T10
(A integer
);
create materialized view log on T10 with rowid, sequence;

create table T11
(A integer primary key
);
create materialized view log on T11 with sequence;

create table T12
(A integer primary key
);
create materialized view log on T12 with rowid, primary key, sequence;

create table T13
(A integer primary key
);
create materialized view log on T13 with sequence including new values;

create table T14
(A integer
);
create materialized view log on T14 with rowid, sequence including new values;

create table T15
(A integer primary key
);
create materialized view log on T15 with rowid, primary key including new values;

create table T16
(A integer primary key
);
create materialized view log on T16 with rowid, primary key, sequence including new values;

create table T2
(A integer primary key
);
create materialized view log on T2 including new values;

create table T3
(A integer
);
create materialized view log on T3 with rowid;

create table T4
(A integer primary key
);
create materialized view log on T4;

create table T5
(A integer primary key
);
create materialized view log on T5 with sequence;

create table T6
(A integer
);
create materialized view log on T6 with rowid including new values;

create table T7
(A integer primary key
);
create materialized view log on T7 including new values;

create table T8
(A integer primary key
);
create materialized view log on T8 with sequence including new values;

create table T9
(A integer primary key
);
create materialized view log on T9 with rowid, primary key;