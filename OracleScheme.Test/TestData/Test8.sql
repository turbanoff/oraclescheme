﻿set define off
set sqlblanklines on

create table LIST_SUBTEMPLATE
(B number
,C char(1 byte)
)
partition by list (B)
subpartition by hash (C)
subpartition template
(subpartition S1
,subpartition S2
,subpartition S3
)
(partition P1 values (1, 2)
,partition P2 values (default)
);