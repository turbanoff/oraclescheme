﻿using System;
using NUnit.Framework;
using OracleScheme.DBObjects;

namespace OracleScheme.Test
{
    [TestFixture]
    public class TextGeneratorSequenceTest
    {
        private static readonly string SCHEMA_NAME = "SN";
        TextGenerator generator;
        TextGenerator generatorWithSchema;

        [SetUp]
        public void Setup()
        {
            generator = new TextGenerator(null);
            generatorWithSchema = new TextGenerator(SCHEMA_NAME);
        }

        [Test]
        public void NegativeIncrement()
        {
            var seq = new Sequence("MY_SEQ", 1, 10, -1, false, false, 20, 5);
            generator.Visit(seq);
            generatorWithSchema.Visit(seq);
            Assert.AreEqual("create sequence MY_SEQ minvalue 1 start with 5 maxvalue 10 increment by -1;", generator.Script);
            Assert.AreEqual("create sequence SN.MY_SEQ minvalue 1 start with 5 maxvalue 10 increment by -1;", generatorWithSchema.Script);
        }

        [Test]
        public void SameStartAndMinValue()
        {
            var seq = new Sequence("MY_SEQ", 1, 9, 1, false, false, 20, 1);
            generator.Visit(seq);
            generatorWithSchema.Visit(seq);
            Assert.AreEqual("create sequence MY_SEQ maxvalue 9;", generator.Script);
            Assert.AreEqual("create sequence SN.MY_SEQ maxvalue 9;", generatorWithSchema.Script);
        }

        [Test]
        public void NegativeIncrementAndDefaultMinValue()
        {
            var seq = new Sequence("MY_SEQ", -999999999999999999999999999m, 0, -1, false, false, 20, -8);
            generator.Visit(seq);
            generatorWithSchema.Visit(seq);
            Assert.AreEqual("create sequence MY_SEQ start with -8 maxvalue 0 increment by -1;", generator.Script);
            Assert.AreEqual("create sequence SN.MY_SEQ start with -8 maxvalue 0 increment by -1;", generatorWithSchema.Script);
        }

        [Test]
        public void SameLastNumberAndMaxValue()
        {
            var seq = new Sequence("MY_SEQ", -20, -10, -1, true, false, 20, -10);
            generator.Visit(seq);
            generatorWithSchema.Visit(seq);
            Assert.AreEqual("create sequence MY_SEQ minvalue -20 maxvalue -10 increment by -1 cycle;", generator.Script);
            Assert.AreEqual("create sequence SN.MY_SEQ minvalue -20 maxvalue -10 increment by -1 cycle;", generatorWithSchema.Script);
        }

        [Test]
        public void NegativeIncementAndDefaultMaxValue()
        {
            var seq = new Sequence("MY_SEQ", -9, -1, -1, false, false, 20, -3);
            generator.Visit(seq);
            generatorWithSchema.Visit(seq);
            Assert.AreEqual("create sequence MY_SEQ minvalue -9 start with -3 increment by -1;", generator.Script);
            Assert.AreEqual("create sequence SN.MY_SEQ minvalue -9 start with -3 increment by -1;", generatorWithSchema.Script);
        }

        [Test]
        public void NoCache()
        {
            var seq = new Sequence("MY_SEQ", 1, 10, 1, false, false, 0, 1);
            generator.Visit(seq);
            generatorWithSchema.Visit(seq);
            Assert.AreEqual("create sequence MY_SEQ maxvalue 10 nocache;", generator.Script);
            Assert.AreEqual("create sequence SN.MY_SEQ maxvalue 10 nocache;", generatorWithSchema.Script);
        }

        [Test]
        public void NonDefaultCacheSize()
        {
            var seq = new Sequence("MY_SEQ", 1, 5, 1, false, false, 2, 1);
            generator.Visit(seq);
            generatorWithSchema.Visit(seq);
            Assert.AreEqual("create sequence MY_SEQ maxvalue 5 cache 2;", generator.Script);
            Assert.AreEqual("create sequence SN.MY_SEQ maxvalue 5 cache 2;", generatorWithSchema.Script);
        }

        [Test]
        public void TwoSequences()
        {
            var seq1 = new Sequence("MY_SEQ#1", 1, 99, 1, false, false, 20, 1);
            var seq2 = new Sequence("MY_SEQ#2", 1, 99, 1, false, false, 20, 1);
            var schema = new OracleSchema();
            schema.AddDependency(seq1);
            schema.AddDependency(seq2);
            generator.Visit(schema);
            generatorWithSchema.Visit(schema);
            Assert.AreEqual(
                "set define off" + Environment.NewLine +
                "set sqlblanklines on" + Environment.NewLine + Environment.NewLine +
                "create sequence MY_SEQ#1 maxvalue 99;" + Environment.NewLine + Environment.NewLine +
                "create sequence MY_SEQ#2 maxvalue 99;",
                generator.Script
            );
            Assert.AreEqual(
                "set define off" + Environment.NewLine +
                "set sqlblanklines on" + Environment.NewLine + Environment.NewLine +
                "create sequence SN.MY_SEQ#1 maxvalue 99;" + Environment.NewLine + Environment.NewLine +
                "create sequence SN.MY_SEQ#2 maxvalue 99;",
                generatorWithSchema.Script
            );
        }

    }
}
